EN ÖNEMLİ KISA YOLLAR
CTRL + p => dosya arama
CTRL + s => kısayolları gösterir
ALT + up/down => Kod satırını aşşağı yukarı taşır
ALT + shift + up/down => kodu kopyalayarak aşağı yukarı yazdırır
CTRL + shift + k => satırı komple siler
CTRL + k + c => satırı açıklama satırı yapar
CTRL + k + u => açıklama satırını açar
CTRL + j => terminali açar
shift + ALT + a => seçili satırları baştan sonra açıklama satırı olarak kapatır
CTRL + g => komut satırı numarasına gitmek içindir. Hızlı seyahat
CTRL + shift + tab => bir yan sayfaya geçer
ALT + sağsol => sayfalar arası geçiş yapmaya  yarar
ctrl + H => kelime bulup değiştirme
ctrl + alt + enter => kelime ararken tüm sayfada değişiklik yapmak içnidir.
alt + enter => bir kelimeyi ararken bunu yaparsanız tümü seçersiniz
ctrl + d => imlecin bulunduğu kelimeyi seçer ve bastıkça benzer kelimeleri seçmeye devam eder
alt + click => çoklu imleç özelliğini kullanır mouse ile istediğiniz bölgeye koyarsınız
ctrl + alt + updown => klavye tuşları ile çopklu seçim aşağı yukarı yaparak seçersiniz
ctrl + f2 => seçilen kelimenin tüm dökümanda seçer ve çoklu imleç koyar
shift + alt + mouse click => düz bir doğrultuda çoklu cursor ile seçim yapabilirsiniz
ctrl + shift + alt +yon tuşları => imlecin bulunduğu alandan yukarı aşağı yaparsınız çoklu imleç oluşturur
ctrl + space => bir objenin metodlarını değişkenlerini gösterir. Bir path bulur.
ctrl + shift + space ile fonksiyon parametre isteklerini görebilirsin
shift + alt + f => kodları güzelleştirir.
ctrl + k + f => seçilen kodları güzelleştirir.
ctrl + mouse click => tanımlama noktasına gider
alt + f12 => seçilen objenin tanımlandığı yeri pencere olarak ekranda açar
ctrl + w => açık olan sayfayı kapatır
ctrl + 1,2,3 => sayfayı 1,2,3 e böler
f11 => full screen
ctrl + artı eksi => yazı tipini büyültür küçültür
ctrl + b => soldaki dosya yönetme kısmını açıp kapatır
ctrl + sfiht + f => komple projede arama
ctrl + shift + d => debug ekranını açar





TÜM KISA YOLLAR
f1 => Komut satırı
CTRL + shift + N => yeni bir vc code açar
CTRL + , => kullanıcı ayarlarını açar
CTRL + ENTER => imleç satırı bozmadan alta geçer
CTRL + shift + ENTER => imleç satırı bozmadan yukarıda boşluk bırakır
CTRL + end => kodun sonuna gider
CTRL + home => kodun başına gider
CTRL + updown => kodu mouse tekeri ile kaydırır gibi aşağı yukarı kaydırır
ALT + pageup/down => kodları en üst sayfaya taşır ya da en alt sayfaya
CTRL + shift + m => problemler penceresini açar
alt + r => benzer kelimeleri belli eder ancak seçmez
ctrl + u => cursoru daha önecki konumlarına getirir
ctrl + I => cursorun olduğu satırı seçer
ctrl + alt + sağsol => ile çalışma sayfasını ikiye böler
ctrl + shift + E => çalışılan sayfanın dosya ekranında nerde olduğunu görmenize yardımcı olur
ctrl + shift + g => kod kaynağını gösterir kod dğeişimleri git kısmı vs
ctrl + shift + x => eklentileri gösterir

