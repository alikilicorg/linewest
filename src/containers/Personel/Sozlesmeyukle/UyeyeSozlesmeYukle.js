import React, { Component } from "react";
import LayoutWrapper from '../../../components/utility/layoutWrapper.js';
import PageHeader from '../../../components/utility/pageHeader';
import { Col, Row, Upload, Icon, message, Input, AutoComplete } from 'antd';
import axios from 'axios';
import config2 from '../../../settings/config';
const ajaxURL = config2.http+config2.server+':'+config2.port;
/*eslint-disable*/
import '../../../containers/css/toastrc.css'
import '../../../containers/css/animated.css'
import { ToastContainer } from "react-toastr";
import mesaj from '../../../components/toast/toast'
let container;
/*eslint-enable*/

const Option = AutoComplete.Option;
const Dragger = Upload.Dragger;

 
  function renderOption(item) {
    debugger;
    return (
      <Option key={item.id} text={item.isim}>{item.isim}</Option>
    );
  }
  

class UyeyeSozlemeYukle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataSource:[{id:1,isim:"Ali KILIÇ"},{id:2,isim:"Onur KILIÇ"}]
        }
    }


    handleSearch = (value) => {
        var that = this;
        if(value!==""){
            axios.post(ajaxURL+'/user/search',{search:value}).then(function(response){
                if(response.data.status===true){
                  var state = that.state;
                  state.dataSource = response.data.result;
                  that.setState(state);
                }else{
                  new mesaj(container,{durum:"error",baslik:"Bir Problem Var",icerik:response.data.message});
                }
              });
        }else{
            new mesaj(container,{durum:"error",baslik:"Eksik Veri",icerik:"Arama Kutusu Boş Bırakılamaz"});
        }
    
    }

    onSelect = (value) => {
        debugger;
    }

    render(){
        const props = {
            name: 'file',
            accept:".pdf",
            multiple: false,
            action: '//jsonplaceholder.typicode.com/posts/',
            onChange(info) {
                debugger;
              const status = info.file.status;
              if (status !== 'uploading') {
                console.log(info.file, info.fileList);
              }
              if (status === 'done') {
                message.success(`${info.fileList[0].originFileObj.name} file uploaded successfully.`);
              } else if (status === 'error') {
                message.error(`${info.fileList[0].originFileObj.name} file upload failed.`);
              }
            },
          };


        return (
            <LayoutWrapper>
                <PageHeader>Üyeye Sözleşme Yükleme Sayfası</PageHeader>
                <ToastContainer
                ref={ref => container = ref}
                className="toast-top-right"
                />  
                    <AutoComplete
                        style={{width:"100%",marginBottom:"16px"}}
                        size="large"
                        dataSource={this.state.dataSource.map(renderOption)}
                        onSelect={this.onSelect}
                        onSearch={this.handleSearch}
                        placeholder="Öğrenci No, Ad Soyad, Mail, Telefon"
                        optionLabelProp="text"
                        ><Input suffix={<Icon type="search" className="certain-category-icon" />} />
                    </AutoComplete>
                <Row style={{width:"100%"}}>
                    <Col lg={24} md={24} sm={24} xs={24}>
                        <Dragger  {...props}>
                            <p className="ant-upload-drag-icon">
                            <Icon type="inbox" />
                            </p>
                            <p className="ant-upload-text">Dosyanızı Bu Alanı Tıklayarak ya da Sürükleyip Bırakarak Yükleyiniz</p>
                            <p className="ant-upload-hint">Sadece PDF formatındaki dosyalar kabul edilmektedir</p>
                            <p className="ant-upload-hint">Yükleme İşlemini Yapılabilmesi İçin Lütfen Kullanıcı Seçimi Yapınız</p>
                        </Dragger>
                    </Col>
                </Row> 
            </LayoutWrapper>
            );
    }
}

export default UyeyeSozlemeYukle;