import React, { Component } from "react";
import LayoutWrapper from '../../../components/utility/layoutWrapper.js';
import PageHeader from '../../../components/utility/pageHeader';
import axios from 'axios';
import config2 from '../../../settings/config';
import yetkiKontrol from '../../../settings/yetkiKontrol';
import { Icon, Button, Input, Col, Row, AutoComplete, message, Card, Switch, Select, Popconfirm, Breadcrumb } from 'antd';
const Option = AutoComplete.Option;
const Options = Select.Option;



const ajaxURL = config2.http+config2.server+':'+config2.port;

function renderOption(item) {
    return (
      <Option key={item.id} text={item.isim}>{item.isim}</Option>
    );
  }

class Yetkilendirme extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user:{id:1,type:"personel",yetkiler:[]},
            yetkiler:{
                sayfa:{id:7,message:"Sayfayı Açma Yetkiniz Yoktur",durum:false},
                personel:{id:27,message:"Kişisel Bilgileri Düzeltme Yetkiniz Yoktur",durum:false},
                admin:{id:28,message:"Bu Sayfada Adminlik Yetkiniz Yoktur",durum:false}
            },
            userDataSource:[],
            personel:{
                panelDisplay:"block",
                yetkliList:[],
                select:0,
                liste:[]
            },
            editUser:{
                panelDisplay:"block",
                yetkliList:[],
                select:0,
                liste:[]
            },
            yenigrup:{
                grupadi:"",
                konum:"Yeni Yetki Grubu Kimler İçin Kullanılacak ?",
                yetkiler:[]
            },
            eskigrup:{
                grupadi:"",
                grupid:0,
                yetkiler:[]
            },
            grupList:[],
            yetkiList:[],
            personelDataSource:[],
            userDataSource:[],
        };
    }

    personelArama = (value) => {
        var that = this;
        if(value!==""){
            axios.post(ajaxURL+'/personel/search',{search:value}).then(function(response){
                if(response.data.status==true){
                  var state = that.state;
                  state.personelDataSource = response.data.result;
                  that.setState(state);
                }else{
                    message.warning(response.data.message);
                }
              });
        }else{
            message.warning("Personel Arama Kutusu Boş Bırakılamaz");
        }
    }

    personelSecme = (value) => {
        var personelid = parseInt(value);
        this.getYetkiler(personelid,'personel');
        var state = this.state;
        state.personel.select=value;
        this.setState(state);
    }

    userArama = (value) => {
        debugger;
        var that = this;
        var personelid = this.state.user.id;
        if(this.state.user.type=="personel"){
            if(value!==""){
                axios.post(ajaxURL+'/user/userbilgisearch',{search:value,personelid:personelid}).then(function(response){
                    if(response.data.status==true){
                        debugger;
                      var state = that.state;
                      if(that.state.user.yetkiler.indexOf(26)>-1){
                        state.userDataSource = response.data.result;
                      }else{
                        state.userDataSource=[];
                      for(var i in response.data.result){
                          if(response.data.result[i].personel===personelid){
                            state.userDataSource.push(response.data.result[i]);
                          }
                        }
                      }
                      that.setState(state);
                    }else{
                        message.warning(response.data.message);
                    }
                  });
            }else{
                message.warning("Kulanıcı Arama Kutusu Boş Bırakılamaz");
            }
        }else{
            message.warning("Sadece Personeller Arama Yapabilir");
        }
    }

    userSecme = (value) => {
        debugger;
        var userid = parseInt(value);
        this.getYetkiler(userid,'user');
        var state = this.state;
        state.editUser.select=parseInt(value);
        this.setState(state);
    }

    getYetkiler(personelid,konum){
        var that = this;
        if(personelid!==0){
            var ekurl = "";
            var data = {};
            if(konum=="personel"){
                ekurl="/yetkilendirme/personelTumYetkiler";
                data = {personelid:personelid};
            }
            if(konum=="user"){
                ekurl="/yetkilendirme/userTumYetkiler";
                data = {userid:personelid};
            }
            axios.post(ajaxURL+ekurl,data).then(function(response){
                if(response.data.status==true){
                    var state = that.state;
                    if(konum=="personel"){
                        //xxx
                        state.personel.yetkliList = response.data.result;
                    }
                    if(konum=="user"){
                        state.editUser.yetkliList = response.data.result;
                    }
                    that.setState(state);
                }else{
                    message.warning(response.data.message);
                }
            });
        }else{
            message.warning("Lütfen Geçerli Bir İsim Seçiniz");
        }
    }

    yetkiKontrol(useryetkilist){
        var state = this.state;
        for(var i in state.yetkiler){
            if(useryetkilist.indexOf(state.yetkiler[i].id)>-1){
                state.yetkiler[i].durum=true;
            }
        }
        this.setState(state);
    }

    yetkileriGetir(){
        var that = this;
        var ekURL = "";
        var data = {};
        if(this.state.user.type==="personel"){
          ekURL = "/yetkilendirme/personelTumYetkiler"
          data["personelid"]=this.state.user.id;
        }else{
          ekURL = "/yetkilendirme/userTumYetkiler"
          data["userid"]=this.state.user.id;
        }
        axios.post(ajaxURL+ekURL,data).then(function(gelen){
            debugger;
          var state = that.state;
          state.user.yetkiler = gelen.data.result;
          that.yetkiKontrol(gelen.data.result);
          that.setState(state);
        }).catch(function(hata){
          message.error(hata.message);
        });
    }

    getAllYetkiler(){
        var that = this;
        axios.get(ajaxURL+'/yetki/list').then(function(response){
            if(response.data.status==true){
                debugger;
                var state = that.state;
                state.yetkiList=response.data.result;
                that.setState(state);
            }else{
                message.warning(response.data.message);
            }
        });
    }

    getAllGrupYetkiler(){
        var that = this;
        axios.get(ajaxURL+'/yetkigrup/list').then(function(response){
            if(response.data.status==true){
                var state = that.state;
                state.grupList=response.data.result;
                that.setState(state);
            }else{
                message.warning(response.data.message);
            }
        }); 
    }

    componentWillMount(){
        this.yetkileriGetir();
    }
    componentDidMount(){
        
        this.getAllYetkiler();
        this.getAllGrupYetkiler();
    }

    listAltYetkiler(a){
        var that = this;
        var grupid = parseInt(a.target.attributes[0].nodeValue);
        var konum = a.target.attributes[1].nodeValue;
        grupid=parseInt(grupid);
        if(grupid>0){
            axios.post(ajaxURL+"/yetkipaketi/findGrupYetkileri",{yetkigrupid:grupid}).then(function(response){
                if(response.data.status==true){
                    var state = that.state;
                    if(konum=="personel"){
                        state.personel.liste = response.data.result;
                    }
                    if(konum=="user"){
                        state.editUser.liste = response.data.result;
                    }
                    that.setState(state);
                }else{
                    message.warning(response.data.message);
                }
            });
        }else{
            message.warning("Geçerli Bir Yetki Grubunu Listeleyiniz");
        }
    }


    getNameAltYetki(yetkiid){
        var name = "";
        for(var i in this.state.yetkiList){
            var id = this.state.yetkiList[i]["id"];
            if(id==yetkiid){
                name =this.state.yetkiList[i]["isim"]
                return name;
                break; 
            }
        }
    }

    yetkilendirme(yetkiid,userid,konum,durum){
        debugger;
        if(userid!==0){
        var that = this;
        var state = this.state;
        var data;
        if(konum=="personel"){
            data = state.personel.yetkliList;
        }
        if(konum=="user"){
            data = state.editUser.yetkliList;
        }
        if(data.indexOf(yetkiid)!==-1){
            var indx = data.indexOf(yetkiid);
            if(durum==false){
                axios.post(ajaxURL+"/yetkilendirme/delete",{userid:userid,yetkiid:yetkiid,usertype:konum}).then(function(response){
                    if(response.data.status==true){
                        message.success(response.data.message);
                        data.splice(indx,1);
                        if(konum=="personel"){
                            state.personel.yetkliList=data;
                        }
                        if(konum=="user"){
                            state.editUser.yetkliList=data;
                        }
                        that.setState(state);
                    }else{
                        message.warning(response.data.message);
                    }
                }); 
            }
        }else{
            if(durum==true){
                axios.post(ajaxURL+"/yetkilendirme/insert",{userid:userid,yetkiid:yetkiid,yetkituru:"normal",usertype:konum}).then(function(response){
                    if(response.data.status==true){
                        message.success(response.data.message);
                        data.push(yetkiid);
                        if(konum=="personel"){
                            state.personel.yetkliList=data;
                        }
                        if(konum=="user"){
                            state.editUser.yetkliList=data;
                        }
                        that.setState(state);
                    }else{
                        message.warning(response.data.message);
                    }
                }); 
                
            }
        }
        }else{
            if(konum=="personel"){
                message.warning("Lütfen Yetkilendirilecek Personeli Bulunuz");
            }else{
                message.warning("Lütfen Yetkilendirilecek Kullanıcıyı Bulunuz");
            }
            
        }
    }


    yeniGrubunYetkileri(yetkiid,durum){
       var that = this;
       var state = this.state;
       
       if(state.yenigrup.yetkiler.indexOf(yetkiid)==-1){
        if(durum){
            state.yenigrup.yetkiler.push(yetkiid);
        }else{
            var idx = state.yenigrup.yetkiler.indexOf(yetkiid);
            state.yenigrup.yetkiler.splice(idx,1);
        }        
       }else{
        if(!durum){
            var idx = state.yenigrup.yetkiler.indexOf(yetkiid);
            state.yenigrup.yetkiler.splice(idx,1);
        }else{
            state.yenigrup.yetkiler.push(yetkiid);
        } 
       }
       this.setState(state);
    }


    yenigrupOlustur(){
        var that = this;
        var state = this.state;
        var yenigrupadi = state.yenigrup.grupadi;
        var yetkiler = state.yenigrup.yetkiler;
        var konum = state.yenigrup.konum;
        if(yenigrupadi!==""){
            if(yetkiler.length>0){
                axios.post(ajaxURL+"/yetkigrup/insert",{isim:yenigrupadi,konum:konum}).then(function(response){
                    if(response.data.status==true){
                        var id = response.data.result.id;
                        axios.post(ajaxURL+"/yetkipaketi/multiinsert",{grupid:id,multi:yetkiler}).then(function(response2){
                            if(response2.data.status==true){
                                state.yenigrup.grupadi="";
                                while(state.yenigrup.yetkiler.length>0){
                                    state.yenigrup.yetkiler.splice(0,1);
                                }
                                state.yenigrup.konum="personel";
                                that.setState(state);
                                that.getAllGrupYetkiler();
                                message.success(response2.data.message);
                            }else{
                                message.error(response2.data.message);
                            }
                        });
                    }else{
                        message.warning(response.data.message);
                    }
                }).catch(function(err){
                    message.error("Grup Adı Sisteme Eklenemedi");
                }); 
            }else{
                message.error("Yeni GRup Oluştururken En az 1 Yetki Seçmiş Olmanız Gerekir");
            }
        }else{
            message.error("Yeni Oluşturacağınız Grup Adını Boş Bırakamazsınız");
        }
    }

    yeniGrupAdiDegistir(a){
        var deger = a.target.value;
        if(deger!==""){
            var state = this.state;
            state.yenigrup.grupadi=deger;
            this.setState(state);
        }else{
            message.error("Yeni Oluşturacağınız Grup Adını Boş Bırakamazsınız");
        }
        

    }

    listAltYetkilerDuzenle(a){
        var that = this;
        a=parseInt(a);
        axios.post(ajaxURL+"/yetkipaketi/findGrupYetkileri",{yetkigrupid:parseInt(a)}).then(function(response){
            if(response.data.status==true){
                var result = response.data.result;
                var state = that.state;
                state.eskigrup.yetkiler = result;
                state.eskigrup.grupid = a;
                state.eskigrup.grupadi = that.state.grupList.find(function(item){
                    if(a==parseInt(item.id)){
                        return item.isim;
                    }
                });
                that.setState(state);
            }});
    }

    gruptanYetkiKaldir(yetkiid,th){
        debugger;
        var that = this;
        var id = parseInt(yetkiid);
        if(id>0){
            axios.post(ajaxURL+"/yetkipaketi/deleteByid",{id:id}).then(function(response){
                if(response.data.status==true){
                    var result = response.data.result;
                    message.success(response.data.message);
                    var state = that.state;
                    var yenidizi = [];
                    var grupid;
                    for(var i in state.eskigrup.yetkiler){
                        var id2 = state.eskigrup.yetkiler[i]["id"];
                        if(parseInt(id2)!==id){
                            yenidizi.push(state.eskigrup.yetkiler[i]);
                        }
                        grupid=state.eskigrup.yetkiler[i].yetkigrupid;
                    }
                    state.eskigrup.yetkiler=yenidizi;
                    that.setState(state);
                    /* that.getAllGrupYetkiler(); */
                    /* that.listAltYetkilerDuzenle(grupid); */
                }});
        }else{
            message.warning("Lütfen Geçerli Bir Grup Seçiniz");
        }

    }

    seciliGrubuSil(){
        var that = this;
        var state = this.state;
        var seciligrup = state.eskigrup.grupid;
        if(parseInt(seciligrup)>0){
            axios.post(ajaxURL+"/yetkigrup/delete",{id:parseInt(seciligrup)}).then(function(response){
                if(response.data.status==true){
                    var result = response.data.result;
                    message.success(response.data.message);
                    var state = that.state;
                    state.eskigrup.grupid="";
                    state.eskigrup.grupadi="";
                    while(state.eskigrup.yetkiler.length>0){
                        state.eskigrup.yetkiler.splice(0.1);
                    }
                    that.setState(state);
                    that.getAllGrupYetkiler();
                }});
        }else{
            message.warning("Lütfen Geçerli Bir Grup Seçiniz");
        }
    }


    grubaYetkiKoy(yetkiid,durum){
        debugger;
        var that = this;
        var state = this.state;
        var seciligrup = state.eskigrup.grupid;
        var id = yetkiid;
        if(id>0){
            axios.post(ajaxURL+"/yetkipaketi/insert",{yetkigrupid:seciligrup,yetkiid:id}).then(function(response){
                if(response.data.status==true){
                    var result = response.data.result;
                    message.success(response.data.message);
                    var state = that.state;
                    var dtm = response.data.result;
                    state.eskigrup.yetkiler.push({id:dtm.id,yetkigrupid:dtm.yetkigrupid,yetkiid:dtm.yetkiid});
                    that.setState(state);
                }});
        }else{
            message.warning("Lütfen Geçerli Bir Yetki İşaretleyini<");
        }
    }

    yeniGrupKonumDegistir(konum){
        var that = this;
        var state = that.state;
        state.yenigrup.konum = konum;
    }

    render(){
        var that = this;

        const personelYetkiOwnerSwitch = this.state.yetkiList.map((item,sira) =>{
            var yetkiid = item.id;
            var yetkiname = item.isim;
            if(that.state.personel.yetkliList.indexOf(yetkiid)!==-1){
               return(
                <Row style={{margin:"5px"}}>
                    <Col style={{height:"33px",padding:"5px",fontSize:"11px"}} lg={19} md={19} sm={19} xs={19}>{yetkiname}</Col>
                    <Col style={{textAlign:"right",borderBottom:"1px solid #e8e8e8",height:"33px",padding:"5px"}} lg={5} md={5} sm={5} xs={5}>
                    <Switch onChange={that.yetkilendirme.bind(that,yetkiid,parseInt(that.state.personel.select),'personel')} checked={true} />
                    </Col>
                </Row>
                );    
            }
        });
        const userYetkiOwnerSwitch = this.state.yetkiList.map((item,sira) =>{
            var yetkiid = item.id;
            var yetkiname = item.isim;
            if(that.state.editUser.yetkliList.indexOf(yetkiid)!==-1){
               return(
                <Row style={{margin:"5px"}}>
                    <Col style={{height:"33px",padding:"5px",fontSize:"11px"}} lg={19} md={19} sm={19} xs={19}>{yetkiname}</Col>
                    <Col style={{textAlign:"right",height:"33px",padding:"5px"}} lg={5} md={5} sm={5} xs={5}>
                    <Switch onChange={that.yetkilendirme.bind(that,yetkiid,parseInt(that.state.editUser.select),'user')} checked={true} />
                    </Col>
                </Row>
                );    
            }
        });
        

        const personelAltYetkiOwner = this.state.personel.liste.map((item,sira) =>{
            var yetkiid = item.yetkiid;
            var yetkiname = that.getNameAltYetki(yetkiid);
            if(that.state.personel.yetkliList.indexOf(yetkiid)==-1){
                return(
                    <Row style={{margin:"5px"}}>
                        <Col style={{height:"33px",padding:"5px",fontSize:"11px",color:"#4caf50"}} lg={19} md={19} sm={19} xs={19}>{yetkiname}</Col>
                        <Col style={{textAlign:"right",height:"33px",padding:"5px"}} lg={5} md={5} sm={5} xs={5}>
                        <Switch onChange={that.yetkilendirme.bind(that,yetkiid,parseInt(that.state.personel.select),'personel')} checked={false} />
                        </Col>
                    </Row>
                );
            }else{
                return(
                    <Row style={{margin:"5px"}}>
                        <Col style={{height:"33px",padding:"5px",fontSize:"11px"}} lg={19} md={19} sm={19} xs={19}>{yetkiname}</Col>
                        <Col style={{textAlign:"right",height:"33px",padding:"5px"}} lg={5} md={5} sm={5} xs={5}>
                            <Switch onChange={that.yetkilendirme.bind(that,yetkiid,parseInt(that.state.personel.select),'personel')} checked={true} />
                        </Col>
                    </Row>
                );
            }
        });
        const userAltYetkiOwner = this.state.editUser.liste.map((item,sira) =>{
            var yetkiid = item.yetkiid;
            var yetkiname = that.getNameAltYetki(yetkiid);
            if(that.state.editUser.yetkliList.indexOf(yetkiid)==-1){
                return(
                    <Row style={{margin:"5px"}}>
                        <Col style={{height:"33px",padding:"5px",fontSize:"11px",color:"#4caf50"}} lg={19} md={19} sm={19} xs={19}>{yetkiname}</Col>
                        <Col style={{textAlign:"right",height:"33px",padding:"5px"}} lg={5} md={5} sm={5} xs={5}>
                        <Switch onChange={that.yetkilendirme.bind(that,yetkiid,parseInt(that.state.editUser.select),'user')} checked={false} />
                        </Col>
                    </Row>
                );
            }else{
                return(
                    <Row style={{margin:"5px"}}>
                        <Col style={{height:"33px",padding:"5px",fontSize:"11px"}} lg={19} md={19} sm={19} xs={19}>{yetkiname}</Col>
                        <Col style={{textAlign:"right",height:"33px",padding:"5px"}} lg={5} md={5} sm={5} xs={5}>
                            <Switch onChange={that.yetkilendirme.bind(that,yetkiid,parseInt(that.state.editUser.select),'user')} checked={true} />
                        </Col>
                    </Row>
                );
            }
        });



        const personelGrupYetkiSwitch = this.state.grupList.map((item,sira) =>{
            var yetkiid = item.id;
            var yetkiname = item.isim;
            var yetkikonum = item.konum;
            if(yetkikonum=="personel"){
                return(
                    <Row style={{margin:"5px"}}>
                        <Col style={{height:"33px",padding:"5px"}} lg={19} md={19} sm={19} xs={19}>{yetkiname}</Col>
                        <Col style={{textAlign:"right",height:"33px",padding:"5px"}} lg={5} md={5} sm={5} xs={5}>
                        <Button style={{borderRadius:"5px"}} onClick={this.listAltYetkiler.bind(this)} grupid={yetkiid} konum={'personel'} size="small" type="primary">Listele</Button>
                        </Col>
                    </Row>
                );
            }
            
        });
        const userGrupYetkiSwitch = this.state.grupList.map((item,sira) =>{
            var yetkiid = item.id;
            var yetkiname = item.isim;
            var yetkikonum = item.konum;
            if(yetkikonum == "user"){
                return(
                    <Row style={{margin:"5px"}}>
                        <Col style={{height:"33px",padding:"5px"}} lg={19} md={19} sm={19} xs={19}>{yetkiname}</Col>
                        <Col style={{textAlign:"right",height:"33px",padding:"5px"}} lg={5} md={5} sm={5} xs={5}>
                        <Button style={{borderRadius:"5px"}} onClick={this.listAltYetkiler.bind(this)} grupid={yetkiid} konum={'user'} size="small" type="primary">Listele</Button>
                        </Col>
                    </Row>
                );
            }
        });

        const yeniGrupOlusturmaListesi = this.state.yetkiList.map((item,sira) =>{
            var yetkiid = item.id;
            var yetkiname = item.isim;
            if(that.state.yenigrup.yetkiler.indexOf(yetkiid)!==-1){
                return(
                    <Row style={{margin:"5px"}}>
                        <Col style={{height:"33px",padding:"5px"}} lg={19} md={19} sm={19} xs={19}>{yetkiname}</Col>
                        <Col style={{textAlign:"right",height:"33px",padding:"5px"}} lg={5} md={5} sm={5} xs={5}>
                            <Switch onChange={that.yeniGrubunYetkileri.bind(that,yetkiid)} checked={true} />
                        </Col>
                    </Row>
                );
            }else{
                return(
                    <Row style={{margin:"5px"}}>
                        <Col style={{height:"33px",padding:"5px"}} lg={19} md={19} sm={19} xs={19}>{yetkiname}</Col>
                        <Col style={{textAlign:"right",height:"33px",padding:"5px"}} lg={5} md={5} sm={5} xs={5}>
                            <Switch onChange={that.yeniGrubunYetkileri.bind(that,yetkiid)} checked={false} />
                        </Col>
                    </Row>
                );
            }
            
        });


        const mevcutGruplar = this.state.grupList.map((item,sira) =>{
            var grupid = item.id;
            var grupadi = item.isim;
            return (<Options key={grupid}>{grupadi}</Options>);
        });

        /* const eskiGrupYetkileri = this.state.eskigrup.yetkiler.map((item,sira) => {
            var yetkiid = item.yetkiid;
            var grupid = item.yetkigrupid;
            var id = item.id;
            var yetkiadi = that.state.yetkiList.find(function(obj){
                if(parseInt(obj.id) == parseInt(yetkiid)){
                    return obj.isim;
                }
            });
            if(typeof yetkiadi !=="undefined"){
                return(
                    <Row style={{margin:"5px"}}>
                        <Col style={{height:"33px",padding:"5px"}} lg={19} md={19} sm={19} xs={19}>{yetkiadi.isim}</Col>
                        <Col style={{textAlign:"right",height:"33px",padding:"5px"}} lg={5} md={5} sm={5} xs={5}>
                            <Switch onChange={that.gruptanYetkiKaldir.bind(that,id)} checked={true} />
                        </Col>
                    </Row>
                );
            }
        }); */

        const eskiGrupYetkileri = this.state.yetkiList.map((item,sira) => {
            var yetkiid = item.id;
            var yetkiadi = item.isim;
            var durum = false;
            var id=0;
            for(var i in that.state.eskigrup.yetkiler){
                var yetkiid2 = that.state.eskigrup.yetkiler[i].yetkiid;
                if(yetkiid==yetkiid2){
                    durum=true;
                    id = that.state.eskigrup.yetkiler[i].id;
                    break; 
                }
            }
            if(durum==true){
                return(
                    <Row style={{margin:"5px"}}>
                        <Col style={{height:"33px",padding:"5px"}} lg={19} md={19} sm={19} xs={19}>{yetkiadi}</Col>
                        <Col style={{textAlign:"right",height:"33px",padding:"5px"}} lg={5} md={5} sm={5} xs={5}>
                            <Switch onChange={that.gruptanYetkiKaldir.bind(that,id)} checked={true} />
                        </Col>
                    </Row>
                );
            }else{
                return(
                    <Row style={{margin:"5px"}}>
                        <Col style={{height:"33px",padding:"5px"}} lg={19} md={19} sm={19} xs={19}>{yetkiadi}</Col>
                        <Col style={{textAlign:"right",height:"33px",padding:"5px"}} lg={5} md={5} sm={5} xs={5}>
                            <Switch onChange={that.grubaYetkiKoy.bind(that,yetkiid)} checked={false} />
                        </Col>
                    </Row>
                );
            }
        });

        const style = {
            rowStyle: {width: '100%',display: 'flex',flexFlow: 'row wrap',marginBottom:"10px"},
            rowStyle2: {width: '100%',display: 'flex',flexFlow: 'row wrap',marginBottom:"10px"},
            rowStyleAdmin: {display:this.state.yetkiler.admin.durum==true ? "flex":"none",width: '100%',flexFlow: 'row wrap',marginBottom:"10px"},
            rowStylePersonel:{display:this.state.yetkiler.admin.durum==true ? "flex":this.state.yetkiler.personel.durum==true ? "flex":"none",width: '100%',flexFlow: 'row wrap',marginBottom:"10px"},
            colStyle:{marginBottom: '16px'},
            colStyle2:{marginTop: '16px'},
            bodyStyle1:{padding:"0",height:"300px",overflowY:"scroll",width: "100%"},
            gutter:"10"
        };

        return (
            <LayoutWrapper>

                <Breadcrumb style={{backgroundColor:"#fff",padding:"10px",width:"100%",marginBottom:"5px"}}>
                    <Breadcrumb.Item>Personel Paneli</Breadcrumb.Item>
                    <Breadcrumb.Item>Üye ve Personel Yetkilendirme</Breadcrumb.Item>
                </Breadcrumb>

                <Card title="Kurum İçi Personelleri Yetkilendirme" style={{width:"100%",marginBottom:"20px"}} bodyStyle={{padding:"10px"}}>
                <Row style={style.rowStyleAdmin} gutter={style.gutter} justify="start">
                    <Col lg={24} md={24} sm={24} xs={24}>
                        <Row style={style.colStyle}>
                            <Col lg={24} md={24} sm={24} xs={24}>
                                <AutoComplete
                                style={{width:"100%",marginBottom:"10px"}}
                                size="large"
                                dataSource={this.state.personelDataSource.map(renderOption)}
                                onSelect={this.personelSecme}
                                onSearch={this.personelArama}
                                placeholder="Personel Ad Soyad, Mail, Telefon"
                                optionLabelProp="text"
                                ><Input suffix={<Icon type="search" className="certain-category-icon" />} />
                            </AutoComplete>
                            </Col>
                        </Row>
                        <Row gutter={style.gutter} style={{marginTop: '16px'}} >
                            <Col lg={8} md={12} sm={12} xs={24}>
                                <Card bodyStyle={style.bodyStyle1} title="Grup Yetkiler">
                                    {personelGrupYetkiSwitch}
                                </Card>
                            </Col>
                            <Col lg={8} md={12} sm={12} xs={24}>
                                <Card bodyStyle={style.bodyStyle1} title="Alt Yetkiler">
                                    {personelAltYetkiOwner}
                                </Card>
                            </Col>
                            <Col lg={8} md={12} sm={12} xs={24}>
                                <Card bodyStyle={style.bodyStyle1} title="Sahip Olduğu Yetkiler">
                                    {personelYetkiOwnerSwitch}
                                </Card>
                            </Col>
                        </Row>  

                    </Col>
                </Row>
                </Card>

                <Card title="Öğrenci ve Kullanıcıların Yetkilendirilmesi" style={{width:"100%",marginBottom:"20px"}} bodyStyle={{padding:"10px"}}>
                <Row style={style.rowStylePersonel} gutter={style.gutter} justify="start">
                    <Col lg={24} md={24} sm={24} xs={24}>
                        <Row style={style.colStyle}>
                            <Col lg={24} md={24} sm={24} xs={24}>
                                <AutoComplete
                                    style={{width:"100%",marginBottom:"16px"}}
                                    size="large"
                                    dataSource={this.state.userDataSource.map(renderOption)}
                                    onSelect={this.userSecme.bind(this)}
                                    onSearch={this.userArama.bind(this)}
                                    placeholder="Üye Ad Soyad, Mail, Telefon"
                                    optionLabelProp="text"
                                    ><Input suffix={<Icon type="search" className="certain-category-icon" />} />
                                </AutoComplete>
                            </Col>
                        </Row>
                        <Row gutter={style.gutter} style={{marginTop: '16px',display:this.state.editUser.panelDisplay}} >
                            <Col lg={8} md={12} sm={12} xs={24}>
                                <Card bodyStyle={style.bodyStyle1} title="Grup Yetkiler">
                                    {userGrupYetkiSwitch}
                                </Card>
                            </Col>
                            <Col lg={8} md={12} sm={12} xs={24}>
                                <Card bodyStyle={style.bodyStyle1} title="Alt Yetkiler">
                                    {userAltYetkiOwner}
                                </Card>
                            </Col>
                            <Col lg={8} md={12} sm={12} xs={24}>
                                <Card bodyStyle={style.bodyStyle1} title="Sahip Olduğu Yetkiler">
                                    {userYetkiOwnerSwitch}
                                </Card>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                </Card>

                <Card title="Yetki Yönetim Paneli" style={{width:"100%",marginBottom:"20px"}} bodyStyle={{padding:"10px"}}>
                <Row style={style.rowStyleAdmin} gutter={style.gutter} justify="start">
                    <Col lg={24} md={24} sm={24} xs={24}>
                   {/*  <Card bodyStyle={{padding:"16px"}} bordered={false} style={{ width: "100%" }}> */}
                        <Row gutter={style.gutter} style={style.colStyle}>
                                <Col lg={12} md={12} sm={24} xs={24}>
                                    <Card 
                                    extra={
                                        <Button style={{borderRadius:"5px"}} onClick={this.yenigrupOlustur.bind(this)} size="small" type="primary">Yeni Yetki Grubu Oluştur</Button>
                                        } 
                                    bodyStyle={style.bodyStyle1} title="Yeni Yetki Grubu Oluştur">
                                        <Row style={{margin:"0",marginTop:"10px"}} gutter={style.gutter} justify="start">
                                            <Col lg={24} md={24} sm={24} xs={24}>
                                                <Input value={this.state.yenigrup.grupadi} onChange={this.yeniGrupAdiDegistir.bind(this)} style={{width:"100%"}} placeholder="Yeni Yetki Grubu Adı Yazınız" />
                                            </Col>
                                        </Row>
                                        <Row style={{margin:"0",marginTop:"10px"}} gutter={style.gutter} justify="start">
                                            <Col lg={24} md={24} sm={24} xs={24}>
                                            <Select 
                                            defaultValue={this.state.yenigrup.konum} 
                                            style={{ width: "100%",marginTop:"10px" }}
                                            onChange={this.yeniGrupKonumDegistir.bind(this)}
                                            placeholder="Bu Grup Hangi Üye Türü Tarafından Kullanılacak"
                                            >
                                            <Option value="personel">Personel</Option>
                                            <Option value="user">Kullanıcı</Option>
                                            </Select>
                                            </Col>
                                        </Row>
                                        <Row style={{margin:"0",marginTop:"10px"}} gutter={style.gutter} justify="start">
                                            <Col lg={24} md={24} sm={24} xs={24}>
                                                {yeniGrupOlusturmaListesi}
                                            </Col>
                                        </Row>
                                    </Card>
                                </Col>
                                <Col lg={12} md={12} sm={24} xs={24}>
                                    <Card 
                                    extra={
                                        <Popconfirm title="Grubu Silmek İstediğinize Eminmisiniz?" onConfirm={this.seciliGrubuSil.bind(this)} okText="Evet" cancelText="Hayır">
                                        <Button style={{borderRadius:"5px"}} size="small" type="primary">Seçilen Grubu Sil</Button>
                                        </Popconfirm>
                                        }
                                    bodyStyle={style.bodyStyle1} title="Var Olan Yetki Gruplarını Düzenle">
                                        <Row style={{margin:"0",marginTop:"10px"}} gutter={style.gutter} justify="start">
                                            <Col lg={24} md={24} sm={24} xs={24}>
                                            <Select
                                                defaultValue={this.state.eskigrup.grupadi}
                                                onChange={this.listAltYetkilerDuzenle.bind(this)}
                                                style={{ width: '100%' }}
                                                placeholder="Düzenlenecek Yetki Grubu Seçiniz"
                                            >{mevcutGruplar}</Select>
                                            </Col>
                                        </Row>
                                        <Row style={{margin:"0",marginTop:"10px"}} gutter={style.gutter} justify="start">
                                            <Col lg={24} md={24} sm={24} xs={24}>
                                                {eskiGrupYetkileri}
                                            </Col>
                                        </Row>
                                    </Card>
                                </Col>
                        </Row>
                    {/* </Card> */}
                    </Col>
                </Row>
                </Card>
            </LayoutWrapper>
        );
    }
}

export default Yetkilendirme;