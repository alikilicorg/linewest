import React, { Component } from "react";
import Card from '../../../components/uielements/card';
import moment from 'moment';
import { Col, Row, Button, Modal, Table, Input, DatePicker, Popover, Select, InputNumber, message, Breadcrumb } from 'antd';
import LayoutWrapper from '../../../components/utility/layoutWrapper.js';
import PageHeader from '../../../components/utility/pageHeader';
import axios from 'axios';
import config2 from '../../../settings/config';
const ajaxURL = config2.http+config2.server+':'+config2.port;
const { TextArea } = Input;
const Option = Select.Option;


class Adayuyellistesi extends Component {
    
    constructor(props) {
        super(props);
        var yil = new Date().getFullYear();
        var ay = new Date().getMonth()+1;;
        var gun = new Date().getDate();;
        this.state = {
            adayUyeList:[],
            randevuUyeList:[],
            activePanel:null,
            activeUser:null,
            activeUserName:"",
            activeModal:"",
            columns:[
                {
                    title: 'Tarih',
                    dataIndex: 'tarih',
                    key: 'tarih',
                    width:"20%"
                },
                {
                    title: 'Gün',
                    dataIndex: 'gun',
                    key: 'gun',
                    width:"15%"
                },
                {
                    title: 'Mesaj',
                    dataIndex: 'mesaj',
                    key: 'mesaj',
                    width:"60%"
                } 
            ],
            dataSource :[],
            randevuTarihi:"",
            gorusmeMesaj:"",
            paketList:[],
            paketSecimi:0,
            tckimlikNo:"",
            taksitmiktari:0,
            taksitTarihi:"",
            taksitler:[],
            ikametgah:"",
            ortaAd:"",
            kayitTarihi:"",
            simdikiZaman : yil+"-"+ay+"-"+gun
        }
        
    }

    uploadAdayUyeler(that){
        axios.post(ajaxURL+'/adayuye/findAdayUyeByPersonelId',{personelid:1}).then(function(response){
            if(response.data.status===true){
              var state = that.state;
              for(var i in response.data.result){
                response.data.result[i]["modalGecmisVisible"]=false;
                response.data.result[i]["modalNotVisible"]=false;
                response.data.result[i]["modalAktifVisible"]=false;
                response.data.result[i]["paket"]=0;
              }
              state.adayUyeList = response.data.result;
              that.setState(state);
            }else{
                message.error("İlgilenilen Program Listesi Çekilemedi");
            }
          });
    }

    uploadRandevuluUyeler(that){
        axios.post(ajaxURL+'/adayuye/sonUcGunRandevulari',{personelid:1}).then(function(response){
            if(response.data.status===true){
              var state = that.state;
              for(var i in response.data.result){
                response.data.result[i]["modalGecmisVisible"]=false;
                response.data.result[i]["modalNotVisible"]=false;
                response.data.result[i]["modalAktifVisible"]=false;
              }
              state.randevuUyeList = response.data.result;
              that.setState(state);
            }else{
                message.error("Son 3 Günlük Randevu Listesi Çekilemedi");
            }
          });
    }

    uploadAdayUyeList(that){
        axios.get(ajaxURL+'/paket/list').then(function(response){
            if(response.data.status===true){
              var state = that.state;
              state.paketList=response.data.result;
              that.setState(state);
            }else{
                message.error("Son 3 Günlük Randevu Listesi Çekilemedi");
            }
          });
    }


    componentDidMount(){
        var that = this;
        this.uploadAdayUyeler(that);
        this.uploadRandevuluUyeler(that);
        this.uploadAdayUyeList(that);
    }

    gecmisiYazdir = (response) =>{
        var yenidata = [];
        var gunler = ["Paz","Pzt","Sal","Çar","Per","Cum","Cts"];
        for(var i in response.data.result){
            var data = response.data.result[i];
            var yeniobje = {};
            var randevuZamani = new Date(data.randevu);
            yeniobje.gun = gunler[randevuZamani.getDay()];
            yeniobje.tarih = randevuZamani.getDate()+'.'+(randevuZamani.getMonth()+1)+'.'+randevuZamani.getFullYear();
            yeniobje.mesaj = data.mesaj;
            yeniobje.key = data.id;
            yenidata.push(yeniobje);
            
        }
        return yenidata;
    }

    showModal = (sira,userid,e) => {
        debugger;
        var that = this;
        var sira = sira || parseInt(e.target.attributes[0]["nodeValue"]);
        var userid = userid || parseInt(e.target.attributes[1]["nodeValue"]);
        var tip = e.target.attributes[2]["nodeValue"];
        var isim = e.target.attributes[3]["nodeValue"];
        var state = this.state;
        state.activePanel=sira;
        state.activeUser=userid;
        state.activeUserName = isim;
        state.activeModal = tip;
        if(tip==="modalGecmisVisible"){
            axios.post(ajaxURL+'/adayuye/adayUyeMesajListesi',{adayuyeid:userid}).then(function(response){
                if(response.data.status===true){
                    state.adayUyeList[sira][tip]=true;
                    state.dataSource=that.gecmisiYazdir(response);
                    that.setState(state);
                }else{
                    state.dataSource=[];
                    state.adayUyeList[sira][tip]=false;
                    that.setState(state);
                    message.error("Aday Üye ile Daha önce hiç görüşülmemiş");
                }
              });
        }
        if(tip==="modalNotVisible"){
            state.adayUyeList[sira][tip]=true;
            state.gorusmeMesaj="";
            state.randevuTarihi="";
            this.setState(state);
        }
        
      }
      handleOk = (e) => {
        debugger;
        var sira = parseInt(this.state.activePanel);
        var state = this.state;
        var tip = state.activeModal;
        if(tip==="modalGecmisVisible"){
            state.adayUyeList[sira].modalGecmisVisible=false;
        }
        if(tip==="modalNotVisible"){
            state.adayUyeList[sira].modalNotVisible=false;
        }
        state.activeUser=null;
        state.activePanel=null;
        this.setState(state);
      }
      handleCancel = (e) => {
        debugger;
        var sira = parseInt(this.state.activePanel);
        var state = this.state;
        var tip = state.activeModal;
        if(tip==="modalGecmisVisible"){
            state.adayUyeList[sira].modalGecmisVisible=false;
        }
        if(tip==="modalNotVisible"){
            state.adayUyeList[sira].modalNotVisible=false;
        }
        state.activeUser=null;
        state.activePanel=null;
        this.setState(state);
      }

      sonrakiRandevu = (date,dateString) => {
        var deger = dateString;
        this.state.randevuTarihi=deger;
        this.setState(this.state); 
      }

      mesajYazisiDegis = (e) => {
        var deger = e.target.value;
        this.state.gorusmeMesaj=deger;
        this.setState(this.state);
      }
      
      mesajEkle = () => {
          debugger;
        var that = this;
        var sira = parseInt(this.state.activePanel);
        var userid = parseInt(this.state.activeUser);
        var tip = this.state.activeModal;
        var messages = this.state.gorusmeMesaj;
        var tarih = this.state.randevuTarihi;
        var state = this.state;
        var data ={adayuyeid:userid,mesaj:messages,randevu:tarih,personelid:1}; 
       
        axios.post(ajaxURL+'/adayuye/mesajEkle',data).then(function(response){
            if(response.data.status===true){
                message.success('Mesajınız ve İlgili Tarih için Randevunuz Kayıt Altına Alınmıştır');
                state.gorusmeMesaj="";
                state.randevuTarihi="";
                state.adayUyeList[sira][tip]=false;
                state.dataSource=[];
                that.setState(state);
                that.uploadRandevuluUyeler(that);
            }else{
                message.error("Aday Üye İle Yapılan Görüşme Kayıt Edilememiştir");
                state.dataSource=[];
                state.adayUyeList[sira][tip]=true;
                that.setState(state);
            }
          }).catch(function(err){
              debugger;
              message.error(err.message);
          });

      }

      confirm(e) {
          debugger;
          message.success('Aday Üye Artık Gerçek Bir Üye Olarak Kayıt Edilmiştir');
      }
      
      cancel(e) {
          message.warning('Herangi Bir Değişiklik Yapılmadı');
      }

      adayUyeyiSistemeKaydet = (e) => {
          debugger;
          var userid = parseInt(e.target.attributes[0].nodeValue);
          var sira = parseInt(e.target.attributes[1].nodeValue);
          var paket = this.state.paketSecimi;
          var tc = this.state.tckimlikNo;
          var ikametgah = this.state.ikametgah;
          var taksitler = this.state.taksitler;
          var kayittarihi = this.state.kayitTarihi;
          var ortaAd = this.state.ortaAd;

          if(userid!==0 && paket!==0 && tc!=="" && taksitler.length>0 && ikametgah!=="" && kayittarihi!==""){
                paket=parseInt(paket);
                var that = this;
                var data = {userid:userid,ortaad:ortaAd,taksitler:taksitler,tc:tc,paket:paket,ikametgah:ikametgah,kayit:kayittarihi}
                debugger;
                axios.post(ajaxURL+'/adayuye/adayiUyeYap',data).then(function(response){
                    that.uploadAdayUyeler(that);
                    that.uploadRandevuluUyeler(that);
                    if(response.data.status===true){
                        var state = that.state;
                        state.paketSecimi=0;
                        state.taksitler=[];
                        state.ikametgah="";
                        state.ortaAd="";
                        state.tckimlikNo="";
                        state.kayitTarihi="";
                        that.setState(state);
                        message.success("Üyelik Aktifleştirilmiştir - "+response.data.message);
                    }else{
                        message.error(response.data.message);
                    }
                });
          }else{

            if(userid==0){
                message.error("Kullanıcı Doğru Seçilmemiş");
            }
  
            if(paket==0){
              message.error("Paketi Seçilmemiş");
            }

            if(tc==""){
              message.error("TC Kimlik Numarası Girilmemiş");
            }

            if(taksitler.length==0){
              message.error("Taksitleri Belirtilmemiş");  
            }

          }

          
      }

      paketSec = (e) => {
          debugger;
        var state = this.state;
        state.paketSecimi = parseInt(e);
        this.setState(state);
      }

      uyeKayitTarihiChange(date,dateString){
        var state = this.state;
        state.kayitTarihi = dateString;
        this.setState(state);
      }

      uyeTcOnChange(deger){
          var state = this.state;
          state.tckimlikNo = deger;
          this.setState(state);
      }
      ortaAdOnChange(deger){
        debugger;
          var state = this.state;
          state.ortaAd = deger.target.value;
          this.setState(state);
      }
      taksitMiktariChange(deger){
          if(deger!==""){
            var state = this.state;
            state.taksitmiktari = parseFloat(deger);
            this.setState(state);
          }else{
            var state = this.state;
            state.taksitmiktari="";
            this.setState(state);
          }
      }
      taksitTarihiOnChange(date){
          var dateString = date.target.value;
          var state = this.state;
          state.taksitTarihi = dateString;
          this.setState(state);
      }

      taksitEkle(){
          var state = this.state;
          var taksitler = state.taksitler;
          var taksitmiktari = state.taksitmiktari;
          var taksitTarihi = state.taksitTarihi;
          if(taksitmiktari>0){
            taksitler.push({miktar:taksitmiktari,tarih:taksitTarihi});
            state.taksitmiktari=0;
            message.success(taksitTarihi+" Tarihi İçin Taksit Miktarı : "+taksitmiktari+"TL Olarak Eklediniz");
            this.setState(state);
          }else{
              message.warning("Lütfen Taksit Miktarını Doğru Giriniz");
          }
          
      }

      ikametgahYazisiDegis(deger){
          debugger;
          var state = this.state;
          state.ikametgah = deger.target.value;
          this.setState(state);
      }

      taksitSil(sira,ths){
        var state = this.state;
        var taksitler = state.taksitler;
        taksitler.splice(sira,1);
        state.taksitler = taksitler;
        this.setState(state);
      }

      popoverContent(userid,sira){
            var paketlist = this.state.paketList;
            var optionss = paketlist.map((item,sira)=><Option value={item.id}>{item.isim}</Option>);
            let sira2 = sira;
            let userid2 = userid;

            const taksitler = this.state.taksitler.map((item,sira)=>{
                return(
                    <Row gutter="10" style={{marginBottom:"10px"}}>
                        <Col lg={10} md={10} sm={10} xs={10}>
                           Miktar :  {item.miktar}TL
                        </Col>
                        <Col lg={10} md={10} sm={10} xs={10}>
                            Tarih : {item.tarih} 
                        </Col>
                        <Col style={{textAlign:"right"}} lg={4} md={4} sm={4} xs={4}>
                            <Button type="error" onClick={this.taksitSil.bind(this,sira)} shape="circle" icon="minus" />
                        </Col>
                    </Row>
                );
            });
        
        

            return (
                <p> 
                    <Row style={{marginBottom:"10px"}}>
                        <Col lg={24} md={24} sm={24} xs={24}>
                            <DatePicker style={{width:"100%"}} placeholder="Kesin Kayıt Tarihini Giriniz" defaultValue={moment(this.state.kayitTarihi=="" ? this.state.simdikiZaman:this.state.kayitTarihi, "YYYY-MM-DD")} onChange={this.uyeKayitTarihiChange.bind(this)} />
                        </Col>
                    </Row>
                    <Row style={{marginBottom:"10px"}}>
                        <Col lg={24} md={24} sm={24} xs={24}>
                            <InputNumber style={{width:"100%"}} placeholder="Üyenin TC Kimlik Bilgisini Giriniz" value={this.state.tckimlikNo} onChange={this.uyeTcOnChange.bind(this)} />
                        </Col>
                    </Row>
                    <Row style={{marginBottom:"10px"}}>
                        <Col lg={24} md={24} sm={24} xs={24}>
                            <Input style={{width:"100%"}} placeholder="Üyenin Orta Adı Varsa Giriniz" value={this.state.ortaAd} onChange={this.ortaAdOnChange.bind(this)} />
                        </Col>
                    </Row>
                    <Row style={{marginBottom:"10px"}}>
                        <Col lg={24} md={24} sm={24} xs={24}>
                            <Select userid={userid} sira={sira} onChange={this.paketSec.bind(this)} style={{width:"100%"}} defaultValue="Paket Seçiniz">{optionss}</Select>
                        </Col>
                    </Row>
                    <Row style={{marginBottom:"10px"}}>
                        <Col lg={24} md={24} sm={24} xs={24}>
                            <h3>Taksitlendirme Aralığı Tanımlayınız </h3>
                        </Col>
                    </Row>
                    <Row style={{marginBottom:"10px"}}>
                        <Col lg={10} md={10} sm={10} xs={10}>
                            <InputNumber style={{width:"100%"}} placeholder="Taksit Miktarı Giriniz" value={this.state.taksitmiktari} onChange={this.taksitMiktariChange.bind(this)}></InputNumber>
                        </Col>
                        <Col lg={10} md={10} sm={10} xs={10}>
                            <input onChange={this.taksitTarihiOnChange.bind(this)} style={{border:"1px solid #d9d9d9",padding:"3px",WebkitBorderRadius:"5px",borderRadius:"5px"}} type="date" placeholder="Lütfen Tarih Seçiniz" />
                        </Col>
                        <Col style={{textAlign:"right"}} lg={4} md={4} sm={4} xs={4}>
                            <Button type="primary" onClick={this.taksitEkle.bind(this)} shape="circle" icon="plus" />
                        </Col>
                    </Row>
                    {taksitler}
                    <Row style={{marginBottom:"10px"}}>
                        <Col lg={24} md={24} sm={24} xs={24}>
                        <TextArea onChange={this.ikametgahYazisiDegis.bind(this)}  value={this.state.ikametgah} placeholder="İkametgah Adresini Yazınız" rows={4} />
                        </Col>
                    </Row>
                    <Row>
                        <Col lg={12} md={12} sm={12} xs={12} offset={12} style={{textAlign:"right"}}>
                            <Button userid={userid} sira={sira} onClick={this.adayUyeyiSistemeKaydet.bind(this)} type="primary">Kaydet</Button>
                        </Col>
                    </Row>
                </p>);
      }


    render(){
        const that = this;

        const style = {
            rowStyle: {width: '100%',display: 'flex',flexFlow: 'row wrap'},
            colStyle:{marginBottom: '16px'},
            gutter:16,
            cardStyle : { padding: "10px",marginBottom:"10px" },
            cardName : {color:"#00BCD4"},
            cardHeader : {color:"#8bc34a",fontSize:"11px"},
            cardNameValue:{color:"#bbbbbb",fontSize:"11px"},
            cardValue:{color:"#bbbbbb"},
            butonGroup:{marginTop:"10px"},
            h3Name:{color:"#666"},
            modalBody:{fontSize:"14px",lineHeight:"1.5",wordWrap:"break-word",padding:"0"}
        };
    
        const adayUyeListesi = this.state.adayUyeList.map((item,sira) =>{
            return <Col lg={6} md={8} sm={12} xs={24}  style={style.colStyle}>
                <Card bodyStyle={style.cardStyle} actions={[
                    <span sira={sira} userid={item.id} tip="modalGecmisVisible" isim={item.isim} onClick={that.showModal.bind(that,sira,item.id)}>Geçmiş</span>,
                    <span sira={sira} userid={item.id} tip="modalNotVisible" isim={item.isim} onClick={that.showModal.bind(that)}>Ekle</span>,
                    <Popover trigger="click" content={that.popoverContent(item.id,sira)} title="Aday Üyeyi Gerçek Üyeliğe Çevirmeye Emin misiniz ?">
                    <span sira={sira} userid={item.id} tip="modalAktifVisible" isim={item.isim} >İşlem</span>
                    </Popover>
                    ]}>
                        <Modal
                            title={that.state.activeUserName+" ile Geçmiş Görüşmelerin Dökümü"}
                            visible={item.modalGecmisVisible}
                            onOk={that.handleOk.bind(that)}
                            onCancel={that.handleCancel.bind(that)}
                            okText="Anladım"
                            cancelText="İptal"
                            bodyStyle={style.modalBody}
                            >
                            <Table dataSource={that.state.dataSource} columns={that.state.columns} />
                        </Modal>
                        <Modal
                            title={that.state.activeUserName+" ile Görüşmeden Kayıt Edilecek Notlar"}
                            visible={item.modalNotVisible}
                            onOk={that.mesajEkle.bind(that)}
                            onCancel={that.handleCancel.bind(that)}
                            okText="Kaydet"
                            cancelText="İptal"
                            >
                            <TextArea onChange={that.mesajYazisiDegis.bind(that)}  value={that.state.gorusmeMesaj} placeholder="Görüşme ile ilgili Bilgilendirmeyi Yazınız" rows={4} />
                            <p>
                            <DatePicker style={{width:"100%",marginTop:"10px"}} defaultValue={that.state.randevuTarihi} onChange={that.sonrakiRandevu.bind(that)} placeholder="Bir Sonraki Randevu Tarihini Seçiniz" />
                            </p>
                        </Modal>
                    <Row>
                        <Col md={24} sm={24} xs={24}><h3 style={style.h3Name}>{item.isim}</h3></Col>
                    </Row>
                    <Row>
                        <Col md={6} sm={6} xs={6} style={style.cardHeader}>E-mail</Col>
                        <Col md={18} sm={18} xs={18} style={style.cardNameValue}>{item.mail}</Col>
                    </Row>
                    <Row>
                        <Col md={6} sm={6} xs={6} style={style.cardHeader}>Telefon</Col>
                        <Col md={18} sm={18} xs={18} style={style.cardNameValue}>{item.telefon}</Col>
                    </Row>
                    <Row>
                        <Col md={6} sm={6} xs={6} style={style.cardHeader}>Kayıt Tarihi</Col>
                        <Col md={18} sm={18} xs={18} style={style.cardNameValue}>{new Date(item.createdAt).toDateString()}</Col>
                    </Row>
                    <Row>
                        <Col md={6} sm={6} xs={6} style={style.cardHeader}>Not</Col>
                        <Col md={18} sm={18} xs={18} style={style.cardNameValue}>{item.notlar}</Col>
                    </Row>
                </Card>
            </Col>
            }
        );


        const randevuUyeListesi = this.state.randevuUyeList.map((item,sira) =>{
            return <Col lg={6} md={8} sm={12} xs={24}  style={style.colStyle}>
                <Card bodyStyle={style.cardStyle} actions={[
                    <span sira={sira} userid={item.id} tip="modalGecmisVisible" isim={item.isim} onClick={that.showModal.bind(that)}>Geçmiş</span>,
                    <span sira={sira} userid={item.id} tip="modalNotVisible" isim={item.isim} onClick={that.showModal.bind(that)}>Ekle</span>,
                    <Popover trigger="click" content={that.popoverContent(item.id,sira)} title="Aday Üyeyi Gerçek Üyeliğe Çevirmeye Emin misiniz ?">
                    <span sira={sira} userid={item.id} tip="modalAktifVisible" isim={item.isim} >İşlem</span>
                    </Popover>
                    ]}>
                        <Modal
                            title={that.state.activeUserName+" ile Geçmiş Görüşmelerin Dökümü"}
                            visible={item.modalGecmisVisible}
                            onOk={that.handleOk.bind(that)}
                            onCancel={that.handleCancel.bind(that)}
                            okText="Anladım"
                            cancelText="İptal"
                            bodyStyle={style.modalBody}
                            >
                            <Table dataSource={that.state.dataSource} columns={that.state.columns} />
                        </Modal>
                        <Modal
                            title={that.state.activeUserName+" ile Görüşmeden Kayıt Edilecek Notlar"}
                            visible={item.modalNotVisible}
                            onOk={that.mesajEkle.bind(that)}
                            onCancel={that.handleCancel.bind(that)}
                            okText="Kaydet"
                            cancelText="İptal"
                            >
                            <TextArea onChange={that.mesajYazisiDegis.bind(that)}  value={that.state.gorusmeMesaj} placeholder="Görüşme ile ilgili Bilgilendirmeyi Yazınız" rows={4} />
                            <p>
                            <DatePicker style={{width:"100%",marginTop:"10px"}} defaultValue={that.state.randevuTarihi} onChange={that.sonrakiRandevu.bind(that)} placeholder="Bir Sonraki Randevu Tarihini Seçiniz" />
                            </p>
                        </Modal>
                    <Row>
                        <Col md={24} sm={24} xs={24}><h3 style={style.h3Name}>{item.isim}</h3></Col>
                    </Row>
                    <Row>
                        <Col md={6} sm={6} xs={6} style={style.cardHeader}>E-mail</Col>
                        <Col md={18} sm={18} xs={18} style={style.cardNameValue}>{item.mail}</Col>
                    </Row>
                    <Row>
                        <Col md={6} sm={6} xs={6} style={style.cardHeader}>Telefon</Col>
                        <Col md={18} sm={18} xs={18} style={style.cardNameValue}>{item.telefon}</Col>
                    </Row>
                    <Row>
                        <Col md={6} sm={6} xs={6} style={style.cardHeader}>Kayıt Tarihi</Col>
                        <Col md={18} sm={18} xs={18} style={style.cardNameValue}>{new Date(item.createdAt).toDateString()}</Col>
                    </Row>
                </Card>
            </Col>
            }
        );

       

        return (
        <LayoutWrapper>
            <Breadcrumb style={{backgroundColor:"#fff",padding:"10px",width:"100%",marginBottom:"5px"}}>
                <Breadcrumb.Item>Personel Paneli</Breadcrumb.Item>
                <Breadcrumb.Item>Aday Üye Havuzu</Breadcrumb.Item>
            </Breadcrumb>
            <Card title="Randevusu Yaklaşan Aday Üyeler - (Son 3 Gün)" style={{width:"100%"}} >
                <Row style={style.rowStyle} gutter={style.gutter} justify="start">
                    {randevuUyeListesi}
                </Row>
            </Card>
            <Card title="Üyelik Başvurusu Yapan Adaylar" style={{width:"100%"}} >
                <Row style={style.rowStyle} gutter={style.gutter} justify="start">
                    {randevuUyeListesi}
                </Row>
            </Card>
        </LayoutWrapper>
        );
    }
}

export default Adayuyellistesi;