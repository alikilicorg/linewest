import React, { Component } from "react";
import LayoutWrapper from '../../../components/utility/layoutWrapper.js';
import PageHeader from '../../../components/utility/pageHeader';
import { Form, Input, Tooltip, Icon, Upload, Modal, Cascader, Select, Row, Col, Checkbox, Radio, Breadcrumb, Button, AutoComplete, Card, Table, DatePicker, InputNumber, message, Popconfirm } from 'antd';
import config2 from '../../../settings/config';
import axios from 'axios';
import moment from 'moment';
import matchSorter from 'match-sorter'
import ReactTable from "react-table";
import "../../css/react-table.css";
const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;
const { TextArea } = Input;
const { RangePicker } = DatePicker;
const ajaxURL = config2.http+config2.server+':'+config2.port;

function renderOption(item) {
    return (
      <Option key={item.id} text={item.isim}>{item.isim}</Option>
    );
}

class Uyelistesi extends Component {
    constructor(){
        super();
        this.state={
            user:localStorage.getItem('user')!==null ? JSON.parse(localStorage.getItem('user')):{id:-1,durum:false,type:"user"},
            /* edituser:localStorage.getItem('editUser')!==null ? JSON.parse(localStorage.getItem('editUser')):{id:1,durum:true}, */
            edituser:window.editUser,
            yetkiler:{
                sayfa:{id:34,message:"Sayfayı Açma Yetkiniz Yoktur",durum:false}
            },
            userDataSource:[],
            tableData:[]
        };
    }

    userArama = (value) => {
        var that = this;
        var personelid = this.state.user.id;
        if(this.state.user.type=="personel"){
            if(value!==""){
                axios.post(ajaxURL+'/user/userbilgisearch',{search:value,personelid:personelid}).then(function(response){
                    if(response.data.status==true){
                        debugger;
                      var state = that.state;
                      if(that.state.user.yetkiler.indexOf(26)>-1){
                        state.userDataSource = response.data.result;
                      }else{
                        state.userDataSource=[];
                      for(var i in response.data.result){
                          if(response.data.result[i].personel===personelid){
                            state.userDataSource.push(response.data.result[i]);
                          }
                        }
                      }
                      
                      that.setState(state);
                    }else{
                        message.warning(response.data.message);
                    }
                  });
            }else{
                message.warning("Kulanıcı Arama Kutusu Boş Bırakılamaz");
            }
        }else{
            message.warning("Sadece Personeller Arama Yapabilir");
        }
    }

    userSecme = (value) => {
        var userid = parseInt(value);
        var state = this.state;
        state.edituser.id=userid;
        state.edituser.durum=false;
        state.form.deneyimler=[];
        state.form.eskideneyimler=[];
        state.form.isdeneyimi.disabled=true;
        state.template = {sirket:"",adres:"",referans:"",telefon:"",pozisyon:"",aciklama:"",baslangic:"",bitis:""};
        for(var i in state.formDurum){
            state.formDurum[i]=true;
        }
        this.setState(state);
        this.bilgileriCek(false);
        
    }

    yetkiKontrol(useryetkilist){
        var state = this.state;
        for(var i in state.yetkiler){
            if(useryetkilist.indexOf(state.yetkiler[i].id)>-1){
                state.yetkiler[i].durum=true;
            }
        }
        this.setState(state);
        this.userDataCek();
    }

    yetkileriGetir(){
        var that = this;
        var ekURL = "";
        var data = {};
        if(this.state.user.type==="personel"){
          ekURL = "/yetkilendirme/personelTumYetkiler"
          data["personelid"]=this.state.user.id;
        }else{
          ekURL = "/yetkilendirme/userTumYetkiler"
          data["userid"]=this.state.user.id;
        }
        axios.post(ajaxURL+ekURL,data).then(function(gelen){
          var state = that.state;
          state.user.yetkiler = gelen.data.result;
          that.setState(state);
          that.yetkiKontrol(gelen.data.result);
        }).catch(function(hata){
          message.error(hata.message);
        });
    }

    bilgileriCek(){
        var userid = this.state.edituser.id;
        var that = this;
        if(this.state.yetkiler.sayfa.durum){
            if(userid>0){
                this.yetkileriGetir();
            }else{
                message.warning("Eğitim Bilgileri çekmek için tanımlı bir kullanıcıya rastlanamadı");
            }
            
        }
    }

    userDataCek(){
        var that = this;
        axios.get(ajaxURL+"/userbilgi/list1").then(function(gelen){
            var state = that.state;
            var dizi = [];
            gelen.data.result.map((item)=>{
                dizi.push({
                    userid:item.userid,
                    adsoyad:item.ortaad!=="" ? item.ad+" "+item.ortaad+" "+item.soyad:item.ad+" "+item.soyad,
                    cinsiyet:item.cinsiyet===null ? "yok":item.cinsiyet,
                    mail:item.mail,
                    telefon:item.telefon,
                    kayit:moment(item.kayit).format('DD.MM.YYYY')
                });
            });
            state.tableData =dizi; 
            that.setState(state);
          }).catch(function(hata){
            message.error(hata.message);
          });
    }

    componentWillMount(){
        debugger;
        this.yetkileriGetir();
    }

    kisiYonet(val,ths){
        debugger;
        localStorage.setItem("editUser", JSON.stringify({id:val.userid,durum:true,isim:val.adsoyad}));
        window.editUser={id:val.userid,durum:true,isim:val.adsoyad};
        var state = this.state;
        state.edituser = {id:val.userid,durum:true,isim:val.adsoyad};
        document.getElementById('personelYonetim').innerHTML= 'Yönetilen Öğrenci : '+state.edituser.isim;
        message.success(val.adsoyad+" Kişisinin Paneli Yönetilmek İçin Seçilmiştir");
        window.open(config2.http+config2.localhost+":"+config2.localport+"/dashboard/work-and-travel-ilk-adim-kisiel-bilgiler");
    }

    render(){
        const that = this;
        const { tableData } = this.state;
        const formItemLayout = {labelCol: {xs: { span: 24 },sm: { span: 6 },},wrapperCol: {xs: { span: 24 },sm: { span: 14 },},style:{marginBottom:"15px"}}
        const formItemLayoutnostyle = {labelCol: {xs: { span: 24 },sm: { span: 6 },},wrapperCol: {xs: { span: 24 },sm: { span: 14 },}}
        const dateFormat = 'YYYY-MM-DD';

        if(this.state.yetkiler.sayfa.durum===true){
            return(
                <LayoutWrapper>
                    <Breadcrumb style={{backgroundColor:"#fff",padding:"10px",width:"100%",marginBottom:"5px"}}>
                        <Breadcrumb.Item>Personel Paneli</Breadcrumb.Item>
                        <Breadcrumb.Item>Üye Listesi</Breadcrumb.Item>
                    </Breadcrumb>
                    <Row style={{width:"100%"}}>
                        <Col lg={24} md={24} sm={24} xs={24}>
                            <Card title="Üye Listesi" bodyStyle={{padding:"10px"}}>
                            <ReactTable
                                data={tableData}
                                filterable
                                defaultFilterMethod={(filter, row) =>
                                    String(row[filter.id]) === filter.value}
                                columns={[
                                    {
                                    columns: [
                                        {
                                            Header: "ID", 
                                            accessor:"userid",
                                            filterMethod: (filter, rows) =>
                                            matchSorter(rows, filter.value, { keys: ["userid"] }),
                                            filterAll: true,
                                            width:60
                                        },
                                        {
                                            Header: "İsim", 
                                            accessor:"adsoyad",
                                            filterMethod: (filter, rows) =>
                                            matchSorter(rows, filter.value, { keys: ["adsoyad"] }),
                                            filterAll: true
                                        },
                                        {
                                            Header: "Cinsiyet",
                                            accessor: "cinsiyet",
                                            id: "cinsiyet",
                                            width:100,
                                            Cell: ({ value }) => (value == 1 ? "Erkek" : "Kadın"),
                                            filterMethod: (filter, row) => {
                                                debugger;
                                                if (filter.value === "all") {
                                                    return true;
                                                  }
                                                  if (filter.value === "1") {
                                                    return row[filter.id] == 1;
                                                  }
                                                  if (filter.value === "2") {
                                                    return row[filter.id] == 2;
                                                  }
                                            },
                                            Filter: ({ filter, onChange }) =>
                                                <select
                                                onChange={event => onChange(event.target.value)}
                                                style={{ width: "100%" }}
                                                value={filter ? filter.value : "all"}
                                                >
                                                <option value="all">Hepsi</option>
                                                <option value="1">Erkek</option>
                                                <option value="2">Kadın</option>
                                                <option value="yok">Bilinmiyon</option>
                                                </select>
                                        },
                                        {
                                            Header: "E-mail", 
                                            accessor:"mail",
                                            filterMethod: (filter, rows) =>
                                            matchSorter(rows, filter.value, { keys: ["mail"] }),
                                            filterAll: true
                                        },
                                        {
                                            Header: "Telefon", 
                                            accessor:"telefon",
                                            filterMethod: (filter, rows) =>
                                            matchSorter(rows, filter.value, { keys: ["telefon"] }),
                                            filterAll: true
                                        },
                                        {
                                            Header: "Kayıt Tarihi", 
                                            accessor:"kayit",
                                            filterMethod: (filter, rows) =>
                                            matchSorter(rows, filter.value, { keys: ["kayit"] }),
                                            filterAll: true
                                        },
                                        {
                                            Header: "Öğrenciyi Yönet",
                                            filterAll: false,
                                            Cell:({ row }) => (<Button type="primary" size="small" style={{borderRadius:"5px",width:"100%"}} onClick={this.kisiYonet.bind(this,row)}>Yönet</Button>)
                                        }
                                        
                                    ]
                                    }
                                ]}
                                defaultPageSize={10}
                                className="-striped -highlight"
                                />
                            </Card>
                        </Col>
                    </Row>
                </LayoutWrapper>
            );
        }else{
            return (<LayoutWrapper/>);
        }
    }
}

export default Uyelistesi;
