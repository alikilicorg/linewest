import React, { Component } from "react";
import { Route } from "react-router-dom";
import asyncComponent from "../../helpers/AsyncFunc";
import customRoutes from "../../customApp/router";

const routes = [
    {
        path:"work-and-travel-sistem-imzasi",
        component: asyncComponent(() => import("../Workandtravel/Sistemimzasi/sistemimzasi"))
    },
    {
        path:"work-and-travel-cv-olusturma",
        component: asyncComponent(() => import("../Workandtravel/Cvolusturma/cvolusturma"))
    },
    {
        path:"work-and-travel-is-deneyimleri",
        component: asyncComponent(() => import("../Workandtravel/Isdeneyimleri3/isdeneyimleri"))
    },
    {
        path:"work-and-travel-egitim-bilgileri",
        component: asyncComponent(() => import("../Workandtravel/Egitimbilgileri2/egitimbilgierli"))
    },
    {
        path:"work-and-travel-ilk-adim-kisiel-bilgiler",
        component: asyncComponent(() => import("../Workandtravel/Kisiselbilgiler1/kisiselbilgiler"))
    },
    {
        path:"adayuyeler",
        component: asyncComponent(() => import("../Personel/Adayuyeler/adayuyeler"))
    },
    {
        path:"uye-listesi",
        component: asyncComponent(() => import("../Personel/Uyelistesi/uyelistesi"))
    },
    {
        path:"yetkilendirme",
        component: asyncComponent(() => import("../Personel/Yetkilendirme/yetkilendirme"))
    },
    {
        path:"workandtravel-bilmeniz-gerekenler",
        component: asyncComponent(() => import("../Workandtravel/Bilmenizgerekenler/Workandtravel"))
    },
    {
        path: "sozlesmeyukle",
        component: asyncComponent(() => import("../Workandtravel/Sozlesme/sozlesme"))
    },
    {
        path: "anasayfa",
        component: asyncComponent(() => import("../Anasayfa/AnasayfaView"))
    },
    {
        path: "imzahazirla",
        component: asyncComponent(() => import("../Imzayukleme/imzahazirla"))
    },
    {
        path: "imzayukle",
        component: asyncComponent(() => import("../Imzayukleme/imzayukleme"))
    }, 
    ...customRoutes
];

class AppRouter extends Component {
    render() {
        const { url, style } = this.props;
        return (
            <div style={style}>
                {routes.map(singleRoute => {
                    const { path, exact, ...otherProps } = singleRoute;
                    return (
                        <Route
                            exact={exact === false ? false : true}
                            key={singleRoute.path}
                            path={`${url}/${singleRoute.path}`}
                            {...otherProps}
                        />
                    );
                })}
            </div>
        );
    }
}

export default AppRouter;
