// FN : 1526235761022
import React, { Component } from "react";
import { connect } from "react-redux";
import Card from '../../components/uielements/card';
import Konva from 'konva';
import download from 'downloadjs';
import Progressbar from '../../components/uielements/progress'
import Button, { ButtonGroup } from '../../components/uielements/button'
import Input from '../../components/uielements/input';
import LayoutWrapper from '../../components/utility/layoutWrapper.js';
import PageHeader from '../../components/utility/pageHeader';
import config2 from '../../settings/config';
import axios from 'axios';
import { Col, Row, message,Breadcrumb } from 'antd';

const ajaxURL = config2.http+config2.server+':'+config2.port;

class Imzahazirla extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user:{id:1,type:"personel",yetkiler:[]},
            /* edituser:localStorage.getItem('editUser')!==null ? JSON.parse(localStorage.getItem('editUser')):{id:1,durum:true}, */
            edituser:window.editUser,
            yetkiler:{
                sayfa:{id:8,message:"Sayfayı Açma Yetkiniz Yoktur",durum:false}
            },
            imza:{
                transparent:40,
                cropStart:[0,0],
                cropFinish:[0,0],
                rect:[]
            },
            imzaDuzenleStatus:"none",
            progress:0,
            cropDrawStatus:"none",
            cropDrawStatus2:"inherit",
        };

        this.konva = false;
        this.konvayeni = false;
        this.layer = false;
        this.layeryeni = false;
        this.cerceve = false;
        this.scaleNokta=false;
        this.orjinalimza = false;
        this.yeniimza = false;
        this.genelData = false;
    }

    imzaYukle = (e) => {
        var that = this;
        var file = e.target.files[0];
        var canvasName = e.target.attributes.canvas.nodeValue;
        var state = this.state;
        state.progress=50;
        this.setState(state);
        var reader = new FileReader();
        reader.onload = function(event){
            var img = new Image();
            img.onload = function(){
                state.progress=100;
                state.imzaDuzenleStatus="flex";
                that.setState(state);
                debugger;
                that.konva = new Konva.Stage({
                    container:canvasName,
                    width: img.width,
                    height: img.height
                });
                that.konvayeni = new Konva.Stage({
                    container:"imzayeni",
                    width: img.width,
                    height: img.height
                });
                that.layer = new Konva.Layer();
                that.layeryeni = new Konva.Layer();

                that.orjinalimza = new Konva.Image({
                    x: 0,
                    y: 0,
                    image: img,
                    width: img.width,
                    height: img.height
                });

                that.yeniimza = new Konva.Image({
                    x: 0,
                    y: 0,
                    image: img,
                    width: img.width,
                    height: img.height
                });

                that.layer.add(that.orjinalimza);
                that.konva.add(that.layer);

                that.layeryeni.add(that.yeniimza);
                that.konvayeni.add(that.layeryeni);
            };
            img.src = event.target.result;
        };
        reader.readAsDataURL(file);
    };

    fotografKes = (e) =>{
        debugger;
        var state = this.state;
        state.cropDrawStatus="inherit";
        state.cropDrawStatus2="none";
        this.setState(state);
        this.cerceve = new Konva.Rect({
            draggable:true,
            x:5,y:5,width:160,height:90,
            fill:"black",opacity:0.3,stroke:"red",strokeWidth:2
        });
        this.cerceve.on('dragend',this.dragEndCerceve);
        this.cerceve.on('dragmove',this.dragCerceve);
        this.scaleNokta = new Konva.Circle({
            x: 165,
            y: 95,
            radius: 4,
            fill: 'green',
            stroke: 'black',
            strokeWidth: 1,
            opacity:0.5,
            draggable:true
        });
        this.scaleNokta.on('dragmove',this.dragNokta);
        this.layer.add(this.cerceve);
        this.layer.add(this.scaleNokta);
        this.konva.add(this.layer);
    };

    dragNokta = (e) => {
        debugger;
        var cer = this.cerceve;
        var info = e.currentTarget.attrs;
        var w = Math.abs(info.x-cer.attrs.x);
        var h = Math.abs(info.y-cer.attrs.y);
        var startx=cer.attrs.x;
        var starty=cer.attrs.y;
        if(info.x<cer.attrs.x){startx=info.x;}
        if(info.y<cer.attrs.y){starty=info.y;}
        this.cerceve.position({x:startx,y:starty});
        this.cerceve.size({
            width:w,
            height:h
        });

    };

    dragCerceve = (e) => {
        var info = e.currentTarget.attrs;
        var x = info.x+info.width;
        var y = info.y+info.height;
        this.scaleNokta.position({x:x,y:y});
    };

    dragEndCerceve = (e) => {
        var info = e.currentTarget.attrs;
        var x = info.x+info.width;
        var y = info.y+info.height;
        this.scaleNokta.position({x:x,y:y});
    };
    fotografKesimKaydet = (e) =>{
        debugger;
        var state = this.state;
        state.cropDrawStatus="none";
        state.cropDrawStatus2="inherit";
        this.setState(state);
        var cerceve = this.cerceve;
        this.yeniimza.crop({
            x: cerceve.attrs.x,
            y: cerceve.attrs.y,
            width: cerceve.attrs.width,
            height: cerceve.attrs.height
        });
        this.cerceve.destroy();
        this.cerceve=false;
        this.scaleNokta.destroy();
        this.scaleNokta=false;
        this.layer.draw();
        this.layeryeni.draw();
        this.yeniimza.position({x:0,y:0});
        this.yeniimza.size({
            width:cerceve.attrs.width,
            height:cerceve.attrs.height
        });
        this.konvayeni.setHeight(cerceve.attrs.height);
        this.konvayeni.setWidth(cerceve.attrs.width);
    };
    setTransparent = (e) => {
        var canvas = this.konvayeni.attrs.container.children[0].children[0];
        var ctx = canvas.getContext('2d');
        this.genelData = this.genelData || ctx.getImageData(0,0,canvas.width,canvas.height);
        ctx.clearRect(0,0,canvas.width,canvas.height);
        var data2 = ctx.getImageData(0,0,canvas.width,canvas.height);
        const islem = e.target.attributes.math.nodeValue;
        const state = this.state;
        var transparent = state.imza.transparent;
        if(islem==="+5"){
            transparent+=5;
        }else{
            transparent-=5;
        }
        if(transparent<0){transparent=0;}
        if(transparent>255){transparent=255;}
        state.imza.transparent=transparent;
        this.setState(state);
        for(var i=0;i<this.genelData.data.length;i=i+4){
            var ri = i; var gi=i+1; var bi=i+2; var oi=i+3;
            var r = this.genelData.data[ri]; var g = this.genelData.data[gi]; var b = this.genelData.data[bi];
            var ru = this.uygunaralik(r,transparent);
            var gu = this.uygunaralik(g,transparent);
            var bu = this.uygunaralik(b,transparent);
            if(ru===true && gu===true && bu===true){
                data2.data[ri]=255;
                data2.data[gi]=255;
                data2.data[bi]=255;
                data2.data[oi]=0;
            }else{
                data2.data[ri]=r;
                data2.data[gi]=g;
                data2.data[bi]=b;
                data2.data[oi]=255;
            }

        }
        ctx.putImageData(data2,0,0);
    };

    renkdolas = (canvas,aralik) =>{
        var ctx = canvas.getContext('2d');
        var width = canvas.width;
        var height = canvas.height;
        for(let i=0;i<width;i++){
            for(let j=0;j<height;j++){
                var c = ctx.getImageData(i, j, 1, 1).data;
                var r=c[0]; var ru = this.uygunaralik(r,aralik);
                var g=c[1]; var gu = this.uygunaralik(g,aralik);
                var b=c[2]; var bu = this.uygunaralik(b,aralik);
                if(ru===true && gu===true && bu===true){
                    this.drawPixel(ctx,i,j,r,g,b,0);
                }else{
                    this.drawPixel(ctx,i,j,r,g,b,1);
                }
            }
        }
    };

    uygunaralik = (val,limit) => {
        const enaz = 255-limit;
        if(255>=val && val>=enaz){
            return true;
        }else{
            return false;
        }
    };

    drawPixel = (ctx,x,y,r,g,b,o) => {
        ctx.fillStyle = 'rgba('+r+','+g+','+b+','+o+')';
        ctx.fillRect(x, y, 1, 1);
    };

    imzaIndir = (e) =>{
        var canvas = this.yeniimza.getCanvas();
        var data = canvas.toDataURL();
        download(data,"imza-"+Date.now()+".png","image/png");
    };

    yetkiKontrol(useryetkilist){
        var state = this.state;
        for(var i in state.yetkiler){
            if(useryetkilist.indexOf(state.yetkiler[i].id)>-1){
                state.yetkiler[i].durum=true;
            }
        }
        this.setState(state);
    }

    yetkileriGetir(){
        debugger;
        var that = this;
        var ekURL = "";
        var data = {};
        if(this.state.user.type==="personel"){
          ekURL = "/yetkilendirme/personelTumYetkiler"
          data["personelid"]=this.state.user.id;
        }else{
          ekURL = "/yetkilendirme/userTumYetkiler"
          data["userid"]=this.state.user.id;
        }
        axios.post(ajaxURL+ekURL,data).then(function(gelen){
          var state = that.state;
          state.user.yetkiler = gelen.data.result;
          that.setState(state);
          that.yetkiKontrol(gelen.data.result);
          
        }).catch(function(hata){
          message.error(hata.message);
        });
    }


    componentWillMount(){
        this.yetkileriGetir(); 
    }

    render() {
        const rowStyle = {width: '100%',display: 'flex',flexFlow: 'row wrap'};
        const rowStyle2 = {margin:"0",width: '100%',display: 'flex',flexFlow: 'row wrap'};
        const colStyle = {
            marginBottom: '16px'
        };
        const gutter = 16;
        const marginStyle = {marginTop:"10px"};
        return (
         <LayoutWrapper style={{display:this.state.yetkiler.sayfa.durum===true ? "flex":"none"}}>
                    <Breadcrumb style={{backgroundColor:"#fff",padding:"10px",width:"100%",marginBottom:"5px"}}>
                            <Breadcrumb.Item>Önemli Ayarlar</Breadcrumb.Item>
                            <Breadcrumb.Item>İmza Hazırlama</Breadcrumb.Item>
                            <Breadcrumb.Item>{this.state.edituser.isim!=="" ? this.state.edituser.isim:"Seçim Yapılmamış"}</Breadcrumb.Item>
                    </Breadcrumb>
            <Row style={rowStyle} justify="start">
                <Col md={24} sm={24} xs={24} style={colStyle}>

                    <Card
                        title="İmza Yükleme Alanı"
                        bodyStyle={{ padding: "10px",marginBottom:"10px" }}>
                        <div className="custom-card">
                            <p>
                                <Input onChange={this.imzaYukle.bind(this)} canvas={"imzaorjinal"} id={"imzafile"} type={"file"}></Input>
                            </p>
                            <p>
                                <Progressbar percent={this.state.propgres} style={marginStyle} />
                            </p>
                        </div>
                    </Card>

                </Col>
            </Row>

             <Row style={rowStyle}  justify="start" style={{width:"100%",display:this.state.imzaDuzenleStatus}}>
                 <Col md={24} sm={24} xs={24} style={colStyle}>
                     <Card
                         bodyStyle={{ padding: "10px",marginBottom:"10px" }}
                     >
                         <Row style={rowStyle2}  justify="start">
                             <Col md={24} sm={24} xs={24} style={colStyle}>
                                 <ButtonGroup>
                                     <Button style={{display:this.state.cropDrawStatus2}} onClick={this.fotografKes.bind(this)} type="primary"><i className={"ion-ios-crop-strong"}></i> İmzayı Kes</Button>
                                     <Button style={{display:this.state.cropDrawStatus}} onClick={this.fotografKesimKaydet.bind(this)} type="primary"><i className={"ion-ios-crop-strong"}></i> Çerçeveyi Kes</Button>
                                     <Button onClick={this.setTransparent.bind(this)} math={"-5"} section={"imza"} type="primary">-</Button>
                                     <Button>Şeffaflık : {this.state.imza.transparent}</Button>
                                     <Button onClick={this.setTransparent.bind(this)} math={"+5"} section={"imza"} type="primary">+</Button>
                                     <Button onClick={this.imzaIndir.bind(this)} section={"imza"} type="primary" style={{backgorundColor:"#8bc34a"}}><i className={"ion-android-download"}></i> İndir</Button>
                                 </ButtonGroup>
                             </Col>
                         </Row>
                         <Row style={rowStyle} justify="start">
                             <Col md={24} sm={24} xs={24} className="custom-card">
                                 <h3>Yeni İmzaa</h3>
                                 <div style={{border:"1px solid #ccc",padding:"5px"}} id="imzayeni"></div>
                             </Col>
                         </Row>
                         <Row style={rowStyle} justify="start">
                             <Col md={24} sm={24} xs={24} className="custom-card">
                                 <h3>Orjinal İmza</h3>
                                 <div style={{border:"1px solid #ccc",padding:"5px"}} id="imzaorjinal"></div>
                             </Col>
                         </Row>
                     </Card>
                 </Col>
             </Row>
         </LayoutWrapper>);
    }
}

export default connect(state => ({
    ...state.App.toJS()
}))(Imzahazirla);