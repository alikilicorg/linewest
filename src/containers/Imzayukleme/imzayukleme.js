// FN : 1526235808453
import React, { Component } from "react";
import { connect } from "react-redux";
import Card from '../../components/uielements/card';
import Timeline, {
    TimelineItem,
} from '../../components/uielements/timeline';
import Progressbar from '../../components/uielements/progress'
import Button, { ButtonGroup } from '../../components/uielements/button'
import Input from '../../components/uielements/input';
import LayoutWrapper from '../../components/utility/layoutWrapper.js';
import PageHeader from '../../components/utility/pageHeader';
import config2 from '../../settings/config';
import axios from 'axios';
import { Col, Row, message, AutoComplete, Icon, Select, Breadcrumb } from 'antd';
/*import ContentHolder from '../../components/utility/contentHolder';*/

const ajaxURL = config2.http+config2.server+':'+config2.port;
const AutoCompleteOption = AutoComplete.Option;
const Option = Select.Option;
function renderOption(item) {
    return (
      <Option key={item.id} text={item.isim}>{item.isim}</Option>
    );
}

class Imza extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user:{id:1,type:"personel",yetkiler:[]},
            /* edituser:localStorage.getItem('editUser')!==null ? JSON.parse(localStorage.getItem('editUser')):{id:1,durum:true}, */
            edituser:window.editUser,
            yetkiler:{
                sayfa:{id:9,message:"Sayfayı Açma Yetkiniz Yoktur",durum:false},
                admin:{id:31,message:"Sayfayı Açma Yetkiniz Yoktur",durum:false}
            },
            userDataSource:[],
            pictures:{
                imza1:{transparent:0,src:"",width:0,height:0,cropWidth:0,cropHeight:0,propgres:0,style:{width: '100%',display: 'none',flexFlow: 'row wrap'}},
                imza2:{transparent:0,src:"",width:0,height:0,cropWidth:0,cropHeight:0,propgres:0,style:{width: '100%',display: 'none',flexFlow: 'row wrap'}},
                imza3:{transparent:0,src:"",width:0,height:0,cropWidth:0,cropHeight:0,propgres:0,style:{width: '100%',display: 'none',flexFlow: 'row wrap'}},
                imza4:{transparent:0,src:"",width:0,height:0,cropWidth:0,cropHeight:0,propgres:0,style:{width: '100%',display: 'none',flexFlow: 'row wrap'}}
            }
        };
    }

    uygunaralik = (val,limit) => {
        const enaz = 255-limit;
        if(255>=val && val>=enaz){
            return true;
        }else{
            return false;
        }
    };

    setTransparent = (e) => {
        const imzaid = e.target.attributes.section.nodeValue;
        const islem = e.target.attributes.math.nodeValue;
        const state = this.state;
        var transparent = state.pictures[imzaid].transparent;
        if(islem==="+5"){
            transparent+=5;
        }else{
            transparent-=5;
        }
        if(transparent<0){transparent=0;}
        if(transparent>255){transparent=255;}
        state.pictures[imzaid].transparent=transparent;
        this.setState(state);
        this.renkdolas(imzaid,transparent);
    };

    renkdolas = (imzaid,aralik) =>{
        var canvas = document.getElementById(imzaid);
        var canvas2 = document.getElementById(imzaid+'yedek');
        var ctx = canvas.getContext('2d');
        var ctx2 = canvas2.getContext('2d');
        ctx2.clearRect(0,0,canvas2.width,canvas2.height);
        var width = canvas.width;
        var height = canvas.height;
        for(let i=0;i<width;i++){
            for(let j=0;j<height;j++){

                var c = ctx.getImageData(i, j, 1, 1).data;
                var r=c[0]; var ru = this.uygunaralik(r,aralik);
                var g=c[1]; var gu = this.uygunaralik(g,aralik);
                var b=c[2]; var bu = this.uygunaralik(b,aralik);
                if(ru===true && gu===true && bu===true){
                    this.drawPixel(ctx2,i,j,r,g,b,0);
                }else{
                    this.drawPixel(ctx2,i,j,r,g,b,1);
                }
            }
        }
    };

    drawPixel = (ctx,x,y,r,g,b,o) => {
        ctx.fillStyle = 'rgba('+r+','+g+','+b+','+o+')';
        ctx.fillRect(x, y, 1, 1);
    };

    imzaYukle = (e) => {
        debugger;
        var that = this;
        var canvasName = e.target.attributes.canvas.nodeValue;
        var canvas = document.getElementById(canvasName);
        var canvas2 = document.getElementById(canvasName+'yedek');
        var ctx = canvas.getContext('2d');
        var ctx2 = canvas2.getContext('2d');
        var file = e.target.files[0];
        var reader = new FileReader();
        var state = this.state;
        state["pictures"][canvasName]["propgres"]=50;
        this.setState(state);
        reader.onload = function(event){
            state["pictures"][canvasName]["propgres"]=100;
            state["pictures"][canvasName]["style"]["display"]="flex";
            that.setState(state);
            var img = new Image();
            img.onload = function(){
                canvas.width = img.width;
                canvas.height = img.height;
                canvas2.width = img.width;
                canvas2.height = img.height;
                canvas.style.width=img.width+'px';
                canvas.style.height=img.height+'px';
                canvas2.style.width=img.width+'px';
                canvas2.style.height=img.height+'px';
                ctx.drawImage(img,0,0);
                ctx2.drawImage(img,0,0);
                //that.griYap(canvasName);
            };
            img.src = event.target.result;
        };
        reader.readAsDataURL(file);

    };

    griYap(imzaid){
        debugger;
        var canvas = document.getElementById(imzaid);
        var canvas2 = document.getElementById(imzaid+'yedek');
        var ctx = canvas.getContext('2d');
        var ctx2 = canvas2.getContext('2d');
        var width = canvas.width;
        var height = canvas.height;
        for(let i=0;i<width;i++){
            for(let j=0;j<height;j++){
                var c = ctx.getImageData(i, j, 1, 1).data;
                var brg = 0.34*c[0]+0.5*c[1]+0.16*c[2];
                this.drawPixel(ctx,i,j,brg,brg,brg,1);
                this.drawPixel(ctx2,i,j,brg,brg,brg,1);
            }
        }
    }

    imzayiSistemeKaydet = (imzano,e) =>{
        debugger;
        var canvasName = e.target.attributes.section.nodeValue;
        var canvas2 = document.getElementById(canvasName+'yedek');
        var imgData=canvas2.toDataURL();
        var userid = this.state.edituser.id;
        var data = {};
        if(imzano==="imza1"){data={userid:userid,resim:{imza1:imgData}};}
        if(imzano==="imza2"){data={userid:userid,resim:{imza2:imgData}};}
        if(imzano==="imza3"){data={userid:userid,resim:{imza3:imgData}};}
        if(imzano==="imza4"){data={userid:userid,resim:{imza4:imgData}};}
        axios.post(ajaxURL+'/resimler/update',data).then(function(response){
            if(response.data.status){
                if(response.data.result[0]==0){
                    message.warning("Hiç Bir Değişiklik Olmamıştır");
                }else{
                    message.success(response.data.message);
                }
                
            }
        }).catch(function(hata){
            message.error(hata.message);
        });
    }

    userArama = (value) => {
        var that = this;
        var personelid = this.state.user.id;
        if(this.state.user.type=="personel"){
            if(value!==""){
                axios.post(ajaxURL+'/user/userbilgisearch',{search:value,personelid:personelid}).then(function(response){
                    if(response.data.status==true){
                        debugger;
                      var state = that.state;
                      if(that.state.user.yetkiler.indexOf(26)>-1){
                        state.userDataSource = response.data.result;
                      }else{
                        state.userDataSource=[];
                      for(var i in response.data.result){
                          if(response.data.result[i].personel===personelid){
                            state.userDataSource.push(response.data.result[i]);
                          }
                        }
                      }
                      
                      that.setState(state);
                    }else{
                        message.warning(response.data.message);
                    }
                  });
            }else{
                message.warning("Kulanıcı Arama Kutusu Boş Bırakılamaz");
            }
        }else{
            message.warning("Sadece Personeller Arama Yapabilir");
        }
    }
    
    userSecme = (value) => {
        debugger;
        var userid = parseInt(value);
        var state = this.state;
        state.edituser.id=value;
        this.setState(state);
    }

    yetkiKontrol(useryetkilist){
        var state = this.state;
        for(var i in state.yetkiler){
            if(useryetkilist.indexOf(state.yetkiler[i].id)>-1){
                state.yetkiler[i].durum=true;
            }
        }
        this.setState(state);
    }

    yetkileriGetir(){
        debugger;
        var that = this;
        var ekURL = "";
        var data = {};
        if(this.state.user.type==="personel"){
          ekURL = "/yetkilendirme/personelTumYetkiler"
          data["personelid"]=this.state.user.id;
        }else{
          ekURL = "/yetkilendirme/userTumYetkiler"
          data["userid"]=this.state.user.id;
        }
        axios.post(ajaxURL+ekURL,data).then(function(gelen){
          var state = that.state;
          state.user.yetkiler = gelen.data.result;
          that.setState(state);
          that.yetkiKontrol(gelen.data.result);
          
        }).catch(function(hata){
          message.error(hata.message);
        });
    }


    componentWillMount(){
        this.yetkileriGetir(); 
    }

    render() {
        const rowStyle = {width: '100%',display: 'flex',flexFlow: 'row wrap'};
        const colStyle = {
            marginBottom: '5px'
        };
        const gutter = 16;
        const marginStyle = {marginTop:"10px"};

        const rowStyleAdmin = {display:this.state.yetkiler.admin.durum==true ? "flex":"none"};

        const bilgiDegistirme = (
            <Row style={rowStyleAdmin}  justify="start">
                <Col md={24} sm={24} xs={24} style={colStyle}>
                    <AutoComplete
                        style={{width:"100%"}}
                        size="large"
                        dataSource={this.state.userDataSource.map(renderOption)}
                        onSelect={this.userSecme}
                        onSearch={this.userArama}
                        placeholder="Öğrenci Ad Soyad, Mail, Telefon"
                        optionLabelProp="text"
                        ><Input suffix={<Icon type="search" className="certain-category-icon" />} />
                    </AutoComplete>
                </Col>
            </Row>
        )
        
        return (
            <LayoutWrapper>
                    <Breadcrumb style={{backgroundColor:"#fff",padding:"10px",width:"100%",marginBottom:"5px"}}>
                            <Breadcrumb.Item>Önemli Ayarlar</Breadcrumb.Item>
                            <Breadcrumb.Item>İmza Yükleme Sistemi</Breadcrumb.Item>
                            <Breadcrumb.Item>{this.state.edituser.isim!=="" ? this.state.edituser.isim:"Seçim Yapılmamış"}</Breadcrumb.Item>
                    </Breadcrumb>
                <Row style={rowStyle} justify="start">
                    <Col md={24} sm={24} xs={24} style={colStyle}>
                    {bilgiDegistirme}
                        <Card bodyStyle={{ padding: "10px" }}>
                            <div className="custom-card">
                                <h3 style={{color:"#ca000d"}}>Bilgilendirme</h3>
                                <p>Bu sayfada şahsınıza ait <b>üç</b>, velinize ait <b>bir</b> adet
                                    imzayı sisteme yükleyerek işlemlerinizi hızlandırabilirsiniz.
                                    Böylece sizden aldığımız imzalar ile halihazırda atılması
                                    gereken alanlarda ki kısımlara imzalarınız otomatik olarak eklenecektir.</p>
                                <h3 style={{marginTop:"20px",color:"#0080ca"}}> Dikkat edilmesi Gereken Hususlar...</h3>
                                <Timeline style={{marginTop:"10px",marginBottom:"0"}}>
                                    <TimelineItem>
                                        İmzalarınız <b>Scanner - Tarayıcı</b> tarafından elde edilmiş olmalıdır
                                    </TimelineItem>
                                    <TimelineItem>
                                        Yükleyeceğiniz resim dosyaları <b>jpg,jpeg ve png</b> formatlarından herangi biri olmak zorundadır
                                    </TimelineItem>
                                    <TimelineItem>
                                        Öğrencinin <b>üç imzası da farklı</b> resimlerden oluşmalıdır
                                    </TimelineItem>
                                    <TimelineItem>
                                        Taranmış İmzaların <b>temiz ve beyaz</b> bir arkaplana sahip olmasına özen gösteriniz
                                    </TimelineItem>
                                    <TimelineItem>
                                        Yüklenen resimlerin  <b>transparanlığını en uygun görünümü elde ettiğinizde</b> sisteme yükleyiniz
                                    </TimelineItem>
                                </Timeline>
                            </div>
                        </Card>
                    </Col>
                </Row>


                <Row style={rowStyle} justify="start">
                    <Col md={24} sm={24} xs={24} style={colStyle}>
                        <div style={{ height: "100%" }}>
                            <Card
                                title="Birinci İmzayı Yükleme Alanı"
                                extra={<a>Yardım</a>}
                                style={{ width: '100%' }}
                            >
                                <p>
                                    <Input onChange={this.imzaYukle.bind(this)} canvas={"imza1"} id={"imzaresim1"} type={"file"}></Input>
                                </p>
                                <p>
                                    <Progressbar percent={this.state.pictures.imza1.propgres} style={marginStyle} />
                                </p>
                                <Row style={this.state.pictures.imza1.style} gutter={gutter} justify="start">
                                    <Col md={12} sm={12} xs={12} style={colStyle}>
                                        <canvas id="imza1"></canvas>
                                    </Col>
                                    <Col md={12} sm={12} xs={12} style={colStyle}>
                                        <canvas id="imza1yedek"></canvas>
                                    </Col>
                                </Row>
                                <Row style={this.state.pictures.imza1.style} gutter={gutter} justify="start">
                                    <Col md={24} sm={24} xs={24} style={colStyle}>
                                        <ButtonGroup>
                                            <Button onClick={this.setTransparent.bind(this)} math={"-5"} section={"imza1"} type="primary">-</Button>
                                            <Button>Şeffaflık : {this.state.pictures.imza1.transparent}</Button>
                                            <Button onClick={this.setTransparent.bind(this)} math={"+5"} section={"imza1"} type="primary">+</Button>
                                            <Button onClick={this.imzayiSistemeKaydet.bind(this,'imza1')} section={"imza1"} type="primary" style={{backgorundColor:"#8bc34a"}}>Kaydet</Button>
                                        </ButtonGroup>
                                    </Col>
                                </Row>
                            </Card>
                        </div>
                    </Col>
                </Row>



                <Row style={rowStyle} gutter={gutter} justify="start">
                    <Col md={24} sm={24} xs={24} style={colStyle}>
                        <div style={{ height: "100%" }}>
                            <Card
                                title="İkinci İmzayı Yükleme Alanı"
                                extra={<a>Yardım</a>}
                                style={{ width: '100%' }}
                            >
                                <p>
                                    <Input onChange={this.imzaYukle.bind(this)} canvas={"imza2"} id={"imzaresim2"} type={"file"}></Input>
                                </p>
                                <p>
                                    <Progressbar percent={this.state.pictures.imza2.propgres} style={marginStyle} />
                                </p>
                                <Row style={this.state.pictures.imza2.style} gutter={gutter} justify="start">
                                    <Col md={12} sm={12} xs={12} style={colStyle}>
                                        <canvas id="imza2"></canvas>
                                    </Col>
                                    <Col md={12} sm={12} xs={12} style={colStyle}>
                                        <canvas id="imza2yedek"></canvas>
                                    </Col>
                                </Row>
                                <Row style={this.state.pictures.imza2.style} gutter={gutter} justify="start">
                                    <Col md={24} sm={24} xs={24} style={colStyle}>
                                        <ButtonGroup>
                                            <Button onClick={this.setTransparent.bind(this)} math={"-5"} section={"imza2"} type="primary">-</Button>
                                            <Button>Şeffaflık : {this.state.pictures.imza2.transparent}</Button>
                                            <Button onClick={this.setTransparent.bind(this)} math={"+5"} section={"imza2"} type="primary">+</Button>
                                            <Button onClick={this.imzayiSistemeKaydet.bind(this,'imza2')} section={"imza2"} type="primary" style={{backgorundColor:"#8bc34a"}}>Kaydet</Button>
                                        </ButtonGroup>
                                    </Col>
                                </Row>
                            </Card>
                        </div>
                    </Col>
                </Row>




                <Row style={rowStyle} gutter={gutter} justify="start">
                    <Col md={24} sm={24} xs={24} style={colStyle}>
                        <div style={{ height: "100%" }}>
                            <Card
                                title="Üçüncü İmzayı Yükleme Alanı"
                                extra={<a>Yardım</a>}
                                style={{ width: '100%' }}
                            >
                                <p>
                                    <Input onChange={this.imzaYukle.bind(this)} canvas={"imza3"} id={"imzaresim2"} type={"file"}></Input>
                                </p>
                                <p>
                                    <Progressbar percent={this.state.pictures.imza3.propgres} style={marginStyle} />
                                </p>
                                <Row style={this.state.pictures.imza3.style} gutter={gutter} justify="start">
                                    <Col md={12} sm={12} xs={12} style={colStyle}>
                                        <canvas id="imza3"></canvas>
                                    </Col>
                                    <Col md={12} sm={12} xs={12} style={colStyle}>
                                        <canvas id="imza3yedek"></canvas>
                                    </Col>
                                </Row>
                                <Row style={this.state.pictures.imza3.style} gutter={gutter} justify="start">
                                    <Col md={24} sm={24} xs={24} style={colStyle}>
                                        <ButtonGroup>
                                            <Button onClick={this.setTransparent.bind(this)} math={"-5"} section={"imza3"} type="primary">-</Button>
                                            <Button>Şeffaflık : {this.state.pictures.imza3.transparent}</Button>
                                            <Button onClick={this.setTransparent.bind(this)} math={"+5"} section={"imza3"} type="primary">+</Button>
                                            <Button onClick={this.imzayiSistemeKaydet.bind(this,'imza3')} section={"imza3"} type="primary" style={{backgorundColor:"#8bc34a"}}>Kaydet</Button>
                                        </ButtonGroup>
                                    </Col>
                                </Row>
                            </Card>
                        </div>
                    </Col>
                </Row>




                <Row style={rowStyle} gutter={gutter} justify="start">
                    <Col md={24} sm={24} xs={24} style={colStyle}>
                        <div style={{ height: "100%" }}>
                            <Card
                                title="Ebeveyn İmzası Yükleme Alanı"
                                extra={<a>Yardım</a>}
                                style={{ width: '100%' }}
                            >
                                <p>
                                    <Input onChange={this.imzaYukle.bind(this)} canvas={"imza4"} id={"imzaresim2"} type={"file"}></Input>
                                </p>
                                <p>
                                    <Progressbar percent={this.state.pictures.imza4.propgres} style={marginStyle} />
                                </p>
                                <Row style={this.state.pictures.imza4.style} gutter={gutter} justify="start">
                                    <Col md={12} sm={12} xs={12} style={colStyle}>
                                        <canvas id="imza4"></canvas>
                                    </Col>
                                    <Col md={12} sm={12} xs={12} style={colStyle}>
                                        <canvas id="imza4yedek"></canvas>
                                    </Col>
                                </Row>
                                <Row style={this.state.pictures.imza4.style} gutter={gutter} justify="start">
                                    <Col md={24} sm={24} xs={24} style={colStyle}>
                                        <ButtonGroup>
                                            <Button onClick={this.setTransparent.bind(this)} math={"-5"} section={"imza4"} type="primary">-</Button>
                                            <Button>Şeffaflık : {this.state.pictures.imza4.transparent}</Button>
                                            <Button onClick={this.setTransparent.bind(this)} math={"+5"} section={"imza4"} type="primary">+</Button>
                                            <Button onClick={this.imzayiSistemeKaydet.bind(this,'imza4')} section={"imza4"} type="primary" style={{backgorundColor:"#8bc34a"}}>Kaydet</Button>
                                        </ButtonGroup>
                                    </Col>
                                </Row>
                            </Card>
                        </div>
                    </Col>
                </Row>

            </LayoutWrapper>
        );
    }
}
export default connect(state => ({
    ...state.App.toJS()
}))(Imza);