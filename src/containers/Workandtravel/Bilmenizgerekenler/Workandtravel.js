import React, { Component } from "react";
import { connect } from "react-redux";
import Card from '../../../components/uielements/card';
import LayoutWrapper from '../../../components/utility/layoutWrapper.js';
import { Col, Row, Breadcrumb} from 'antd';

class Yardimworkandtravel extends Component {
    constructor(props) {
        super(props);
        this.sayfa = "ali";
        this.state = {
            user:localStorage.getItem('user')!==null ? JSON.parse(localStorage.getItem('user')):{id:-1,durum:false,type:"user"},
            edituser:window.editUser,
        };
    }

    render(){
        const rowStyle = {width: '100%'};
        const colStyle = {
            marginBottom: '16px'
        };

        return(<LayoutWrapper>
            <Breadcrumb style={{backgroundColor:"#fff",padding:"10px",width:"100%",marginBottom:"5px"}}>
                            <Breadcrumb.Item>Work and Travel</Breadcrumb.Item>
                            <Breadcrumb.Item>Bilmeniz Gerekenler</Breadcrumb.Item>
                            <Breadcrumb.Item>{this.state.edituser.isim!=="" ? this.state.edituser.isim:"Seçim Yapılmamış"}</Breadcrumb.Item>
            </Breadcrumb>
            <Row style={rowStyle} justify="start">
                <Col md={24} sm={24} xs={24} style={colStyle}>

                    <Card
                        title="Work and Travel Başvurusunda Bilinmesi Gerekenler"
                        bodyStyle={{ padding: "10px",marginBottom:"10px" }}>

                    </Card>
                </Col>
            </Row>
        </LayoutWrapper>);
    }
}

export default connect(state => ({
    ...state.App.toJS()
}))(Yardimworkandtravel);
