import React, { Component } from "react";
import LayoutWrapper from '../../../components/utility/layoutWrapper.js';
import PageHeader from '../../../components/utility/pageHeader';
import { Form, Input, Tooltip, Icon, Upload, Modal, Cascader, Breadcrumb, Select, Row, Col, Checkbox, Radio, Button, AutoComplete, Card, Table, DatePicker, InputNumber, message, Popconfirm } from 'antd';
import config2 from '../../../settings/config';
import axios from 'axios';
import moment from 'moment';
import matchSorter from 'match-sorter'
import ReactTable from "react-table";
import "../../css/react-table.css";
const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const Dragger = Upload.Dragger;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;
const { TextArea } = Input;
const { RangePicker } = DatePicker;
const ajaxURL = config2.http+config2.server+':'+config2.port;

function renderOption(item) {
    return (
      <Option key={item.id} text={item.isim}>{item.isim}</Option>
    );
}

class Sistemimzasi extends Component {
    constructor(){
        super();
        this.state={
            user:localStorage.getItem('user')!==null ? JSON.parse(localStorage.getItem('user')):{id:-1,durum:false,type:"user"},
            /* edituser:localStorage.getItem('editUser')!==null ? JSON.parse(localStorage.getItem('editUser')):{id:1,durum:true}, */
            edituser:window.editUser,
            yetkiler:{
                sayfa:{id:35,message:"Sayfayı Açma Yetkiniz Yoktur",durum:false},
                guncelleme:{id:37,message:"Kişisel Bilgileri Düzeltme Yetkiniz Yoktur",durum:false},
                admin:{id:36,message:"Bu Sayfada Adminlik Yetkiniz Yoktur",durum:false}
            },
            userDataSource:[],
            form:{
                sistemimza:{list:[],value:"",disabled:false}
            }
        };
    }

    userArama = (value) => {
        var that = this;
        var personelid = this.state.user.id;
        if(this.state.user.type=="personel"){
            if(value!==""){
                axios.post(ajaxURL+'/user/userbilgisearch',{search:value,personelid:personelid}).then(function(response){
                    if(response.data.status==true){
                        debugger;
                      var state = that.state;
                      if(that.state.user.yetkiler.indexOf(26)>-1){
                        state.userDataSource = response.data.result;
                      }else{
                        state.userDataSource=[];
                      for(var i in response.data.result){
                          if(response.data.result[i].personel===personelid){
                            state.userDataSource.push(response.data.result[i]);
                          }
                        }
                      }
                      that.setState(state);
                    }else{
                        message.warning(response.data.message);
                    }
                  });
            }else{
                message.warning("Kulanıcı Arama Kutusu Boş Bırakılamaz");
            }
        }else{
            message.warning("Sadece Personeller Arama Yapabilir");
        }
    }

    userSecme = (value) => {
        var userid = parseInt(value);
        var state = this.state;
        state.edituser.id=userid;
        state.edituser.durum=true;
        this.setState(state);
        this.bilgileriCek();
        
    }

    yetkiKontrol(useryetkilist){
        var state = this.state;
        for(var i in state.yetkiler){
            if(useryetkilist.indexOf(state.yetkiler[i].id)>-1){
                state.yetkiler[i].durum=true;
            }
        }
        this.setState(state);
        this.bilgileriCek();
    }

    yetkileriGetir(){
        var that = this;
        var ekURL = "";
        var data = {};
        if(this.state.user.type==="personel"){
          ekURL = "/yetkilendirme/personelTumYetkiler"
          data["personelid"]=this.state.user.id;
        }else{
          ekURL = "/yetkilendirme/userTumYetkiler"
          data["userid"]=this.state.user.id;
        }
        axios.post(ajaxURL+ekURL,data).then(function(gelen){
          var state = that.state;
          state.user.yetkiler = gelen.data.result;
          that.setState(state);
          that.yetkiKontrol(gelen.data.result);
        }).catch(function(hata){
          message.error(hata.message);
        });
    }

    bilgileriCek(){
        var userid = this.state.edituser.id;
        var that = this;
        if(this.state.yetkiler.sayfa.durum){
            if(userid>0){
                axios.post(ajaxURL+"/resimler/findByUserid",{userid:userid}).then(function(gelen){
                    var state = that.state;
                    state.form.sistemimza.value=gelen.data.result[0].imzasozlesmesi;
                    that.setState(state);
                }).catch(function(err){
                    message.warning("Kullanıcının Böyle Bir Sözleşmesi Bulunmamaktadır Hata : "+err.message);
                });
            }else{
                message.warning("Sistem İmzası Bilgilerini çekmek için tanımlı bir kullanıcıya rastlanamadı");
            }
            
        }
    }

    sistemImzasiIndir(){
        const fileURL = config2.serverFileRoot+"/public/depo/sistemimzasi.pdf";
        var save = document.createElement('a');
        save.href = fileURL;
        save.target = '_blank';
        var filename = fileURL.substring(fileURL.lastIndexOf('/')+1);
        save.download = filename || "sistemimzasi.pdf";
	       if ( navigator.userAgent.toLowerCase().match(/(ipad|iphone|safari)/) && navigator.userAgent.search("Chrome") < 0) {
				document.location = save.href; 
			}else{
		        var evt = new MouseEvent('click', {
		            'view': window,
		            'bubbles': true,
		            'cancelable': false
		        });
		        save.dispatchEvent(evt);
		        (window.URL || window.webkitURL).revokeObjectURL(save.href);
			}	
    }

    componentWillMount(){
        debugger;
        this.yetkileriGetir();
    }

    


    render(){
        const that = this;
        const { tableData } = this.state;
        const formItemLayout = {labelCol: {xs: { span: 24 },sm: { span: 6 },},wrapperCol: {xs: { span: 24 },sm: { span: 14 },},style:{marginBottom:"15px"}}
        const formItemLayoutnostyle = {labelCol: {xs: { span: 24 },sm: { span: 6 },},wrapperCol: {xs: { span: 24 },sm: { span: 14 },}}
        const dateFormat = 'YYYY-MM-DD';

        const bilgiDegistirme = (
            <FormItem 
            style={{marginBottom:"15px",display:this.state.yetkiler.admin.durum===true ? "block":"none"}} 
            labelCol={formItemLayout.labelCol} 
            wrapperCol={formItemLayout.wrapperCol} 
            label="Düzenlenecek Kullanıcı : ">
                    <AutoComplete
                        defaultValue={this.state.edituser.isim}
                        style={{width:"100%",marginBottom:"16px"}}
                        size="large"
                        dataSource={this.state.userDataSource.map(renderOption)}
                        onSelect={this.userSecme}
                        onSearch={this.userArama}
                        placeholder="Öğrenci Ad Soyad, Mail, Telefon"
                        optionLabelProp="text"
                        ><Input suffix={<Icon type="search" className="certain-category-icon" />} />
                    </AutoComplete>
            </FormItem>
        )

        const uploadprops = {
            name: 'file',
            multiple: false,
            type:"file",
            onChange(info) {
              debugger;
                 var reader  = new FileReader();
                 var file = info.target.files[0];
                 
                 reader.addEventListener("load", function () {
                    debugger;
                    var userid = that.state.edituser.id;
                    var imgData = reader.result;
                    var data = {};
                    data={userid:userid,resim:imgData};
                    axios.post(ajaxURL+'/resimler/imzasozlesmesiyukle',data).then(function(response){
                        debugger;
                        if(response.data.status){
                            debugger;
                            message.success(response.data.message);
                            var state = that.state;
                            state.form.sistemimza.value=response.data.result;
                            that.setState(state);
                        }else{
                            message.error("İmza Sözleşmesi Yüklenemedi");
                        }
                    }).catch(function(hata){
                        message.error(hata.message);
                    });
                  }, false);
                  if (file) {
                    reader.readAsDataURL(file);
                  }
              //var imgData=canvas2.toDataURL();
                var imgData = "";
                
                
            },
          };

        const imzaSozlesmesiYukleme = (
            <FormItem 
            style={{marginBottom:"15px",display:this.state.yetkiler.admin.durum===true ? "block":this.state.yetkiler.guncelleme.durum===true ? "block":"none"}} 
            labelCol={formItemLayout.labelCol} 
            wrapperCol={formItemLayout.wrapperCol} 
            label="Yükleme Alanı : ">
                <Input {...uploadprops}>
                </Input>
            </FormItem>
        );

        if(this.state.yetkiler.sayfa.durum===true){
            return(
                <LayoutWrapper>
                    <Breadcrumb style={{backgroundColor:"#fff",padding:"10px",width:"100%",marginBottom:"5px"}}>
                            <Breadcrumb.Item>Work and Travel</Breadcrumb.Item>
                            <Breadcrumb.Item>Sistem İmzası Yükleme ve İndirme</Breadcrumb.Item>
                            <Breadcrumb.Item>{this.state.edituser.isim!=="" ? this.state.edituser.isim:"Seçim Yapılmamış"}</Breadcrumb.Item>
                    </Breadcrumb>
                    <Row style={{width:"100%"}}>
                        <Col lg={24} md={24} sm={24} xs={24}>
                        
                            <Card title="Sistem İmzası Yükleme ve İndirme"
                             extra = {<Button type="primary" style={{borderRadius:"5px"}} onClick={this.sistemImzasiIndir.bind(this)}>Sistem İmzası İndir</Button>} 
                             bodyStyle={{padding:"10px"}}>
                                <Row>
                                    <Col style={{textAlign:"center"}} lg={24} md={24} sm={24} xs={24}>
                                        {bilgiDegistirme}
                                        {imzaSozlesmesiYukleme}
                                        <img style={{width:"100%",maxWidth:"800px"}} src={config2.serverFileRoot+this.state.form.sistemimza.value}/>
                                    </Col>
                                </Row>
                            </Card>
                        </Col>
                    </Row>
                </LayoutWrapper>
            );
        }else{
            return (<LayoutWrapper/>);
        }
    }
}

export default Sistemimzasi;