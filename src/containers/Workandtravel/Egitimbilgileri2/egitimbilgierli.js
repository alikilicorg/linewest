import React, { Component } from "react";
import LayoutWrapper from '../../../components/utility/layoutWrapper.js';
import PageHeader from '../../../components/utility/pageHeader';
import { Form, Input, Tooltip, Icon, Upload, Modal, Cascader, Select, Row, Col, Checkbox, Button, Breadcrumb, AutoComplete, Card, Table, DatePicker, InputNumber, message, Popconfirm } from 'antd';
import config2 from '../../../settings/config';
import axios from 'axios';
import moment from 'moment';
const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;
const { TextArea } = Input;
const { RangePicker } = DatePicker;
const ajaxURL = config2.http+config2.server+':'+config2.port;

function renderOption(item) {
    return (
      <Option key={item.id} text={item.isim}>{item.isim}</Option>
    );
}

class Egitimbilgileri extends Component {
    constructor(){
        super();
        this.state = {
            user:localStorage.getItem('user')!==null ? JSON.parse(localStorage.getItem('user')):{id:-1,durum:false,type:"user"},
            /* edituser:localStorage.getItem('editUser')!==null ? JSON.parse(localStorage.getItem('editUser')):{id:1,durum:true}, */
            edituser:window.editUser,
            yetkiler:{
                sayfa:{id:13,message:"Sayfayı Açma Yetkiniz Yoktur",durum:false},
                guncelleme:{id:17,message:"Kişisel Bilgileri Düzeltme Yetkiniz Yoktur",durum:false},
                admin:{id:18,message:"Bu Sayfada Adminlik Yetkiniz Yoktur",durum:false}
            },
            userDataSource:[],
            form:{
                universiteid:{list:[],value:"",disabled:false},
                fakulteid:{list:[],value:"",disabled:false},
                bolumid:{list:[],value:"",disabled:false},
                sinif:{list:[],value:"",disabled:false},
                sonnotortalama:{list:[],value:"",disabled:false},
                okulno:{list:[],value:"",disabled:false},
                akademikkapanis:{list:[],value:"",disabled:false},
                akademikacilis:{list:[],value:"",disabled:false},
                ilkkayittarihi:{list:[],value:"",disabled:false},
                tahminimezuniyet:{list:[],value:"",disabled:false},
                okulil:{list:[],value:"",disabled:false}
            }

        }
    }

    userArama = (value) => {
        var that = this;
        var personelid = this.state.user.id;
        if(this.state.user.type=="personel"){
            if(value!==""){
                axios.post(ajaxURL+'/user/userbilgisearch',{search:value,personelid:personelid}).then(function(response){
                    if(response.data.status==true){
                        debugger;
                      var state = that.state;
                      if(that.state.user.yetkiler.indexOf(26)>-1){
                        state.userDataSource = response.data.result;
                      }else{
                        state.userDataSource=[];
                      for(var i in response.data.result){
                          if(response.data.result[i].personel===personelid){
                            state.userDataSource.push(response.data.result[i]);
                          }
                        }
                      }
                      
                      that.setState(state);
                    }else{
                        message.warning(response.data.message);
                    }
                  });
            }else{
                message.warning("Kulanıcı Arama Kutusu Boş Bırakılamaz");
            }
        }else{
            message.warning("Sadece Personeller Arama Yapabilir");
        }
    }

    userSecme = (value) => {
        debugger;
        var userid = parseInt(value);
        var state = this.state;
        state.edituser.id=value;
        this.setState(state);
        this.bilgileriCek();
    }

    formGuncelle(degisken,tip,veri,dateString){
        var deger = "";
        if(tip==="input"){
            deger = veri.target.value;
        }
        if(tip==="select" || tip==="number"){
            deger = veri;
        }
        if(tip=="date"){
            deger=dateString;
        }
        if(deger!==""){
            var state = this.state;
            state.form[degisken].value=deger;
            this.setState(state);
        }else{
            var state = this.state;
            state.form[degisken].value="";
            this.setState(state);
        }
        if(degisken==="universiteid"){
            this.getFakulteByUniversiteId(deger);
        }

        if(degisken==="fakulteid"){
            this.getBolumByFakulteId(deger);
        }
    }

    getBolumByFakulteId(fakulteid){
        debugger;
        var that = this;
        axios.post(ajaxURL+"/bolum/findbyfakulteid",{fakulteid:fakulteid}).then(function(veriler){
            if(veriler.data.status){
                var state = that.state;
                state.form.bolumid.list = veriler.data.result;
                var eskibolumid = state.form.bolumid.value;
                var durum = false;
                for(var i in state.form.bolumid.list){
                    if(state.form.bolumid.list[i].id===eskibolumid){
                        durum=true; 
                    }
                }
                if(durum===false){
                    state.form.bolumid.value="";
                }
                that.setState(state);
            }else{
                message.error(veriler.data.message); 
            }
            
        }).catch(function(err){
            message.error(err.message);
        });
    }

    getFakulteByUniversiteId(universiteid){
        debugger;
        var that = this;
        if(universiteid>0){
            axios.post(ajaxURL+"/fakulte/findbyuniversiteid",{universiteid:universiteid}).then(function(veriler){
                if(veriler.data.status){
                    var state = that.state;
                    state.form.fakulteid.list = veriler.data.result;
                    var eskifakulteid = state.form.fakulteid.value;
                    var durum = false;
                    for(var i in state.form.fakulteid.list){
                        if(state.form.fakulteid.list[i].id===eskifakulteid){
                            durum=true; 
                        }
                    }
                    if(durum===false){
                        state.form.fakulteid.value="";
                        state.form.bolumid.value="";
                        state.form.bolumid.list=[];
                    }
                    that.setState(state);
                }else{
                    message.error(veriler.data.message); 
                }

                
            }).catch(function(err){
                message.error(err.message);
            });
        }else{
            var state = this.state;
            state.form.fakulteid.value="";
            state.form.fakulteid.list=[];
        }
        
    }

    getIlList(){
        var that  =this;
        axios.get(ajaxURL+'/il/iller').then(function(response){
            if(response.data.status==true){
              var state = that.state;
              state.form.okulil.list = response.data.result;
              that.setState(state);
            }else{
                message.warning(response.data.message);
            }
          });
    }

    getUniversite(){
        var that  =this;
        axios.get(ajaxURL+'/universite/list').then(function(response){
            if(response.data.status==true){
              var state = that.state;
              state.form.universiteid.list = response.data.result;
              that.setState(state);
            }else{
                message.warning(response.data.message);
            }
          });
    }
    siniflarListele(){
        var liste = [
            {id:"1. Sınıf Güz Dönemi",isim:"1. Sınıf Güz Dönemi"},
            {id:"1. Sınıf Bahar Dönemi",isim:"1. Sınıf Bahar Dönemi"},
            {id:"2. Sınıf Güz Dönemi",isim:"2. Sınıf Güz Dönemi"},
            {id:"2. Sınıf Bahar Dönemi",isim:"2. Sınıf Bahar Dönemi"},
            {id:"3. Sınıf Güz Dönemi",isim:"3. Sınıf Güz Dönemi"},
            {id:"3. Sınıf Bahar Dönemi",isim:"3. Sınıf Bahar Dönemi"},
            {id:"4. Sınıf Güz Dönemi",isim:"4. Sınıf Güz Dönemi"},
            {id:"4. Sınıf Bahar Dönemi",isim:"4. Sınıf Bahar Dönemi"},
            {id:"5. Sınıf Güz Dönemi",isim:"5. Sınıf Güz Dönemi"},
            {id:"5. Sınıf Bahar Dönemi",isim:"5. Sınıf Bahar Dönemi"},
            {id:"6. Sınıf Güz Dönemi",isim:"6. Sınıf Güz Dönemi"},
            {id:"6. Sınıf Bahar Dönemi",isim:"6. Sınıf Bahar Dönemi"}

        ];
        var state = this.state;
        state.form.sinif.list=liste;
        this.setState(state);
    }
    getTahminimezuniyet(){
        var yil = new Date().getFullYear();
        var sonyil = yil+10;
        var yillist = [];
        for(var i=yil;i<sonyil;i++){
            yillist.push({id:i,isim:i+""});
        }
        var state = this.state;
        state.form.tahminimezuniyet.list=yillist;
        this.setState(state); 
    }

    yetkiKontrol(useryetkilist){
        var state = this.state;
        for(var i in state.yetkiler){
            if(useryetkilist.indexOf(state.yetkiler[i].id)>-1){
                state.yetkiler[i].durum=true;
            }
        }
        this.setState(state);
        this.bilgileriCek(true);
    }

    yetkileriGetir(){
        var that = this;
        var ekURL = "";
        var data = {};
        if(this.state.user.type==="personel"){
          ekURL = "/yetkilendirme/personelTumYetkiler"
          data["personelid"]=this.state.user.id;
        }else{
          ekURL = "/yetkilendirme/userTumYetkiler"
          data["userid"]=this.state.user.id;
        }
        axios.post(ajaxURL+ekURL,data).then(function(gelen){
          var state = that.state;
          state.user.yetkiler = gelen.data.result;
          that.yetkiKontrol(gelen.data.result);
          that.setState(state);
        }).catch(function(hata){
          message.error(hata.message);
        });
    }

    degisiklikleriKaydet(){
        debugger;
        var that = this;
        var userid = this.state.edituser.id;
        var data = {
            userid:userid,
            universiteid:this.state.form.universiteid.value,
            fakulteid:this.state.form.fakulteid.value,
            bolumid:this.state.form.bolumid.value,
            sinif:this.state.form.sinif.value,
            okulil:this.state.form.okulil.value,
            sonnotortalama:this.state.form.sonnotortalama.value,
            okulno:this.state.form.okulno.value,
            akademikkapanis:this.state.form.akademikkapanis.value,
            akademikacilis:this.state.form.akademikacilis.value,
            ilkkayittarihi:this.state.form.ilkkayittarihi.value,
            tahminimezuniyet:this.state.form.tahminimezuniyet.value
        }
        axios.post(ajaxURL+"/egitimbilgileri/update",data).then(function(gelen){
            var state = that.state;
            if(gelen.data.status){
                var state = that.state;
                state.yetkiler.guncelleme.durum=false;
                that.setState(state);
                message.success("Eğitim Bilgileri Güncellenmiştir"); 
            }else{
                message.error(gelen.data.message); 
            }
            
          }).catch(function(hata){
            message.error(hata.message);
          });

    }

    bilgileriCek(){
        debugger;
        var userid = this.state.edituser.id;
        var that = this;
        if(this.state.yetkiler.guncelleme.durum){
            if(userid>0){
                axios.post(ajaxURL+"/egitimbilgileri/findbyUserid",{userid:userid}).then(function(gelen){
                    if(gelen.data.status){
                        var gelendata = gelen.data.result[0];
                        var state = that.state;
                        for(var i in state.form){
                            if(typeof gelendata[i]!=="undefine"){
                                if(gelendata[i]==null || gelendata[i]==""){
                                    state.form[i].value="";
                                }else{
                                    state.form[i].value=gelendata[i];
                                }
                            }
                        }
                        if(state.form.universiteid.value!==""){
                            that.getFakulteByUniversiteId(state.form.universiteid.value);
                        }else{
                            state.form.fakulteid.value=""
                            state.form.bolumid.value=""
                        }
                        if(state.form.fakulteid.value!=="" && state.form.universiteid.value!==""){
                            that.getBolumByFakulteId(state.form.fakulteid.value);
                        }else{
                            state.form.fakulteid.value=""
                            state.form.bolumid.value=""
                        }
                        that.setState(state);
                    }else{
                        message.error("İlgili Kullanıcının Bilgilerine Rastlanamadı. Lütfen Bilgileri Doldurup Kayıt Ediniz");
                    }
                  }).catch(function(hata){
                    message.error(hata.message);
                  });
            }else{
                message.warning("Eğitim Bilgileri çekmek için tanımlı bir kullanıcıya rastlanamadı");
            }
            
        }
    }

    componentWillMount(){
        this.yetkileriGetir();
    }

    componentDidMount(){
       this.getUniversite();
       this.siniflarListele();
       this.getTahminimezuniyet();
       this.getIlList();
    }

    render(){
        const formItemLayout = {labelCol: {xs: { span: 24 },sm: { span: 5 },},wrapperCol: {xs: { span: 24 },sm: { span: 15 },},style:{marginBottom:"15px"}}
        const dateFormat = 'YYYY-MM-DD';
        

        const bilgiDegistirme = (
            <FormItem 
            style={{marginBottom:"15px", marginTop:"30px",display:this.state.yetkiler.admin.durum===true ? "block":"none"}} 
            labelCol={formItemLayout.labelCol} 
            wrapperCol={formItemLayout.wrapperCol} 
            label="Düzenlenecek Kullanıcı : ">
                    <AutoComplete
                        defaultValue={this.state.edituser.isim}
                        style={{width:"100%",marginBottom:"16px"}}
                        size="large"
                        dataSource={this.state.userDataSource.map(renderOption)}
                        onSelect={this.userSecme}
                        onSearch={this.userArama}
                        placeholder="Öğrenci Ad Soyad, Mail, Telefon"
                        optionLabelProp="text"
                        ><Input suffix={<Icon type="search" className="certain-category-icon" />} />
                    </AutoComplete>
            </FormItem>
        )

        const universiteListesi = this.state.form.universiteid.list.map((item,sira)=>{
            return (<Option value={item.id}>{item.isim}</Option>);
        });

        const fakulteListesi = this.state.form.fakulteid.list.map((item,sira)=>{
            return (<Option value={item.id}>{item.isim}</Option>);
        });

        const bolumListesi = this.state.form.bolumid.list.map((item,sira)=>{
            return (<Option value={item.id}>{item.isim}</Option>);
        });

        const sinifListesi = this.state.form.sinif.list.map((item,sira)=>{
            return (<Option value={item.id}>{item.isim}</Option>);
        });

        const tahminimezuniyet = this.state.form.tahminimezuniyet.list.map((item,sira)=>{
            return (<Option value={item.id}>{item.isim}</Option>);
        });

        const ilListesi = this.state.form.okulil.list.map((item,sira)=>{
            return (<Option value={item.id}>{item.isim}</Option>);
        });

        if(this.state.yetkiler.sayfa.durum===true){
            return(
                <LayoutWrapper>
                    <Breadcrumb style={{backgroundColor:"#fff",padding:"10px",width:"100%",marginBottom:"5px"}}>
                            <Breadcrumb.Item>Work and Travel</Breadcrumb.Item>
                            <Breadcrumb.Item>Eğitim Bilgilerin Girişi</Breadcrumb.Item>
                            <Breadcrumb.Item>{this.state.edituser.isim!=="" ? this.state.edituser.isim:"Seçim Yapılmamış"}</Breadcrumb.Item>
                    </Breadcrumb>
                    <Card title="2. Adım - Eğitim Bilgilerinin Girilmesi" style={{width:"100%"}} bodyStyle={{padding:"0"}}>
                    <Row style={{width:"100%"}}>
                        <Col lg={24} md={24} sm={24} xs={24}>
                            <Card bodyStyle={{padding:"10px"}}>
                                <Form>
                                    {bilgiDegistirme}
    
                                <FormItem {...formItemLayout} label="Üniversite Seçimi : ">
                                <Select disabled={this.state.form.universiteid.disabled} value={this.state.form.universiteid.value}  onChange={this.formGuncelle.bind(this,'universiteid','select')} style={{ width: "100%" }} showSearch filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
                                    {universiteListesi}
                                </Select>
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="Fakülte Seçimi : ">
                                <Select disabled={this.state.form.fakulteid.disabled} value={this.state.form.fakulteid.value}  onChange={this.formGuncelle.bind(this,'fakulteid','select')} style={{ width: "100%" }} showSearch filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
                                    {fakulteListesi}
                                </Select>
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="Bölüm Seçimi : ">
                                <Select disabled={this.state.form.bolumid.disabled} value={this.state.form.bolumid.value}  onChange={this.formGuncelle.bind(this,'bolumid','select')} style={{ width: "100%" }} showSearch filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
                                    {bolumListesi}
                                </Select>
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="Sınıf Seçimi : ">
                                <Select disabled={this.state.form.sinif.disabled} value={this.state.form.sinif.value}  onChange={this.formGuncelle.bind(this,'sinif','select')} style={{ width: "100%" }} >
                                    {sinifListesi}
                                </Select>
                                </FormItem>

                                <FormItem {...formItemLayout} label="Üniversiteyi Hangi İlde Okudunuz : ">
                                <Select disabled={this.state.form.okulil.disabled} value={this.state.form.okulil.value}  onChange={this.formGuncelle.bind(this,'okulil','select')} style={{ width: "100%" }} >
                                    {ilListesi}
                                </Select>
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="Son Dönem Not Ortalaması : ">
                                <Input disabled={this.state.form.sonnotortalama.disabled} value={this.state.form.sonnotortalama.value} onChange={this.formGuncelle.bind(this,'sonnotortalama','input')} />
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="Üniversite Numarası : ">
                                <Input disabled={this.state.form.okulno.disabled} value={this.state.form.okulno.value} onChange={this.formGuncelle.bind(this,'okulno','input')} />
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="Üni. Akademik Bitiş Tarihi :">
                                    <DatePicker value={this.state.form.akademikkapanis.value!=="" ? moment(this.state.form.akademikkapanis.value, dateFormat):moment()} disabled={this.state.form.akademikkapanis.disabled} style={{width:"100%"}} onChange={this.formGuncelle.bind(this,'akademikkapanis','date')} format={dateFormat} />
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="Üni. Akademik Başlangıç Tarihi :">
                                    <DatePicker value={this.state.form.akademikacilis.value!=="" ? moment(this.state.form.akademikacilis.value, dateFormat):moment()}   style={{width:"100%"}} onChange={this.formGuncelle.bind(this,'akademikacilis','date')} format={dateFormat} />
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="Üni. İlk Kayıt Tarihi :">
                                    <DatePicker value={this.state.form.ilkkayittarihi.value!=="" ? moment(this.state.form.ilkkayittarihi.value, dateFormat):moment()}  disabled={this.state.form.ilkkayittarihi.disabled} style={{width:"100%"}} onChange={this.formGuncelle.bind(this,'ilkkayittarihi','date')} format={dateFormat} />
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="Tahmini Mezuniyet Yılı :">
                                <Select disabled={this.state.form.tahminimezuniyet.disabled} value={this.state.form.tahminimezuniyet.value}  onChange={this.formGuncelle.bind(this,'tahminimezuniyet','select')} style={{ width: "100%" }} showSearch filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0} >
                                    {tahminimezuniyet}
                                </Select>
                                </FormItem>

                                <FormItem style={{textAlign:"right",display:this.state.yetkiler.guncelleme.durum==false ? "block":"none"}} labelCol={formItemLayout.labelCol} wrapperCol={formItemLayout.wrapperCol} label="Sisteme Kaydet">
                                    Eğitim Bilgilerini Düzeltme Yetkiniz Yoktur Bir Hata Olduğunu Düşünüyorsanız Lütfen İletişime Geçiniz : 05534259487
                                </FormItem>

                                <FormItem style={{textAlign:"right",display:this.state.yetkiler.guncelleme.durum==true ? "block":this.state.yetkiler.admin.durum==true ? "block":"none"}} labelCol={formItemLayout.labelCol} wrapperCol={formItemLayout.wrapperCol} label="Sisteme Kaydet">
                                <Popconfirm title="Eğitim Bilgilerini Kaydetmek İstediğinizden Emin misiniz?" onConfirm={this.degisiklikleriKaydet.bind(this)} okText="Evet" cancelText="Hayır">
                                        <Button style={{borderRadius:"5px"}} type="primary" >Değişiklikleri Kaydet</Button>
                                    </Popconfirm>
                                </FormItem>
    
                                </Form>
                            </Card>
                        </Col>
                    </Row>
                    </Card>
                </LayoutWrapper>
            );
        }else{
            return (
            <LayoutWrapper>

            </LayoutWrapper>
            );
        } 
    }

}

export default Egitimbilgileri;