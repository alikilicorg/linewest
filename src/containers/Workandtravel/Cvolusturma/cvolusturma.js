import React, { Component } from "react";
import LayoutWrapper from '../../../components/utility/layoutWrapper.js';
import PageHeader from '../../../components/utility/pageHeader';
import { Form, Input, Tooltip, Icon, Upload, Modal, Cascader, Select, Breadcrumb, Row, Col, Checkbox, Radio, Button, AutoComplete, Card, Table, DatePicker, InputNumber, message, Popconfirm } from 'antd';
import config2 from '../../../settings/config';
import axios from 'axios';
import moment from 'moment';
const InputGroup = Input.Group;
const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;
const { TextArea } = Input;
const { RangePicker } = DatePicker;
const ajaxURL = config2.http+config2.server+':'+config2.port;

function renderOption(item) {
    return (
      <Option key={item.id} text={item.isim}>{item.isim}</Option>
    );
}



class Cvolusturma extends Component {
    constructor(){
        super();
        this.state={
            user:localStorage.getItem('user')!==null ? JSON.parse(localStorage.getItem('user')):{id:-1,durum:false,type:"user"},
            /* edituser:localStorage.getItem('editUser')!==null ? JSON.parse(localStorage.getItem('editUser')):{id:1,durum:true}, */
            edituser:window.editUser,
            yetkiler:{
                sayfa:{id:23,message:"Sayfayı Açma Yetkiniz Yoktur",durum:false},
                guncelleme:{id:24,message:"Kişisel Bilgileri Düzeltme Yetkiniz Yoktur",durum:false},
                admin:{id:25,message:"Bu Sayfada Adminlik Yetkiniz Yoktur",durum:false}
            },
            userDataSource:[],
            form:{
                adsoyad:{list:[],value:"",disabled:true},
                adres:{list:[],value:"",disabled:true},
                telefon:{list:[],value:"",disabled:true},
                mail:{list:[],value:"",disabled:true},
                skype:{list:[],value:"",disabled:true},
                watbaslama:{list:[],value:"",disabled:true},
                watbitis:{list:[],value:"",disabled:true},
                whoami:{list:[],value:"",disabled:false},
                whyami:{list:[],value:"",disabled:false},
                yetenekler:{list:[],value:"",disabled:false},
                egitim:{list:{},value:"",disabled:false},
                isdeneyimi:{list:[],value:"",disabled:false},
                stajlar:{list:[],value:"",disabled:false},
                diller:{list:[],value:"",disabled:false},
                hobiler:{list:[],value:"",disabled:false},
                sporclub:{list:[],value:"",disabled:false},
                fotograflar:{list:["http://brandmark.io/logo-rank/random/pepsi.png"],value:"",disabled:false},
                neden:{list:[],value:"",disabled:false},
            },
            templateisdeneyimi:{sirket:"",adres:"",referans:"",telefon:"",pozisyon:"",aciklama:"",baslangic:"",bitis:""},
            templateegitim:{uniname:"",unisehir:"",tahminiMezNotu:"",tahminiUniBitis:""},
            templateaktivite:{diller:"",sporveclub:"",hobiler:""},
            fotograflar:[],
            previewVisible: false,
            previewImage: '',
            ingilizceListesi:[],
            ingilizceSeviyesi:"",
            simdikidil:"",
            fileList: [{
                uid: -1,
                name: 'sosya1.png',
                status: 'done',
                url: "http://brandmark.io/logo-rank/random/pepsi.png",
              }],
        };
    }

    handleCancel = () => this.setState({ previewVisible: false })

    handlePreview = (file) => {
        this.setState({
        previewImage: file.url || file.thumbUrl,
        previewVisible: true,
        });
    }

    handleChange = ({ fileList }) => this.setState({ fileList })

    userArama = (value) => {
        var that = this;
        var personelid = this.state.user.id;
        if(this.state.user.type=="personel"){
            if(value!==""){
                axios.post(ajaxURL+'/user/userbilgisearch',{search:value,personelid:personelid}).then(function(response){
                    if(response.data.status==true){
                        debugger;
                      var state = that.state;
                      if(that.state.user.yetkiler.indexOf(26)>-1){
                        state.userDataSource = response.data.result;
                      }else{
                        state.userDataSource=[];
                        for(var i in response.data.result){
                            if(response.data.result[i].personel===personelid){
                                state.userDataSource.push(response.data.result[i]);
                            }
                            }
                      }
                      
                      that.setState(state);
                    }else{
                        message.warning(response.data.message);
                    }
                  });
            }else{
                message.warning("Kulanıcı Arama Kutusu Boş Bırakılamaz");
            }
        }else{
            message.warning("Sadece Personeller Arama Yapabilir");
        }
    }

    userSecme = (value) => {
        var userid = parseInt(value);
        var state = this.state;
        state.edituser.id=userid;
        state.edituser.durum=false;
        for(var i in state.formDurum){
            state.formDurum[i]=true;
        }
        this.setState(state);
        this.bilgileriCek();
        
    }

    yetkiKontrol(useryetkilist){
        var that = this;
        var state = that.state;
        for(var i in state.yetkiler){
            if(useryetkilist.indexOf(state.yetkiler[i].id)>-1){
                state.yetkiler[i].durum=true;
            }
        }
        this.setState(state);
        this.bilgileriCek();
    }

    yetkileriGetir(){
        debugger;
        var that = this;
        var ekURL = "";
        var data = {};
        if(this.state.user.type==="personel"){
          ekURL = "/yetkilendirme/personelTumYetkiler"
          data["personelid"]=this.state.user.id;
        }else{
          ekURL = "/yetkilendirme/userTumYetkiler"
          data["userid"]=this.state.user.id;
        }
        axios.post(ajaxURL+ekURL,data).then(function(gelen){
          var state = that.state;
          state.user.yetkiler = gelen.data.result;
          that.setState(state);
          that.yetkiKontrol(gelen.data.result);
        }).catch(function(hata){
          message.error(hata.message);
        });
    }

    bilgileriCek(){
        var userid = this.state.edituser.id;
        var that = this;
        if(this.state.yetkiler.guncelleme.durum){
            if(userid>0){
                axios.post(ajaxURL+"/userbilgi/findbyid2",{userid:userid}).then(function(gelen){
                    debugger;
                    var state = that.state;
                    var data = gelen.data.result[0];
                    for(var i in data){
                        if(data[i]==null){data[i]="";}
                    }
                    state.form.adsoyad.value = data.ad+' '+data.ortaad+' '+data.soyad;
                    state.form.adres.value = data.ikametgah;
                    state.form.telefon.value = data.telefon;
                    state.form.mail.value = data.mail;
                    state.form.skype.value = data.skype;
                    state.form.watbaslama.value = data.baslangic!=="" ? moment(data.baslangic).format('DD.MM.YYYY'):"";
                    state.form.watbitis.value = data.bitis!=="" ? moment(data.bitis).format('DD.MM.YYYY'):"";
                    
                    that.setState(state);
                    that.egitimBilgileriCek();
                  }).catch(function(hata){
                    message.error(hata.message);
                  });
            }else{
                message.warning("Eğitim Bilgileri çekmek için tanımlı bir kullanıcıya rastlanamadı");
            }
            
        }
    }

    egitimBilgileriCek(){
        var userid = this.state.edituser.id;
        var that = this;
        if(this.state.yetkiler.sayfa.durum){
            if(userid>0){
                axios.post(ajaxURL+"/egitimbilgileri/findbyUserid2",{userid:userid}).then(function(gelen){
                    debugger;
                    if(gelen.data.status){
                        var gelendata = gelen.data.result;
                        var data = {
                            universite:gelendata.universiteid.isim+' - '+gelendata.fakulteid.isim+' - '+gelendata.bolumid.isim,
                            universiteil:"",
                            sonnotortalama:gelendata.sonnotortalama,
                            tahminimezuniyet:gelendata.tahminimezuniyet
                        };
                        var state = that.state;
                        state.form.egitim.list = data;
                        that.setState(state);
                        that.isdeneyimleriBilgileriCek();
                    }else{
                        message.error("İlgili Kullanıcının Bilgilerine Rastlanamadı. Lütfen Bilgileri Doldurup Kayıt Ediniz");
                    }
                  }).catch(function(hata){
                    message.error(hata.message);
                  });
            }else{
                message.warning("Eğitim Bilgileri çekmek için tanımlı bir kullanıcıya rastlanamadı");
            }
            
        }
    }

    isdeneyimleriBilgileriCek(){
        var userid = this.state.edituser.id;
        var that = this;
        if(this.state.yetkiler.sayfa.durum){
            if(userid>0){
                axios.post(ajaxURL+"/isdeneyimleri/findbyUserid",{userid:userid}).then(function(gelen){
                    debugger;
                    if(gelen.data.status){
                        var gelendata = gelen.data.result;
                        var state = that.state;
                        state.form.isdeneyimi.list = gelendata;
                        that.setState(state);
                    }else{
                        message.error("İlgili Kullanıcının Bilgilerine Rastlanamadı. Lütfen Bilgileri Doldurup Kayıt Ediniz");
                    }
                  }).catch(function(hata){
                    message.error(hata.message);
                  });
            }else{
                message.warning("Eğitim Bilgileri çekmek için tanımlı bir kullanıcıya rastlanamadı");
            }
            
        }
    }

    formGuncelle(degisken,tip,veri,dateString){
        var deger = "";
        if(tip==="input"){
            deger = veri.target.value;
        }
        if(tip==="select" || tip==="number"){
            deger = veri;
        }
        if(tip=="date"){
            deger=dateString;
        }
        if(deger!==""){
            var state = this.state;
            state.template[degisken]=deger;
            this.setState(state);
        }else{
            var state = this.state;
            state.template[degisken]="";
            this.setState(state);
        }
    }

    ingilizceListesi(){
        debugger;
        var that = this;
        axios.get(ajaxURL+"/ingilizce/list").then(function(veriler){
            if(veriler.data.status){
                var state = that.state;
                state.ingilizceListesi = veriler.data.result;
                that.setState(state);
            }else{
                message.error(veriler.data.message); 
            }
            
        }).catch(function(err){
            message.error(err.message);
        });
    }


    componentWillMount(){
        this.yetkileriGetir();
        this.ingilizceListesi();
    }

    degisiklikleriKaydet(){
        var userid = this.state.edituser.id;
    }

    yetenekEklendi(a){
        var state = this.state;
        state.form.yetenekler.list = a;
        this.setState(state);
    }

    dilDegistir(a){
        var state = this.state;
        state.form.diller.value = a.target.value;
        this.setState(state);
    }

    dilEklendi(a){
        debugger;
        var state = this.state;
        state.simdikidil = a.target.value;
        state.form.diller.list.push({dil:state.form.diller.value,seviye:state.ingilizceSeviyesi,sira:state.form.diller.list.length});
        state.form.diller.value="";
        state.ingilizceSeviyesi="";
        this.setState(state);
    }

    sporclubEklendi(a){
        var state = this.state;
        state.form.sporclub.list = a;
        this.setState(state);
    }

    hobiEklendi(a){
        var state = this.state;
        state.form.hobiler.list = a;
        this.setState(state);
    }

    egitimtablo(){
        debugger;
        const columns = [
            {title: 'Özellik',dataIndex: 'ozellik',key: 'ozellik',width:"25%"},
            {title: 'Değer',dataIndex: 'deger',key: 'deger',width:"75%"}
        ];
        var data = [
            {ozellik:"School name",deger:this.state.form.egitim.list.universite},
            {ozellik:"School City",deger:this.state.form.egitim.list.sirket},
            {ozellik:"Degree Expected",deger:this.state.form.egitim.list.sonnotortalama},
            {ozellik:"Degree Expected Date",deger:this.state.form.egitim.list.tahminimezuniyet}
        ];
        return (<Table  style={{marginBottom:"16px"}} pagination={false} dataSource={data} size="small" columns={columns} />);
    }

    ingilizceSeviyeSecimi(veri,asda){
        debugger;
        var state = this.state;
        state.ingilizceSeviyesi=asda.props.children;
        this.setState(state);
    }
    dilSil(a,b){
        debugger;
    }

    render(){
        const { previewVisible, previewImage, fileList } = this.state;
        const uploadButton = (
            <div>
                <Icon type="plus" />
                <div className="ant-upload-text">Upload</div>
            </div>
            );
        const that = this;
        const formItemLayout = {labelCol: {xs: { span: 24 },sm: { span: 6 },},wrapperCol: {xs: { span: 24 },sm: { span: 14 },},style:{marginBottom:"15px"}}
        const formItemLayoutnostyle = {labelCol: {xs: { span: 24 },sm: { span: 6 },},wrapperCol: {xs: { span: 24 },sm: { span: 14 },}}



       
        const dateFormat = 'YYYY-MM-DD';
        const userinputDisabled = this.state.yetkiler.admin.durum==true ? false:this.state.yetkiler.guncelleme.durum==true ? false:true;
        const admininputDisabled = this.state.yetkiler.admin.durum==true ? false:true;

        const bilgiDegistirme = (
            <FormItem 
            style={{marginBottom:"15px", marginTop:"30px",display:this.state.yetkiler.admin.durum==true ? "block":"none"}} 
            labelCol={formItemLayout.labelCol} 
            wrapperCol={formItemLayout.wrapperCol} 
            label="Düzenlenecek Kullanıcı : ">
                    <AutoComplete
                        defaultValue={this.state.edituser.isim}
                        style={{width:"100%",marginBottom:"16px"}}
                        size="large"
                        dataSource={this.state.userDataSource.map(renderOption)}
                        onSelect={this.userSecme}
                        onSearch={this.userArama}
                        placeholder="Öğrenci Ad Soyad, Mail, Telefon"
                        optionLabelProp="text"
                        ><Input suffix={<Icon type="search" className="certain-category-icon" />} />
                    </AutoComplete>
            </FormItem>
            )
        
        const isdeneyimtablosu = this.state.form.isdeneyimi.list.map((item,sira)=>{
            const columns = [
                {title: 'Özellik',dataIndex: 'ozellik',key: 'ozellik',width:"25%"},
                {title: 'Değer',dataIndex: 'deger',key: 'deger',width:"75%"}
            ];
            var data = [
                {ozellik:"Çalıştığı Tarihler",deger:"Başlangıç : "+moment(item.baslangic).format('MM/DD/YYYY')+" - Bitiş : "+moment(item.bitis).format('MM/DD/YYYY')},
                {ozellik:"Çalıştığı Şirket",deger:item.sirket},
                {ozellik:"Şirket Yöneticisi",deger:item.referans},
                {ozellik:"Yönetici Telefonu",deger:item.telefon},
                {ozellik:"Çalıştığı Pozisyon",deger:item.pozisyon},
                {ozellik:"Pozisyon Bilgisi",deger:item.aciklama},
                {ozellik:"Adres",deger:item.adres}
            ];
            return (<Table  style={{marginBottom:"16px"}} pagination={false} dataSource={data} size="small" columns={columns} />);
        });

        const ingilzicelistesi = this.state.ingilizceListesi.map((item,sira)=>{
            return (<Option value={item.id}>{item.isim}</Option>);
        });

        const columns = [
            {title: 'Girilen Dil',dataIndex: 'dil',key: 'dil',width:"40%"},
            {title: 'Seviye',dataIndex: 'seviye',key: 'seviye',width:"50%"},
            {title: 'Sil',dataIndex: 'sil',key: 'sil',width:"10%",render:()=>{<Button onClick={this.dilSil.bind(this)}>Sil</Button>}}
        ];

        

        

        if(this.state.yetkiler.sayfa.durum===true){
            return(
                <LayoutWrapper>
                    <Breadcrumb style={{backgroundColor:"#fff",padding:"10px",width:"100%",marginBottom:"5px"}}>
                            <Breadcrumb.Item>Work and Travel</Breadcrumb.Item>
                            <Breadcrumb.Item>Özgeçmiş Bilgilerin Girişi</Breadcrumb.Item>
                            <Breadcrumb.Item>{this.state.edituser.isim!=="" ? this.state.edituser.isim:"Seçim Yapılmamış"}</Breadcrumb.Item>
                    </Breadcrumb>
                    <Card title="4. Adım - Özgeçmiş Bilgilerinin Girilmesi" style={{width:"100%"}} bodyStyle={{padding:"0"}}>
                    <Row style={{width:"100%"}}>
                        <Col lg={24} md={24} sm={24} xs={24}>
                            <Card bodyStyle={{padding:"10px"}}>
                                <Form>
                                {bilgiDegistirme} 

                                <FormItem style={{display:this.state.yetkiler.sayfa.durum==true ? "block":"none",marginBottom:"15px"}} {...formItemLayoutnostyle} label="Ad Soyad ">
                                    {this.state.form.adsoyad.value!=="" ? this.state.form.adsoyad.value:<span style={{color:"#f44336"}}>Bilgi Yok</span>}
                                </FormItem> 

                                <FormItem style={{display:this.state.yetkiler.sayfa.durum==true ? "block":"none",marginBottom:"15px"}} 
                                {...formItemLayoutnostyle} label="Adres ">
                                    {this.state.form.adres.value!=="" ? this.state.form.adres.value:<span style={{color:"#f44336"}}>Bilgi Yok</span>}
                                </FormItem>

                                <FormItem style={{display:this.state.yetkiler.sayfa.durum==true ? "block":"none",marginBottom:"15px"}} 
                                {...formItemLayoutnostyle} label="Telefon ">
                                    {this.state.form.telefon.value!=="" ? this.state.form.telefon.value:<span style={{color:"#f44336"}}>Bilgi Yok</span>}
                                </FormItem>

                                <FormItem style={{display:this.state.yetkiler.sayfa.durum==true ? "block":"none",marginBottom:"15px"}} 
                                {...formItemLayoutnostyle} label="E-Mail ">
                                    {this.state.form.mail.value!=="" ? this.state.form.mail.value:<span style={{color:"#f44336"}}>Bilgi Yok</span>}
                                </FormItem>

                                <FormItem style={{display:this.state.yetkiler.sayfa.durum==true ? "block":"none",marginBottom:"15px"}} 
                                {...formItemLayoutnostyle} label="Skype ">
                                    {this.state.form.skype.value!=="" ? this.state.form.skype.value:<span style={{color:"#f44336"}}>Bilgi Yok</span>}
                                </FormItem>

                                <FormItem style={{display:this.state.yetkiler.sayfa.durum==true ? "block":"none",marginBottom:"15px"}} 
                                {...formItemLayoutnostyle} label="Work & Travel Başlama Tarihi ">
                                    {this.state.form.watbaslama.value!=="" ? this.state.form.watbaslama.value:<span style={{color:"#f44336"}}>Bilgi Yok</span>}
                                </FormItem>

                                <FormItem style={{display:this.state.yetkiler.sayfa.durum==true ? "block":"none",marginBottom:"15px"}} 
                                {...formItemLayoutnostyle} label="Work & Travel Bitiş Tarihi ">
                                    {this.state.form.watbitis.value!=="" ? this.state.form.watbitis.value:<span style={{color:"#f44336"}}>Bilgi Yok</span>}
                                </FormItem>

                                <FormItem style={{display:this.state.yetkiler.sayfa.durum==true ? "block":"none",marginBottom:"15px"}} 
                                {...formItemLayoutnostyle} label="Who am I ? ">
                                   <TextArea value={this.state.form.whoami.value} rows={3}></TextArea>
                                </FormItem>

                                <FormItem style={{display:this.state.yetkiler.sayfa.durum==true ? "block":"none",marginBottom:"15px"}} 
                                {...formItemLayoutnostyle} label="Why am i Participating in the Summer Work Travel Program ? ">
                                    <TextArea value={this.state.form.whyami.value} rows={3}></TextArea>
                                </FormItem>

                                <FormItem style={{display:this.state.yetkiler.sayfa.durum==true ? "block":"none",marginBottom:"15px"}} 
                                {...formItemLayoutnostyle} label="My main qualities and abilities ">
                                    <Select
                                    mode="tags"
                                    style={{ width: '100%' }}
                                    onChange={this.yetenekEklendi.bind(this)}
                                    tokenSeparators={[',']}
                                    >
                                    </Select>
                                </FormItem>

                                <FormItem style={{display:this.state.yetkiler.sayfa.durum==true ? "block":"none",marginBottom:"15px"}} 
                                {...formItemLayoutnostyle} label="Education Group ">
                                    <Card
                                    bodyStyle={{margin:"0",padding:"0"}}
                                    title={"Education Group"}
                                    >
                                    {this.egitimtablo()}

                                    </Card>
                                </FormItem>

                                <FormItem style={{display:this.state.yetkiler.sayfa.durum==true ? "block":"none",marginBottom:"15px"}} 
                                {...formItemLayoutnostyle} label="İş Deneyimleri ">
                                    <Card
                                    bodyStyle={{margin:"0",padding:"10px"}}
                                    title={"İş Deneyimleri"}
                                    >
                                    {this.state.form.isdeneyimi.list.length>0 ? isdeneyimtablosu : <span style={{color:"#f44336"}}>İş Deneyimi Yok</span>}
                                    </Card>
                                </FormItem>

                                <FormItem style={{display:this.state.yetkiler.sayfa.durum==true ? "block":"none",marginBottom:"15px"}} 
                                {...formItemLayoutnostyle} label="Staj Grubu ">
                                    <Card
                                    bodyStyle={{margin:"0",padding:"0"}}
                                    title={"Staj Bilgileri"}
                                    extra= {<Button style={{borderRadius:"5px"}} size="small" type="primary">Yeni Staj Ekle</Button>}
                                    >

                                    </Card>
                                </FormItem>


                                <FormItem style={{display:this.state.yetkiler.sayfa.durum==true ? "block":"none",marginBottom:"15px"}} 
                                {...formItemLayoutnostyle} label="Aktivite Grubu ">
                                    <Card
                                    bodyStyle={{margin:"0",padding:"10px"}}
                                    title={"Aktivite Bilgileri"}
                                    > 
                                    <Form>
                                        <FormItem {...formItemLayoutnostyle}  label="Bildiğiniz Diller ">
                                            <InputGroup>
                                                <Col span={10}>
                                                    <Input placeholder="" value={this.state.form.diller.value} onChange={this.dilDegistir.bind(this)}/>
                                                </Col>
                                                <Col span={10}>
                                                    <Select style={{width:"100%"}} onChange={this.ingilizceSeviyeSecimi.bind(this)}>
                                                    <Option value="" selected>Seviyeler</Option>
                                                        {ingilzicelistesi}
                                                    </Select>
                                                </Col>
                                                <Col span={4}>
                                                    <Button onClick={this.dilEklendi.bind(this)}>Ekle</Button>
                                                </Col>
                                            </InputGroup>
                                        </FormItem>
                                        <FormItem {...formItemLayoutnostyle}  label="Girilen Diller ">
                                            <Table  style={{marginBottom:"16px"}} pagination={false} dataSource={this.state.form.diller.list} size="small" columns={columns} />
                                        </FormItem>
                                        <FormItem {...formItemLayoutnostyle}  label="Hobileriniz ">
                                            <Select
                                            mode="tags"
                                            style={{ width: '100%' }}
                                            onChange={this.hobiEklendi.bind(this)}
                                            tokenSeparators={[',']}
                                            >
                                            </Select>
                                        </FormItem>
                                        <FormItem {...formItemLayoutnostyle}  label="Üye Olduğunuz Spor ve Klüpler ">
                                            <Select
                                            mode="tags"
                                            style={{ width: '100%' }}
                                            onChange={this.sporclubEklendi.bind(this)}
                                            
                                            tokenSeparators={[',']}
                                            >
                                            </Select>
                                        </FormItem>
                                    </Form>

                                    </Card>
                                </FormItem>


                                <FormItem {...formItemLayout} label="3 Adet Sosyal Yaşantıdan Fotoğraf :">
                                    <Upload
                                    action="http://jsonplaceholder.typicode.com/posts/"
                                    listType="picture-card"
                                    fileList={fileList}
                                    onPreview={this.handlePreview}
                                    onChange={this.handleChange}
                                    >
                                    {fileList.length >= 3 ? null : uploadButton}
                                    </Upload>
                                    
                                    <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
                                    <img alt="example" style={{ width: '100%' }} src={previewImage} />
                                    </Modal>
                                </FormItem>

                                <FormItem style={{display:this.state.form.isdeneyimi.disabled==true ? "block":"none",marginBottom:"15px"}} {...formItemLayoutnostyle} label="Ad Soyad ">
                                    <Input disabled={userinputDisabled} value={"deger"} onChange={this.formGuncelle.bind(this,'sirket','input')} />
                                </FormItem>

                                <FormItem style={{textAlign:"right",display:this.state.yetkiler.guncelleme.durum==false ? "block":"none"}} labelCol={formItemLayout.labelCol} wrapperCol={formItemLayout.wrapperCol} label="Sisteme Kaydet">
                                    İşlem Yapma Yetkiniz Bulunmamaktadır. İrtibat : <a target="_blank" href="mailto:yardim@westline.com.tr">yardim@westline.com.tr</a>
                                </FormItem>

                                <FormItem style={{textAlign:"right",display:this.state.yetkiler.guncelleme.durum==true ? "block":this.state.yetkiler.admin.durum==true ? "block":"none"}} labelCol={formItemLayout.labelCol} wrapperCol={formItemLayout.wrapperCol} label="Sisteme Kaydet">
                                <Popconfirm title={"Buraya Soru Gelecek"} 
                                onConfirm={this.degisiklikleriKaydet.bind(this)} okText="Evet" cancelText="Hayır">
                                        <Button style={{borderRadius:"5px"}} type="primary" >Değişiklikleri Kaydet</Button>
                                    </Popconfirm>
                                </FormItem>
    
                                </Form>
                            </Card>
                        </Col>
                    </Row>
                    </Card>
                </LayoutWrapper>
            );
        }else{
            return (
                    <LayoutWrapper>
                    </LayoutWrapper>
                    );
        }
    }
}

export default Cvolusturma;