import React, { Component } from "react";
import LayoutWrapper from '../../../components/utility/layoutWrapper.js';
import PageHeader from '../../../components/utility/pageHeader';
import { Form, Input, Tooltip, Icon, Upload, Modal, Cascader, Select, Breadcrumb, Row, Col, Checkbox, Radio, Button, AutoComplete, Card, Table, DatePicker, InputNumber, message, Popconfirm } from 'antd';
import config2 from '../../../settings/config';
import axios from 'axios';
import moment from 'moment';
const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;
const { TextArea } = Input;
const { RangePicker } = DatePicker;
const ajaxURL = config2.http+config2.server+':'+config2.port;

function renderOption(item) {
    return (
      <Option key={item.id} text={item.isim}>{item.isim}</Option>
    );
}

class Isdeneyimleri extends Component {
    constructor(){
        super();
        this.state={
            user:localStorage.getItem('user')!==null ? JSON.parse(localStorage.getItem('user')):{id:-1,durum:false,type:"user"},
            /* edituser:localStorage.getItem('editUser')!==null ? JSON.parse(localStorage.getItem('editUser')):{id:1,durum:true}, */
            edituser:window.editUser,
            yetkiler:{
                sayfa:{id:20,message:"Sayfayı Açma Yetkiniz Yoktur",durum:false},
                guncelleme:{id:21,message:"Kişisel Bilgileri Düzeltme Yetkiniz Yoktur",durum:false},
                admin:{id:22,message:"Bu Sayfada Adminlik Yetkiniz Yoktur",durum:false}
            },
            userDataSource:[],
            form:{
                isdeneyimi:{list:[],value:"",disabled:false},
                deneyimler:[],
                eskideneyimler:[]
            },
            template:{sirket:"",adres:"",referans:"",telefon:"",pozisyon:"",aciklama:"",baslangic:"",bitis:""},
        };
    }

    userArama = (value) => {
        var that = this;
        var personelid = this.state.user.id;
        if(this.state.user.type=="personel"){
            if(value!==""){
                axios.post(ajaxURL+'/user/userbilgisearch',{search:value,personelid:personelid}).then(function(response){
                    if(response.data.status==true){
                        debugger;
                      var state = that.state;
                      if(that.state.user.yetkiler.indexOf(26)>-1){
                        state.userDataSource = response.data.result;
                      }else{
                        state.userDataSource=[];
                      for(var i in response.data.result){
                          if(response.data.result[i].personel===personelid){
                            state.userDataSource.push(response.data.result[i]);
                          }
                        }
                      }
                      
                      that.setState(state);
                    }else{
                        message.warning(response.data.message);
                    }
                  });
            }else{
                message.warning("Kulanıcı Arama Kutusu Boş Bırakılamaz");
            }
        }else{
            message.warning("Sadece Personeller Arama Yapabilir");
        }
    }

    userSecme = (value) => {
        var userid = parseInt(value);
        var state = this.state;
        state.edituser.id=userid;
        state.edituser.durum=false;
        state.form.deneyimler=[];
        state.form.eskideneyimler=[];
        state.form.isdeneyimi.disabled=true;
        state.template = {sirket:"",adres:"",referans:"",telefon:"",pozisyon:"",aciklama:"",baslangic:"",bitis:""};
        for(var i in state.formDurum){
            state.formDurum[i]=true;
        }
        this.setState(state);
        this.bilgileriCek(false);
        
    }

    isDeneyimiRadioChange(durum){
        debugger;
        var deger = durum.target.value;
        var state = this.state;
        state.form.isdeneyimi.disabled=deger;
        this.setState(state);
    }


    yetkiKontrol(useryetkilist){
        var state = this.state;
        for(var i in state.yetkiler){
            if(useryetkilist.indexOf(state.yetkiler[i].id)>-1){
                state.yetkiler[i].durum=true;
            }
        }
        this.setState(state);
        this.bilgileriCek();
    }

    yetkileriGetir(){
        var that = this;
        var ekURL = "";
        var data = {};
        if(this.state.user.type==="personel"){
          ekURL = "/yetkilendirme/personelTumYetkiler"
          data["personelid"]=this.state.user.id;
        }else{
          ekURL = "/yetkilendirme/userTumYetkiler"
          data["userid"]=this.state.user.id;
        }
        axios.post(ajaxURL+ekURL,data).then(function(gelen){
          var state = that.state;
          state.user.yetkiler = gelen.data.result;
          that.setState(state);
          that.yetkiKontrol(gelen.data.result);
        }).catch(function(hata){
          message.error(hata.message);
        });
    }

    bilgileriCek(){
        var userid = this.state.edituser.id;
        var that = this;
        if(this.state.yetkiler.guncelleme.durum){
            if(userid>0){
                axios.post(ajaxURL+"/isdeneyimleri/findbyUserid",{userid:userid}).then(function(gelen){
                    debugger;
                    if(gelen.data.status){
                        var gelendata = gelen.data.result;
                        var state = that.state;
                        if(gelendata.length>0){
                            state.form.isdeneyimi.disabled=true;
                        }else{
                            state.form.isdeneyimi.disabled=false;
                        }

                        for(var i in gelendata){
                            var ekle = gelendata[i];
                            var obje = {};
                            for(var j in state.template){
                                if(typeof ekle[j]!=="undefined"){
                                    obje[j]=ekle[j];
                                }
                            }
                            obje.id=ekle.id;
                            state.form.eskideneyimler.push(obje);
                        }
                        that.setState(state);
                    }else{
                        message.error("İlgili Kullanıcının Bilgilerine Rastlanamadı. Lütfen Bilgileri Doldurup Kayıt Ediniz");
                    }
                  }).catch(function(hata){
                    message.error(hata.message);
                  });
            }else{
                message.warning("Eğitim Bilgileri çekmek için tanımlı bir kullanıcıya rastlanamadı");
            }
            
        }
    }

    formGuncelle(degisken,tip,veri,dateString){
        var deger = "";
        if(tip==="input"){
            deger = veri.target.value;
        }
        if(tip==="select" || tip==="number"){
            deger = veri;
        }
        if(tip=="date"){
            deger=dateString;
        }
        if(deger!==""){
            var state = this.state;
            state.template[degisken]=deger;
            this.setState(state);
        }else{
            var state = this.state;
            state.template[degisken]="";
            this.setState(state);
        }
    }

    yeniBirIsDeneyimiEkle(){
        debugger;
        var state = this.state;
        var template = state.template;
        var durum = false;
        for(var i in template){
            if(template[i]==""){
                durum = true; 
            }
        }
        if(durum){
            message.warning("Girdiğiniz Bilgilerde Eksik Alanlar Var Lütfen Boş Bırakmayınız");
        }else{
            state.form.deneyimler.push(template);
            state.template = {id:Date.now(),sirket:"",adres:"",referans:"",telefon:"",pozisyon:"",aciklama:"",baslangic:"",bitis:""};
            this.setState(state);
        }
    }

    baslangicBitisDegistir(date,dateString){
        debugger;
        var baslangic = dateString[0];
        var bitis = dateString[1];
        var state = this.state;
        state.template.baslangic=baslangic;
        state.template.bitis=bitis;
        this.setState(state);
    }

    componentWillMount(){
        this.yetkileriGetir();
    }

    degisiklikleriKaydet(){
        var userid = this.state.edituser.id;
        var deneyimler = this.state.form.deneyimler;
        for(var i in deneyimler){
            deneyimler[i]["userid"]=userid;
        }
        var deneyimsayisi  = deneyimler.length;
        if(deneyimler.length>0){
            axios.post(ajaxURL+"/isdeneyimleri/multiinsert",{multi:deneyimler}).then(function(gelen){
                if(gelen.data.status){
                    message.success("İş Deneyimleriniz sisteme eklenmiştir");
                }else{
                    message.warning("İş Deneyimleriniz Malesef Eklenemedi");
                }
            }).catch(function(hata){
                message.warning(hata.message);
            });
        }else{
            message.warning("Hiç Deneyiminiz Yok Olarak Kayıt Edildiniz. ");
        }
    }

    isDeneyiminiSistemdenSil(id){
        debugger;
        if(id>0){
            axios.post(ajaxURL+"/isdeneyimleri/deleteById",{id:id}).then(function(gelen){
                if(gelen.data.status){
                    message.success("İş Deneyimi Sistemden Silinmiştir");
                }else{
                    message.warning("Malesef İş Deneyimini Sistemden silemedik");
                }
            }).catch(function(hata){
                message.warning(hata.message);
            });
        }
    }

    buIsDeneyiminiSil(id,tur,ths){
        debugger;
        var state = this.state;
        if(tur==="eski"){
            var sil = -1;
            var silid=false;
            for(var i in state.form.eskideneyimler){
                var deneyim =state.form.eskideneyimler[i];
                var xid = deneyim.id;
                if(id===xid){
                    silid=id;
                    sil=i;
                    break;  
                }
            }
            if(sil!==-1){
                state.form.eskideneyimler.splice(sil,1);
            }
            this.isDeneyiminiSistemdenSil(silid);
            
        }

        if(tur==="yeni"){
            var sil = -1;
            for(var i in state.form.deneyimler){
                var deneyim = state.form.deneyimler[i];
                var xid = deneyim.id;
                if(id===xid){
                    sil=i;
                    break;  
                }
            }
            if(sil!==-1){
                state.form.deneyimler.splice(sil,1);
            }
        }
        this.setState(state);

    }

    


    render(){
        const that = this;
        const formItemLayout = {labelCol: {xs: { span: 24 },sm: { span: 6 },},wrapperCol: {xs: { span: 24 },sm: { span: 14 },},style:{marginBottom:"15px"}}
        const formItemLayoutnostyle = {labelCol: {xs: { span: 24 },sm: { span: 6 },},wrapperCol: {xs: { span: 24 },sm: { span: 14 },}}

        const Isdeneyimtablolari = this.state.form.deneyimler.map((item,sira)=>{

            const columns = [
                {title: 'Özellik',dataIndex: 'ozellik',key: 'ozellik',width:"25%"},
                {title: 'Değer',dataIndex: 'deger',key: 'deger',width:"75%"}
            ];
            var data = [
                {ozellik:"Çalıştığı Tarihler",deger:"Başlangıç : "+moment(item.baslangic).format('MM/DD/YYYY')+" - Bitiş : "+moment(item.bitis).format('MM/DD/YYYY')},
                {ozellik:"Çalıştığı Şirket",deger:item.sirket},
                {ozellik:"Şirket Yöneticisi",deger:item.referans},
                {ozellik:"Yönetici Telefonu",deger:item.telefon},
                {ozellik:"Çalıştığı Pozisyon",deger:item.pozisyon},
                {ozellik:"Pozisyon Bilgisi",deger:item.aciklama},
                {ozellik:"Adres",deger:item.adres}
            ];

            return (<Table  title={() => <h3>{item.sirket} - <Popconfirm title="Bu İş Deneyimini Silmeye Emin misiniz ?" onConfirm={that.buIsDeneyiminiSil.bind(that,item.id,'yeni')} okText="Evet" cancelText="Hayır" ><span style={{color:"#f00",cursor:"pointer"}}>Sil</span></Popconfirm></h3>}  style={{marginBottom:"16px"}} pagination={false} dataSource={data} size="small" columns={columns} />);
        });

        const eskiIsdeneyimtablolaris = this.state.form.eskideneyimler.map((item,sira)=>{

            const columns = [
                {title: 'Özellik',dataIndex: 'ozellik',key: 'ozellik',width:"25%"},
                {title: 'Değer',dataIndex: 'deger',key: 'deger',width:"75%"}
            ];
            var data = [
                {ozellik:"Çalıştığı Tarihler",deger:"Başlangıç : "+moment(item.baslangic).format('MM/DD/YYYY')+" - Bitiş : "+moment(item.bitis).format('MM/DD/YYYY')},
                {ozellik:"Çalıştığı Şirket",deger:item.sirket},
                {ozellik:"Şirket Yöneticisi",deger:item.referans},
                {ozellik:"Yönetici Telefonu",deger:item.telefon},
                {ozellik:"Çalıştığı Pozisyon",deger:item.pozisyon},
                {ozellik:"Pozisyon Bilgisi",deger:item.aciklama},
                {ozellik:"Adres",deger:item.adres}
            ];

            return (<Table title={() => <h3>{item.sirket} - <Popconfirm title="Bu İş Deneyimini Silmeye Emin misiniz ?" onConfirm={that.buIsDeneyiminiSil.bind(that,item.id,'eski')} okText="Evet" cancelText="Hayır" ><span style={{color:"#f00",cursor:"pointer"}}>Sil</span></Popconfirm></h3>}  style={{marginBottom:"16px"}} pagination={false} dataSource={data} size="small" columns={columns} />);
        });
        //template:{userid:"",sirket:"",adres:"",referans:"",telefon:"",pozisyon:"",aciklama:"",baslangic:"",bitis:""},
       
        const dateFormat = 'YYYY-MM-DD';
        const userinputDisabled = this.state.yetkiler.admin.durum==true ? false:this.state.yetkiler.guncelleme.durum==true ? false:true;
        const admininputDisabled = this.state.yetkiler.admin.durum==true ? false:true;

        const bilgiDegistirme = (
            <FormItem 
            style={{marginBottom:"15px", marginTop:"30px",display:this.state.yetkiler.admin.durum==true ? "block":"none"}} 
            labelCol={formItemLayout.labelCol} 
            wrapperCol={formItemLayout.wrapperCol} 
            label="Düzenlenecek Kullanıcı : ">
                    <AutoComplete
                        defaultValue={this.state.edituser.isim}
                        style={{width:"100%",marginBottom:"16px"}}
                        size="large"
                        dataSource={this.state.userDataSource.map(renderOption)}
                        onSelect={this.userSecme}
                        onSearch={this.userArama}
                        placeholder="Öğrenci Ad Soyad, Mail, Telefon"
                        optionLabelProp="text"
                        ><Input suffix={<Icon type="search" className="certain-category-icon" />} />
                    </AutoComplete>
            </FormItem>
            )

        if(this.state.yetkiler.sayfa.durum===true){
            return(
                <LayoutWrapper>
                    <Breadcrumb style={{backgroundColor:"#fff",padding:"10px",width:"100%",marginBottom:"5px"}}>
                            <Breadcrumb.Item>Work and Travel</Breadcrumb.Item>
                            <Breadcrumb.Item>İş Deneyimlerinin Girişi</Breadcrumb.Item>
                            <Breadcrumb.Item>{this.state.edituser.isim!=="" ? this.state.edituser.isim:"Seçim Yapılmamış"}</Breadcrumb.Item>
                    </Breadcrumb>
                    <Card title="3. Adım - İş Deneyimlerinin Girilmesi" style={{width:"100%"}} bodyStyle={{padding:"0"}}>
                    <Row style={{width:"100%"}}>
                        <Col lg={24} md={24} sm={24} xs={24}>
                            <Card bodyStyle={{padding:"10px"}}>
                                <Form>
                                {bilgiDegistirme}  
                                <FormItem {...formItemLayout} label="Daha Önce Full-Part Time Çalıştınız mı? : ">
                                    <RadioGroup onChange={this.isDeneyimiRadioChange.bind(this)} value={this.state.form.isdeneyimi.disabled}>
                                        <Radio value={true}>Evet</Radio>
                                        <Radio value={false}>Hayır</Radio>
                                    </RadioGroup>
                                </FormItem>

                                
                                <FormItem style={{display:this.state.form.isdeneyimi.disabled==true ? "block":"none",marginBottom:"15px"}} {...formItemLayoutnostyle} label=" ">
                                    <h3 style={{display:this.state.form.isdeneyimi.disabled==true ? "block":"none",fontWeight:"bold"}}>İş Deneyimlerinizi Giriniz</h3>
                                </FormItem>

                                <FormItem style={{display:this.state.form.isdeneyimi.disabled==true ? "block":"none",marginBottom:"15px"}} {...formItemLayoutnostyle} label="Hanfgi Şirkette Çalıştınız : ">
                                    <Input disabled={userinputDisabled} value={this.state.template.sirket} onChange={this.formGuncelle.bind(this,'sirket','input')} />
                                </FormItem>

                                <FormItem style={{display:this.state.form.isdeneyimi.disabled==true ? "block":"none",marginBottom:"15px"}} {...formItemLayoutnostyle} label="Yönetici Adı : ">
                                    <Input disabled={userinputDisabled}  value={this.state.template.referans} onChange={this.formGuncelle.bind(this,'referans','input')} />
                                </FormItem>

                                <FormItem style={{display:this.state.form.isdeneyimi.disabled==true ? "block":"none",marginBottom:"15px"}} {...formItemLayoutnostyle} label="Yönetici Telefon Numarası : ">
                                    <Input disabled={userinputDisabled}  value={this.state.template.telefon} onChange={this.formGuncelle.bind(this,'telefon','input')} />
                                </FormItem>

                                <FormItem style={{display:this.state.form.isdeneyimi.disabled==true ? "block":"none",marginBottom:"15px"}} {...formItemLayoutnostyle} label="Çalıştığınız Pozisyon Nedir : ">
                                    <Input disabled={userinputDisabled}  value={this.state.template.pozisyon} onChange={this.formGuncelle.bind(this,'pozisyon','input')} />
                                </FormItem>

                                <FormItem style={{display:this.state.form.isdeneyimi.disabled==true ? "block":"none",marginBottom:"15px"}} {...formItemLayoutnostyle} label="Pozisyonunun Hakkında Açıklama : ">
                                    <TextArea disabled={userinputDisabled}  value={this.state.template.aciklama} onChange={this.formGuncelle.bind(this,'aciklama','input')} />
                                </FormItem>

                                <FormItem style={{display:this.state.form.isdeneyimi.disabled==true ? "block":"none",marginBottom:"15px"}} {...formItemLayoutnostyle} label="Çalıştığınız Yerin Adresi : ">
                                    <TextArea disabled={userinputDisabled}  value={this.state.template.adres} onChange={this.formGuncelle.bind(this,'adres','input')} />
                                </FormItem>

                                <FormItem style={{display:this.state.form.isdeneyimi.disabled==true ? "block":"none",marginBottom:"15px"}} {...formItemLayoutnostyle} label="Çalıştığınız Yerin Adresi : ">
                                    <RangePicker 
                                        disabled={userinputDisabled}
                                        value={[this.state.template.baslangic!=="" ? moment(this.state.template.baslangic, dateFormat):moment(),this.state.template.bitis==true ? moment(this.state.template.bitis, dateFormat):moment()]} 
                                        format="YYYY-MM-DD" 
                                        onChange={this.baslangicBitisDegistir.bind(this)} />
                                </FormItem>

                                <FormItem style={{display:this.state.form.isdeneyimi.disabled==true ? "block":"none",marginBottom:"15px"}} {...formItemLayoutnostyle} label="Yeni Bir İş Deneyimi Ekle : ">
                                    <Button onClick={this.yeniBirIsDeneyimiEkle.bind(this)} style={{borderRadius:"5px"}} type="default" >Yeni Bir İş Deneyimi Ekle</Button>
                                </FormItem>

                                <FormItem style={{display:this.state.form.isdeneyimi.disabled==true ? "block":"none",marginBottom:"15px"}} {...formItemLayoutnostyle} label="Kayıt Edilen İş Deneyimleri">
                                {Isdeneyimtablolari}
                                {eskiIsdeneyimtablolaris}
                                </FormItem>

                                

                                

    
                                

                                <FormItem style={{textAlign:"right",display:this.state.yetkiler.guncelleme.durum==false ? "block":"none"}} labelCol={formItemLayout.labelCol} wrapperCol={formItemLayout.wrapperCol} label="Sisteme Kaydet">
                                    İşlem Yapma Yetkiniz Bulunmamaktadır. İrtibat : <a target="_blank" href="mailto:yardim@westline.com.tr">yardim@westline.com.tr</a>
                                </FormItem>

                                <FormItem style={{textAlign:"right",display:this.state.yetkiler.guncelleme.durum==true ? "block":this.state.yetkiler.admin.durum==true ? "block":"none"}} labelCol={formItemLayout.labelCol} wrapperCol={formItemLayout.wrapperCol} label="Sisteme Kaydet">
                                <Popconfirm 
                                title={this.state.form.deneyimler.length>0 ? "İş Deneyim(ler)inizi Kaydetmek İstediğinizden Emin misiniz?":"Deneyiminiz Yok Olarak Kayıt Edilecek Emin misiniz ?"} 
                                onConfirm={this.degisiklikleriKaydet.bind(this)} okText="Evet" cancelText="Hayır">
                                        <Button style={{borderRadius:"5px"}} type="primary" >Değişiklikleri Kaydet</Button>
                                    </Popconfirm>
                                </FormItem>
    
                                </Form>
                            </Card>
                        </Col>
                    </Row>
                    </Card>
                </LayoutWrapper>
            );
        }else{
            return (
                    <LayoutWrapper>

                    </LayoutWrapper>
                    );
        }
    }
}

export default Isdeneyimleri;