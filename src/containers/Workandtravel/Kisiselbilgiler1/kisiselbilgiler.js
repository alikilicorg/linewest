import React, { Component } from "react";
import LayoutWrapper from '../../../components/utility/layoutWrapper.js';
import PageHeader from '../../../components/utility/pageHeader';
import config2 from '../../../settings/config';
import axios from 'axios';
import moment from 'moment';
import { Form, Input, Icon, Upload, Modal, Select, Row, Col, Button, AutoComplete, Card, Table, DatePicker, InputNumber, message, Popconfirm, Breadcrumb  } from 'antd';

const FormItem = Form.Item;
const Option = Select.Option;
//const AutoCompleteOption = AutoComplete.Option;
const { TextArea } = Input;
const { RangePicker } = DatePicker;

const ajaxURL = config2.http+config2.server+':'+config2.port;


function renderOption(item) {
    return (
      <Option key={item.id} text={item.isim}>{item.isim}</Option>
    );
  }

class Kisiselbilgiler extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user:localStorage.getItem('user')!==null ? JSON.parse(localStorage.getItem('user')):{id:-1,durum:false,type:"user"},
            /* edituser:localStorage.getItem('editUser')!==null ? JSON.parse(localStorage.getItem('editUser')):{id:1,durum:true}, */
            edituser:window.editUser,
            yetkiler:{
                sayfa:{id:12,message:"Sayfayı Açma Yetkiniz Yoktur",durum:false},
                guncelleme:{id:16,message:"Kişisel Bilgileri Güncelleme Yetkiniz Yoktur",durum:false},
                admin:{id:15,message:"Kullanıcıların Bilgisini Değiştirme Yetkiniz Yoktur",durum:false}
            },
            disabled:true,

            taksit:[],
            previewVisible: false,
            previewImage: '',
            fileList: [{
                uid: -1,
                name: 'biometrik.png',
                status: 'done',
                url: "",
              }],
            ulkeListesi:[],
            sehirListesi:[{id:0,isim:"Ülke Seçiniz"}],
            uyrukListesi:[],
            userDataSource:[],
            ingilizceListesi:[],
            ehliyetListesi:[],
            paketListesi:[],
            cinsiyetListesi:[],
            personelListesi:[],
            reklamListesi:[],
            formDurum:{
                personel:true,paket:true,tc:true,ad:true,ortaad:true,soyad:true,cinsiyet:true,telefon:true,ehliyet:true,mail:true,baslangic:true,bitis:true,ingilizce:true,reklam:true,kayit:true,ikametgah:true,postakodu:true,pasaport:true,dogum:true,sehir:true,ulke:true,uyruk:true,ikametulke:true,anneadi:true,babaadi:true,acilkisi:true,aciltelefon:true,acilyakinlik:true,skype:true,resim:true
            },
            form:{
                personel:"",paket:"",tc:"",ad:"",ortaad:"",soyad:"",cinsiyet:"",telefon:"",ehliyet:"",mail:"",baslangic:"",bitis:"",ingilizce:"",reklam:"",kayit:"",ikametgah:"",postakodu:"",pasaport:"",dogum:"",sehir:"",ulke:"",uyruk:"",ikametulke:"",anneadi:"",babaadi:"",acilkisi:"",aciltelefon:"",acilyakinlik:"",skype:"",resim:""
            }
        };
    }

    userArama = (value) => {
        var that = this;
        var personelid = this.state.user.id;
        if(this.state.user.type=="personel"){
            if(value!==""){
                axios.post(ajaxURL+'/user/userbilgisearch',{search:value,personelid:personelid}).then(function(response){
                    if(response.data.status==true){
                      var state = that.state;
                      if(that.state.user.yetkiler.indexOf(26)>-1){
                        state.userDataSource = response.data.result;
                      }else{
                        state.userDataSource=[];
                      for(var i in response.data.result){
                          if(response.data.result[i].personel===personelid){
                            state.userDataSource.push(response.data.result[i]);
                          }
                        }
                      }
                      
                      that.setState(state);
                    }else{
                        message.warning(response.data.message);
                    }
                  });
            }else{
                message.warning("Kulanıcı Arama Kutusu Boş Bırakılamaz");
            }
        }else{
            message.warning("Sadece Personeller Arama Yapabilir");
        }
    }

    userSecme = (value) => {
        var userid = parseInt(value);
        var state = this.state;
        state.edituser.id=userid;
        state.edituser.durum=false;
        for(var i in state.formDurum){
            state.formDurum[i]=true;
        }
        this.setState(state);
        this.bilgileriCek(false);
        
    }

    handleCancel = () => this.setState({ previewVisible: false })

    handlePreview = (file) => {
        this.setState({
        previewImage: file.url || file.thumbUrl,
        previewVisible: true,
        });
    }

    handleChange = ({ fileList }) => this.setState({ fileList })

    taksitleriCek(){
        var that = this;
        axios.post(ajaxURL+"/taksit/findByUserid",{userid:this.state.edituser.id}).then(function(veriler){
            var veri = veriler.data.result;
            var aylar = ["Ocak","Şubat","Mart","Nisan","Mayıs","Haziran","Temmuz","Ağustos","Eylül","Ekim","Kasım","Aralık"];
            for(var i=0;i<veri.length;i++){
                veri[i]["sira"]=i+1;
                if(veri[i]["durum"]){veri[i]["durum"]="Ödendi";}else{veri[i]["durum"]="Ödenmedi";}
                var tarih2 = new Date(veri[i]["tarih"]);
                var ay = aylar[tarih2.getMonth()];
                var gun = tarih2.getDate();
                var yil = tarih2.getFullYear();
                veri[i]["tarih"]=gun+" "+ay+" "+yil;
            }
            var state = that.state;
            state.taksit = veri;
            that.setState(state);
        }).catch(function(err){
            message.error("Taksit Bilgileri Çekilemedi.");
        });
    }

    yetkiKontrol(useryetkilist){
        var state = this.state;
        for(var i in state.yetkiler){
            if(useryetkilist.indexOf(state.yetkiler[i].id)>-1){
                state.yetkiler[i].durum=true;
            }
        }
        this.setState(state);
        this.bilgileriCek(true);
    }

    yetkileriGetir(){
        var that = this;
        var ekURL = "";
        var data = {};
        if(this.state.user.type==="personel"){
          ekURL = "/yetkilendirme/personelTumYetkiler"
          data["personelid"]=this.state.user.id;
        }else{
          ekURL = "/yetkilendirme/userTumYetkiler"
          data["userid"]=this.state.user.id;
        }
        axios.post(ajaxURL+ekURL,data).then(function(gelen){
          var state = that.state;
          state.user.yetkiler = gelen.data.result;
          that.setState(state);
          that.yetkiKontrol(gelen.data.result);
          
        }).catch(function(hata){
          message.error(hata.message);
        });
    }

    bilgileriCek(xyz){
        var that = this;
        var refreshveri = {personel:"",paket:"",tc:"",ad:"",ortaad:"",soyad:"",cinsiyet:"",telefon:"",ehliyet:"",mail:"",baslangic:"",bitis:"",ingilizce:"",reklam:"",kayit:"",ikametgah:"",postakodu:"",pasaport:"",dogum:"",sehir:"",ulke:"",uyruk:"",ikametulke:"",anneadi:"",babaadi:"",acilkisi:"",aciltelefon:"",acilyakinlik:"",skype:"",resim:""};
        var sts = this.state;
        sts.form=refreshveri;
        this.setState(sts);
        axios.post(ajaxURL+"/userbilgi/findbyid",{userid:this.state.edituser.id}).then(function(veriler){
            var veri = veriler.data.result[0];
            var state = that.state;
            for(var j in state.form){
                if(veri[j]==null || veri[j]==""){
                    veri[j]="";
                }
                if(veri[j]===""){
                    if(xyz===true){
                        state.formDurum[j]=false;
                    }else{
                        state.formDurum[j]=false;
                    }
                }else{
                    if(xyz===true){
                        state.formDurum[j]=true;
                    }else{
                        state.formDurum[j]=false;
                    }
                    
                    state.form[j]=veri[j];
                }
            }
            for(var i in state.form){
                if(state.form[i] !== ""){
                     if(["kayit","baslangic","bitis","dogum"].indexOf(i)>-1 && state.form[i]!==""){
                        state.form[i] = state.form[i]=="" ? moment():moment(state.form[i]).format("YYYY-MM-DD");
                     }
                }
            }
            that.setState(state);
            that.taksitleriCek();
        }).catch(function(err){
            message.error(err.message);
        });
    }
    
    ulkeListesi(){
        var that = this;
        axios.get(ajaxURL+"/ulke/list").then(function(veriler){
            if(veriler.data.status){
                var state = that.state;
                state.ulkeListesi = veriler.data.result;
                that.setState(state);
            }else{
                message.error(veriler.data.message); 
            }
        }).catch(function(err){
            message.error(err.message);
        });
    }

    sehirListesi(ulkeid){
        var that = this;
        axios.post(ajaxURL+"/il/findbyulkeid",{ulkeid:ulkeid}).then(function(veriler){
            if(veriler.data.status){
                var state = that.state;
                state.sehirListesi = veriler.data.result;
                that.setState(state);
            }else{
                message.error(veriler.data.message); 
            }
            
        }).catch(function(err){
            message.error(err.message);
        });
    }

    uyrukListesi(){
        var that = this;
        axios.get(ajaxURL+"/uyruk/list").then(function(veriler){
            if(veriler.data.status){
                var state = that.state;
                state.uyrukListesi = veriler.data.result;
                that.setState(state);
            }else{
                message.error(veriler.data.message); 
            }
            
        }).catch(function(err){
            message.error(err.message);
        });
    }

    ingilizceListesi(){
        var that = this;
        axios.get(ajaxURL+"/ingilizce/list").then(function(veriler){
            if(veriler.data.status){
                var state = that.state;
                state.ingilizceListesi = veriler.data.result;
                that.setState(state);
            }else{
                message.error(veriler.data.message); 
            }
            
        }).catch(function(err){
            message.error(err.message);
        });
    }

    ehliyetListesi(){
        var that = this;
        axios.get(ajaxURL+"/ehliyet/list").then(function(veriler){
            if(veriler.data.status){
                var state = that.state;
                state.ehliyetListesi = veriler.data.result;
                that.setState(state);
            }else{
                message.error(veriler.data.message); 
            }
            
        }).catch(function(err){
            message.error(err.message);
        });
    }

    paketListesi(){
        var that = this;
        axios.get(ajaxURL+"/paket/list").then(function(veriler){
            if(veriler.data.status){
                var state = that.state;
                state.paketListesi = veriler.data.result;
                that.setState(state);
            }else{
                message.error(veriler.data.message); 
            }
            
        }).catch(function(err){
            message.error(err.message);
        });
    }

    cinsiyetListesi(){
        var that = this;
        axios.get(ajaxURL+"/cinsiyet/list").then(function(veriler){
            if(veriler.data.status){
                var state = that.state;
                state.cinsiyetListesi = veriler.data.result;
                that.setState(state);
            }else{
                message.error(veriler.data.message); 
            }
            
        }).catch(function(err){
            message.error(err.message);
        });
    }

    personelListesi(){
        var that = this;
        axios.get(ajaxURL+"/personel/list").then(function(veriler){
            if(veriler.data.status){
                var state = that.state;
                state.personelListesi = veriler.data.result;
                that.setState(state);
            }else{
                message.error(veriler.data.message); 
            }
            
        }).catch(function(err){
            message.error(err.message);
        });
    }

    reklamListesi(){
        var that = this;
        axios.get(ajaxURL+"/takip/list").then(function(veriler){
            if(veriler.data.status){
                var state = that.state;
                state.reklamListesi = veriler.data.result;
                that.setState(state);
            }else{
                message.error(veriler.data.message); 
            }
            
        }).catch(function(err){
            message.error(err.message);
        });
    }

    biometrikFotoUrl(){
        var that = this;
        var userid = this.state.edituser.id;
        axios.post(ajaxURL+"/resimler/findByUserid",{userid:userid}).then(function(veriler){
            if(veriler.data.status){
                var state = that.state;
                state.fileList[0].url=veriler.data.result[0].biometrik;
                that.setState(state);
            }else{
                message.error(veriler.data.message); 
            }
            
        }).catch(function(err){
            message.error(err.message);
        });
    }

    componentWillMount(){
        this.yetkileriGetir(); 
    }
    componentDidMount(){
        this.personelListesi();
        this.ulkeListesi();
        this.sehirListesi(213);
        this.uyrukListesi();
        this.ingilizceListesi();
        this.ehliyetListesi();
        this.paketListesi();
        this.cinsiyetListesi();
        this.reklamListesi();
        this.biometrikFotoUrl();
    }

    formGuncelle(degisken,tip,veri){
        var deger = "";
        if(tip==="input"){
            deger = veri.target.value;
        }
        if(tip==="select" || tip==="number"){
            deger = veri;
        }
        if(deger!==""){
            var state = this.state;
            state.form[degisken]=deger;
            this.setState(state);
        }else{
            var state = this.state;
            state.form[degisken]="";
            this.setState(state);
        }
        if(degisken==="ulke"){
            this.sehirListesi(parseInt(deger));
        }
    }

    degisiklikleriKaydet(){
        var data = this.state.form;
        data["userid"]=this.state.edituser.id;
        var that = this;
        for(var i in data){
            if(data[i]===""){
                delete data[i];
            }
        }
        axios.post(ajaxURL+"/userbilgi/update",data).then(function(veriler){
            if(veriler.data.status){
                var state = that.state;
                state.yetkiler.guncelleme.durum=false;
                that.setState(state);
                message.success(veriler.data.message);
            }else{
                message.error(veriler.data.message); 
            }
            
        }).catch(function(err){
            message.error(err.message);
        });
    }

    baslangicBitisDegistir(date,dateString){
        var baslangic = dateString[0];
        var bitis = dateString[1];
        var state = this.state;
        state.form.baslangic=baslangic;
        state.form.bitis=bitis;
        this.setState(state);
    }

    dogumtarihDegistir(date,dateString){
        var tarih = dateString;
        var state = this.state;
        state.form.dogum=tarih;
        state.formDurum.dogum=true;
        this.setState(state);
    }

    biometrikFotoYukle = (file) => {
        var that = this;
        var reader  = new FileReader();
        reader.addEventListener("load", function () {
            var userid = that.state.edituser.id;
            var data = {};
            data={userid:userid,resim:reader.result};
            axios.post(ajaxURL+'/resimler/biometrikFotoYukle',data).then(function(response){
                if(response.data.status){
                    message.success(response.data.message);
                    var state = that.state;
                    state.fileList[0].url=response.data.result;
                    that.setState(state);
                }else{
                    message.error("Biometrik Fotoğrafınız Yüklenemedi");
                }
            }).catch(function(hata){
                message.error(hata.message);
            });
          }, false);
        reader.readAsDataURL(file);
    }

    render(){

        const { previewVisible, previewImage, fileList } = this.state;
        const uploadButton = (
        <div>
            <Icon type="plus" />
            <div className="ant-upload-text">Upload</div>
        </div>
        );

        const formItemLayout = {
            labelCol: {
              xs: { span: 24 },
              sm: { span: 24 },
              md: { span: 6 },
            },
            wrapperCol: {
              xs: { span: 24 },
              sm: { span: 24 },
              md: { span: 14 },
            },
            style:{
                marginBottom:"15px"
            }
        }
        const formItemLayout2 = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 24 },
                md: { span: 6 },
              },
              wrapperCol: {
                xs: { span: 24 },
                sm: { span: 24 },
                md: { span: 14 },
              }
        }

        const tarihStyle = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 4 },
                md: { span: 4 },
              },
              wrapperCol: {
                xs: { span: 24 },
                sm: { span: 4 },
                md: { span: 4 },
              }
        }

        const taksitcolumns = [
            {
                title: 'Sira',
                dataIndex: 'sira',
                key: 'sira',
            },
            {
                title: 'Miktar',
                dataIndex: 'miktar',
                key: 'miktar',
            },
            {
                title: 'Tarih',
                dataIndex: 'tarih',
                key: 'tarih',
            },
            {
                title: 'Durum',
                dataIndex: 'durum',
                key: 'durum',
            }
            ];
        
        const formDisabled = this.state.yetkiler.guncelleme.durum;

        const dogdugunuzSehir = this.state.sehirListesi.map((item,sira)=>{
            return (<Option value={item.id}>{item.isim}</Option>);
        });

        const dogdugunuzUlke = this.state.ulkeListesi.map((item,sira)=>{
            return (<Option value={item.id}>{item.isim}</Option>);
        });

        const ingilzicelistesi = this.state.ingilizceListesi.map((item,sira)=>{
            return (<Option value={item.id}>{item.isim}</Option>);
        });

        const ehliyetlistesi = this.state.ehliyetListesi.map((item,sira)=>{
            return (<Option value={item.id}>{item.isim}</Option>);
        });

        const uyrukList = this.state.ulkeListesi.map((item,sira)=>{
            return (<Option value={item.id}>{item.isim}</Option>);
        });

        const paketlist = this.state.paketListesi.map((item,sira)=>{
            return (<Option value={item.id}>{item.isim}</Option>);
        });

        const cinsiyetlistesi = this.state.cinsiyetListesi.map((item,sira)=>{
            return (<Option value={item.id}>{item.isim}</Option>);
        });
        
        const personellistesi = this.state.personelListesi.map((item,sira)=>{
            return (<Option value={item.id}>{item.isim}</Option>);
        });

        const reklamlistesi = this.state.reklamListesi.map((item,sira)=>{
            return (<Option value={item.id}>{item.isim}</Option>);
        });

        const dateFormat = 'YYYY-MM-DD';

        const bilgiDegistirme = (
                <FormItem 
                style={{marginBottom:"15px", marginTop:"30px",display:this.state.yetkiler.admin.durum==true ? "block":"none"}} 
                {...formItemLayout2}
                label="Düzenlenecek Kullanıcı : ">
                        <AutoComplete
                            defaultValue={this.state.edituser.isim}
                            style={{width:"100%",marginBottom:"16px"}}
                            size="large"
                            dataSource={this.state.userDataSource.map(renderOption)}
                            onSelect={this.userSecme}
                            onSearch={this.userArama}
                            placeholder="Öğrenci Ad Soyad, Mail, Telefon"
                            optionLabelProp="text"
                            ><Input suffix={<Icon type="search" className="certain-category-icon" />} />
                        </AutoComplete>
                </FormItem>
        )
        
        const userinputDisabled = this.state.yetkiler.admin.durum==true ? false:this.state.yetkiler.guncelleme.durum==true ? false:true;
        const admininputDisabled = this.state.yetkiler.admin.durum==true ? false:true;

        if(this.state.yetkiler.sayfa.durum===true){
            return(
                <LayoutWrapper>
                        <Breadcrumb style={{backgroundColor:"#fff",padding:"10px",width:"100%",marginBottom:"5px"}}>
                            <Breadcrumb.Item>Work and Travel</Breadcrumb.Item>
                            <Breadcrumb.Item>Kişisel Bilgilerin Girişi</Breadcrumb.Item>
                            <Breadcrumb.Item>{this.state.edituser.isim!=="" ? this.state.edituser.isim:"Seçim Yapılmamış"}</Breadcrumb.Item>
                        </Breadcrumb>
                        <Card title="1. Adım - Kişisel Bilgilerin Girilmesi" style={{width:"100%"}} bodyStyle={{padding:"0"}}>
                        <Row style={{width:"100%"}}>
                            <Col lg={24} md={24} sm={24} xs={24}>
                            <Card bodyStyle={{padding:"10px"}} >
    
                            <Form>
                                {bilgiDegistirme}
                                <FormItem style={{marginBottom:"15px", marginTop:"30px"}} labelCol={formItemLayout.labelCol} wrapperCol={formItemLayout.wrapperCol} label="Yetkili Personel : ">
                                <Select disabled={admininputDisabled}  value={this.state.form.personel}  onChange={this.formGuncelle.bind(this,'personel','select')} style={{ width: "100%" }}>
                                    {personellistesi}
                                </Select>
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="Paket Seçimi : ">
                                <Select disabled={admininputDisabled} value={this.state.form.paket}  onChange={this.formGuncelle.bind(this,'paket','select')} style={{ width: "100%" }}>
                                    {paketlist}
                                </Select>
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="TC Kimlik No : ">
                                <Input disabled={admininputDisabled} value={this.state.form.tc} onChange={this.formGuncelle.bind(this,'tc','input')} />
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="Adınız : ">
                                <Input disabled={admininputDisabled} value={this.state.form.ad} onChange={this.formGuncelle.bind(this,'ad','input')} />
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="Orta Adınız : ">
                                <Input disabled={admininputDisabled} value={this.state.form.ortaad} onChange={this.formGuncelle.bind(this,'ortaad','input')} />
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="Soyadınız : " >
                                <Input disabled={admininputDisabled} value={this.state.form.soyad} onChange={this.formGuncelle.bind(this,'soyad','input')} />
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="Cinsiyetiniz : ">
                                <Select disabled={userinputDisabled} value={this.state.form.cinsiyet} onChange={this.formGuncelle.bind(this,'cinsiyet','select')} style={{ width: "100%" }}>
                                    {cinsiyetlistesi}
                                </Select>
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="Telefon Numaranız : ">
                                <Input disabled={userinputDisabled} value={this.state.form.telefon} onChange={this.formGuncelle.bind(this,'telefon','input')} />
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="Ehliyetiniz : ">
                                <Select disabled={userinputDisabled} value={this.state.form.ehliyet} onChange={this.formGuncelle.bind(this,'ehliyet','select')} style={{ width: "100%" }}>
                                    {ehliyetlistesi}
                                </Select>
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="Mail Adresiniz :">
                                <Input disabled={admininputDisabled} value={this.state.form.mail} onChange={this.formGuncelle.bind(this,'mail','input')} />
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="Work & Trv. Başlama & Bitiş Tarihi :">
                                    <RangePicker 
                                    disabled={userinputDisabled}
                                    value={[this.state.formDurum.baslangic==true ? moment(this.state.form.baslangic, dateFormat):moment(),this.state.formDurum.bitis==true ? moment(this.state.form.bitis, dateFormat):moment()]} 
                                    format="YYYY-MM-DD" 
                                    onChange={this.baslangicBitisDegistir.bind(this)} />
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="İngilizce Seviyeniz : ">
                                <Select disabled={admininputDisabled} value={this.state.form.ingilizce}  onChange={this.formGuncelle.bind(this,'ingilizce','select')} style={{ width: "100%" }}>
                                    {ingilzicelistesi}
                                </Select>
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="Bizi Nereden Buldunuz :">
                                <Select disabled={admininputDisabled} value={this.state.form.reklam} onChange={this.formGuncelle.bind(this,'reklam','select')} style={{ width: "100%" }}>
                                    {reklamlistesi}
                                </Select>
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="Kayıt Tarihi :">
                                    <DatePicker disabled={true} style={{width:"100%"}} value={this.state.formDurum.kayit==true ? moment(this.state.form.kayit, dateFormat):moment()} />
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="İkametgah Adresiniz :">
                                <TextArea disabled={admininputDisabled} rows={4} value={this.state.form.ikametgah} placeholder="Bosna Hersek Mahallesi Şu Caddesi Bu Caddesi" />
                                </FormItem>
    
                                <FormItem {...formItemLayout}  disabled={formDisabled}   label="Posta Kodunuz :">
                                <InputNumber disabled={userinputDisabled} value={this.state.form.postakodu} onChange={this.formGuncelle.bind(this,'postakodu','number')} />
                                </FormItem>
    
                                <FormItem {...formItemLayout}  disabled={formDisabled}  label="Pasaport Numarası :">
                                <Input disabled={userinputDisabled} value={this.state.form.pasaport} onChange={this.formGuncelle.bind(this,'pasaport','input')} />
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="Doğum Tarihi :">
                                    <DatePicker disabled={userinputDisabled} onChange={this.dogumtarihDegistir.bind(this)} value={this.state.formDurum.dogum==true ? moment(this.state.form.dogum, dateFormat):moment()} format={dateFormat} style={{width:"100%"}}/>
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="Doğduğunuz Ülke : ">
                                <Select disabled={userinputDisabled} value={this.state.form.ulke} onChange={this.formGuncelle.bind(this,'ulke','select')} style={{ width: "100%" }}>
                                    {dogdugunuzUlke}
                                </Select>
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="Pasaportta Yazan Doğum Yeriniz : ">
                                <Select disabled={userinputDisabled} value={this.state.form.sehir} onChange={this.formGuncelle.bind(this,'sehir','select')} style={{ width: "100%" }}>
                                    {dogdugunuzSehir}
                                </Select>
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="Uyruğunuz : ">
                                <Select disabled={userinputDisabled} value={this.state.form.uyruk} style={{ width: "100%" }} onChange={this.formGuncelle.bind(this,'uyruk','select')}>
                                    {uyrukList}
                                </Select>
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="İkameth Ettiğiniz Ülke : ">
                                <Select disabled={userinputDisabled} value={this.state.form.ikametulke} onChange={this.formGuncelle.bind(this,'ikametulke','select')} style={{ width: "100%" }}>
                                    {dogdugunuzUlke}
                                </Select>
                                </FormItem>
    
                                <FormItem {...formItemLayout}  label="Annenizin Adı ve Soyadı : ">
                                <Input disabled={userinputDisabled} value={this.state.form.anneadi} onChange={this.formGuncelle.bind(this,'anneadi','input')} />
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="Babanızın Adı ve Soyadı : ">
                                <Input disabled={userinputDisabled} value={this.state.form.babaadi} onChange={this.formGuncelle.bind(this,'babaadi','input')} />
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="Acil Durumlarda Aranacak Kişi : ">
                                <Input disabled={userinputDisabled} value={this.state.form.acilkisi} onChange={this.formGuncelle.bind(this,'acilkisi','input')} />
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="Acil Dur. Ar. Kişinin Telefonu : ">
                                <Input disabled={userinputDisabled} value={this.state.form.aciltelefon} onChange={this.formGuncelle.bind(this,'aciltelefon','input')} />
                                </FormItem>

                                <FormItem {...formItemLayout} label="Acil Dur. Ar. Kişinin Yakınlık Derecesi : ">
                                <Input disabled={userinputDisabled} value={this.state.form.acilyakinlik} onChange={this.formGuncelle.bind(this,'acilyakinlik','input')} />
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="SKYPE ID :">
                                <Input disabled={userinputDisabled} value={this.state.form.skype} onChange={this.formGuncelle.bind(this,'skype','input')} />
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="Ödeme Takvimi :">
                                <Table dataSource={this.state.taksit} columns={taksitcolumns} />
                                </FormItem>
    
                                <FormItem {...formItemLayout} label="Biometrik Fotoğraf :">
                                    <Upload
                                    style={{width:"50mm",height:"60mm"}}
                                    listType="picture-card"
                                    accept=".jpg,.jpeg,.png"
                                    data={this.biometrikFotoYukle}                                    
                                    fileList={fileList}
                                    onPreview={this.handlePreview}
                                    onChange={this.handleChange}
                                    disabled={this.state.formDurum.resim}
                                    >
                                    {fileList.length >= 1 ? null : uploadButton}
                                    </Upload>
                                    
                                    <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
                                    <img alt="example" style={{ width: '100%' }} src={previewImage} />
                                    </Modal>
                                </FormItem>

                                <FormItem style={{textAlign:"right",display:this.state.yetkiler.guncelleme.durum==false ? "block":"none"}} labelCol={formItemLayout.labelCol} wrapperCol={formItemLayout.wrapperCol} label="Sisteme Kaydet">
                                    İşlem Yapma Yetkiniz Bulunmamaktadır. İrtibat : <a target="_blank" href="mailto:yardim@westline.com.tr">yardim@westline.com.tr</a>
                                </FormItem>

                                <FormItem style={{textAlign:"right",display:this.state.yetkiler.admin.durum==true ? "block":this.state.yetkiler.guncelleme.durum==true ? "block":"none"}} labelCol={formItemLayout.labelCol} wrapperCol={formItemLayout.wrapperCol} label="Sisteme Kaydet">
                                <Popconfirm title="Kullanıcı Bilgilerini Kaydetmek İstediğinizden Emin misiniz?" onConfirm={this.degisiklikleriKaydet.bind(this)} okText="Evet" cancelText="Hayır">
                                        <Button style={{borderRadius:"5px"}} type="primary" >Değişiklikleri Kaydet</Button>
                                    </Popconfirm>
                                </FormItem>
                            </Form>
                            </Card>
                            </Col>
                        </Row>
                        </Card>
                </LayoutWrapper>
            );
        }else{
            return (
            <LayoutWrapper>

            </LayoutWrapper>
            );
        }
        
    }
}

export default Kisiselbilgiler;