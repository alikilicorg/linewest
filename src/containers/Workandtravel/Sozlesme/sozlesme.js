// FN : 1526235846119
import React, { Component } from "react";
import axios from 'axios';
import { connect } from "react-redux";
import {Upload, Icon, message, Table, Button, Spin, Col, Row, Input, Card, Form, AutoComplete, Select, Breadcrumb } from 'antd';
import config2 from '../../../settings/config';
import LayoutWrapper from '../../../components/utility/layoutWrapper.js';
import pdfDoc from 'jspdf'
import moment from 'moment';
import download from 'downloadjs';
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;
const FormItem = Form.Item;
const Dragger = Upload.Dragger;
const ajaxURL = config2.http+config2.server+':'+config2.port;
const ButtonGroup = Button.Group;

function renderOption(item) {
    return (
      <Option key={item.id} text={item.isim}>{item.isim}</Option>
    );
}

function download_file(fileURL, fileName) {
    // for non-IE
    if (!window.ActiveXObject) {
        var save = document.createElement('a');
        save.href = fileURL;
        save.target = '_blank';
        var filename = fileURL.substring(fileURL.lastIndexOf('/')+1);
        save.download = fileName || filename;
	       if ( navigator.userAgent.toLowerCase().match(/(ipad|iphone|safari)/) && navigator.userAgent.search("Chrome") < 0) {
				document.location = save.href; 
			}else{
		        var evt = new MouseEvent('click', {
		            'view': window,
		            'bubbles': true,
		            'cancelable': false
		        });
		        save.dispatchEvent(evt);
		        (window.URL || window.webkitURL).revokeObjectURL(save.href);
			}	
    }

    // for IE < 11
    else if ( !! window.ActiveXObject && document.execCommand)     {
        var _window = window.open(fileURL, '_blank');
        _window.document.close();
        _window.document.execCommand('SaveAs', true, fileName || fileURL)
        _window.close();
    }
}

class Sozlesme extends Component {

    constructor(){
        super();
        this.state={
            user:localStorage.getItem('user')!==null ? JSON.parse(localStorage.getItem('user')):{id:-1,durum:false,type:"user"},
            /* edituser:localStorage.getItem('editUser')!==null ? JSON.parse(localStorage.getItem('editUser')):{id:1,durum:true}, */
            edituser:window.editUser,
            yetkiler:{
                sayfa:{id:11,message:"Sözleşme Sayfasını Açma Yetkiniz Yoktur",durum:false},
                yukleme:{id:19,message:"Sözleşme Yükleme Yetkiniz Yoktur",durum:false},
                indirme:{id:29,message:"Sözleşme İndirme Yetkiniz Yoktur",durum:false},
                indirmeadmin:{id:30,message:"Sözleşme İndirme Yetkiniz Yoktur",durum:false},
                admin:{id:14,message:"Sözleşme Yükleme Yetkiniz Yoktur",durum:false}
            },
            userDataSource:[],
            form:{
                adsoyad:"",
                tc:"",
                ikametgah:"",
                taksitler:[],
                telefon:"",
                mail:"",
                tarih:"",
                paket:""
            },
            
            indirmeLinki:"",
            loading:false
        }
    }


    userArama = (value) => {
        var that = this;
        var personelid = this.state.user.id;
        if(this.state.user.type=="personel"){
            if(value!==""){
                axios.post(ajaxURL+'/user/userbilgisearch',{search:value,personelid:personelid}).then(function(response){
                    if(response.data.status==true){
                        debugger;
                      var state = that.state;
                      if(that.state.user.yetkiler.indexOf(26)>-1){
                        state.userDataSource = response.data.result;
                      }else{
                        state.userDataSource=[];
                      for(var i in response.data.result){
                          if(response.data.result[i].personel===personelid){
                            state.userDataSource.push(response.data.result[i]);
                          }
                        }
                      }
                      
                      that.setState(state);
                    }else{
                        message.warning(response.data.message);
                    }
                  });
            }else{
                message.warning("Kulanıcı Arama Kutusu Boş Bırakılamaz");
            }
        }else{
            message.warning("Sadece Personeller Arama Yapabilir");
        }
    }
    
    userSecme = (value) => {
        debugger;
        var userid = parseInt(value);
        var state = this.state;
        state.edituser.id=value;
        this.setState(state);
        this.bilgileriCek();
    }


    sozlesmeYukle = (e) =>{
        var file = e.target.files[0];
        if(file.type==="application/pdf"){
            message.success(file.name+' Adlı PDF Dosyası Yüklendi');
        }else{

        }
    };

    taksitleriCek(){
        var that = this;
        axios.post(ajaxURL+"/taksit/findByUserid",{userid:this.state.edituser.id}).then(function(veriler){
            var veri = veriler.data.result;
            var state = that.state;
            state.form.taksitler = veri;
            that.setState(state);
        }).catch(function(err){
            message.error(err.message);
        });
    }

    bilgileriCek(){
        var userid = this.state.edituser.id;
        var that = this;
        if(this.state.yetkiler.sayfa.durum){
            axios.post(ajaxURL+"/userbilgi/findbyid2",{userid:this.state.edituser.id}).then(function(veriler){
                debugger;
                var veri = veriler.data.result[0];
                var state = that.state;
                var adsoyad = "";
                if(veri.ortaad!==""){
                    adsoyad = veri.ad+" "+veri.ortaad+" "+veri.soyad;
                }else{
                    adsoyad = veri.ad+" "+veri.soyad;
                }
                state.form.adsoyad = adsoyad;
                state.form.tc = veri.tc;
                state.form.ikametgah = veri.ikametgah;
                state.form.telefon = veri.telefon;
                state.form.mail = veri.mail;
                state.form.kayit = moment(veri.kayit).format('MM.DD.YYYY');
                state.form.paket = veri.paket.isim;
                that.setState(state);
                that.taksitleriCek();
            }).catch(function(err){
                message.error(err.message);
            });
        }else{
            message.warning("Sayfayı Açma Yetkiniz Yoktur");
        }
        
    }



    yetkiKontrol(useryetkilist){
        var state = this.state;
        for(var i in state.yetkiler){
            if(useryetkilist.indexOf(state.yetkiler[i].id)>-1){
                state.yetkiler[i].durum=true;
            }
        }
        this.setState(state);
        this.bilgileriCek();
    }

    yetkileriGetir(){
        debugger;
        var that = this;
        var ekURL = "";
        var data = {};
        if(this.state.user.type==="personel"){
          ekURL = "/yetkilendirme/personelTumYetkiler"
          data["personelid"]=this.state.user.id;
        }else{
          ekURL = "/yetkilendirme/userTumYetkiler"
          data["userid"]=this.state.user.id;
        }
        axios.post(ajaxURL+ekURL,data).then(function(gelen){
          var state = that.state;
          state.user.yetkiler = gelen.data.result;
          that.yetkiKontrol(gelen.data.result);
          that.setState(state);
        }).catch(function(hata){
          message.error(hata.message);
        });
      }

    componentWillMount(){
        this.yetkileriGetir();
    }

    sozlesmeMetniIndir(){
        var that = this;
        var state = this.state;
        state.loading=true;
        that.setState(state);
        var sira = ["","Birinci","İkinci","Üçüncü","Dördüncü","Beşinci","Altıncı","Yedinci","Sekizinci","Dokuzuncu","Onuncu","Onbirinci","Onikinci","Onüçüncü","Ondördüncü","Onbeşinci"];
        var toplam = 0;
        var takdizi = [];
        var form = this.state.form;
        for(var i=0; i<form.taksitler.length;i++){
            var taksit = form.taksitler[i];
            var tarihi = taksit.tarih;
            
            var i2 = i+1;
            if(i>=1){
                toplam+=taksit.miktar;
                takdizi.push({taksit:sira[i]+" Taksit : "+moment(tarihi).format('MM.DD.YYYY')+" : "+taksit.miktar+" USD"});
            }

        }

        var data = {
            userid:this.state.edituser.id,
            tarih: form.tarih,
            tcno: form.tc,
            usermail:form.mail,
            telefon: form.telefon,
            adsoyad: form.adsoyad,
            paket:form.paket,
            onkayit:form.taksitler[0].miktar+"",
            gerikalan:toplam+"",
            taksitler:takdizi
        };
        message.info("Sözleşme Hazırlanıyor. Bu İşlem Biraz Zaman Alabilir");
       axios.post(ajaxURL+"/dosyalar/sozlesme",data).then(function(gelen){
           debugger;
            if(gelen.data.status){
                message.success("Sözleşme Metni Hazırlanmıştır Artık İndirebilirsiniz");
                state.indirmeLinki = gelen.data.result
                state.loading=false;
                that.setState(state);
            }
          }).catch(function(hata){
            message.error(hata.message);
          });

        


    }


    sozlesmeDownload(indirmelinki,ths){
        debugger;
        message.success("Dosyanız İndiriliyor...");
        download_file(config2.serverFileRoot+this.state.indirmeLinki,"imzaliSozlesme.pdf");
    }

    render(){
        var that = this;
        const formItemLayout = {labelCol: {xs: { span: 24 },sm: { span: 5 },},wrapperCol: {xs: { span: 24 },sm: { span: 15 },},style:{marginBottom:"15px"}}
        const rowStyle = {display:this.state.yetkiler.sayfa.durum==true ? "flex":"none",width: '100%',flexFlow: 'row wrap'};
        const rowStyleAdmin = {display:this.state.yetkiler.admin.durum==true ? "block":this.state.yetkiler.yukleme.durum==true ? "block":"none",width: '100%'};
        const colStyle = {
            marginBottom: '16px'
        };
        const gutter = 16;

        const taksitduzenleme = this.state.form.taksitler.map((item,sira)=>{
            debugger;
            var aylar = ["Ocak","Şubat","Mart","Nisan","Mayıs","Haziran","Temmuz","Ağustos","Eylül","Ekim","Kasım","Aralık"];
            var i = sira+1;
            var durum = <span style={{color:"#E91E63"}}>Ödenmedi</span>
            if(item.durum){
                durum=<span style={{color:"#4caf50"}}>Ödendi</span>;
            }
            var tarih2 = new Date(item.tarih);
            var ay = aylar[tarih2.getMonth()];
            var gun = tarih2.getDate();
            var yil = tarih2.getFullYear();
            return {sira:i,miktar:item.miktar+"TL",tarih:gun+" "+ay+" "+yil,durum:durum};
        });

        const bilgitablecolumn = [
            {
                title: 'Özellik',
                dataIndex: 'ozellik',
                key: 'ozellik',
            },
            {
                title: 'Değer',
                dataIndex: 'deger',
                key: 'deger',
            }
        ];

        const bilgitablosu = [
            {ozellik:"Ad Soyad : ",deger:this.state.form.adsoyad},
            {ozellik:"Tc No : ",deger:this.state.form.tc},
            {ozellik:"Telefon : ",deger:this.state.form.telefon},
            {ozellik:"Mail : ",deger:this.state.form.mail},
            {ozellik:"İkamet Ettiği Adres : ",deger:this.state.form.ikametgah},
            {ozellik:"Kayıt Tarihi : ",deger:this.state.form.kayit},
            {ozellik:"Paket Seçimi : ",deger:this.state.form.paket}
        ];

        const taksitcolumns = [
            {
                title: 'Sira',
                dataIndex: 'sira',
                key: 'sira',
            },
            {
                title: 'Miktar',
                dataIndex: 'miktar',
                key: 'miktar',
            },
            {
                title: 'Tarih',
                dataIndex: 'tarih',
                key: 'tarih',
            },
            {
                title: 'Durum',
                dataIndex: 'durum',
                key: 'durum',
            }
            ];
        
        const uploadProps = {
            name: 'file',
            accept:".pdf",
            multiple: false,
            action: ajaxURL+'/upload/sozlesme',
            onChange(info) {

                debugger;
                var file = info.fileList[0].originFileObj;
                let data = new FormData()
                data.append('file', file)    
                var setting = {
                    method: 'post',
                    url: ajaxURL+"/upload/sozlesme",
                    data:data,
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    },   
                }
                axios(setting).then(function(gelen){
                    if(gelen.data.status){
                        message.success("Sözleşme Başarılı Bir Şekilde Yüklendi");
                    }else{
                        message.error("Malesef Sözleşme Yüklenemedi");
                    }
                }).catch(function(saka){
                    message.error("Malesef Sözleşme Yüklenemedi");
                });
            },
          };

        if(this.state.yetkiler.admin.durum===true || this.state.yetkiler.sayfa.durum===true){

            const bilgiDegistirme = (
                <Row style={rowStyleAdmin} justify="start">
                    <Col md={24} sm={24} xs={24}>
                        <AutoComplete
                            defaultValue={this.state.edituser.isim}
                            style={{width:"100%",marginBottom:"16px"}}
                            size="large"
                            dataSource={this.state.userDataSource.map(renderOption)}
                            onSelect={this.userSecme}
                            onSearch={this.userArama}
                            placeholder="Öğrenci Ad Soyad, Mail, Telefon"
                            optionLabelProp="text"
                            ><Input suffix={<Icon type="search" className="certain-category-icon" />} />
                        </AutoComplete>
                    </Col>
                </Row>
            )

            return(
                <LayoutWrapper>
                        <Breadcrumb style={{backgroundColor:"#fff",padding:"10px",width:"100%",marginBottom:"5px"}}>
                            <Breadcrumb.Item>Work and Travel</Breadcrumb.Item>
                            <Breadcrumb.Item>Sözleşme Yükleme ve İndirme</Breadcrumb.Item>
                            <Breadcrumb.Item>{this.state.edituser.isim!=="" ? this.state.edituser.isim:"Seçim Yapılmamış"}</Breadcrumb.Item>
                        </Breadcrumb>
                <Row style={rowStyle} justify="start">
                    <Col md={24} sm={24} xs={24} style={colStyle}>
                        <Spin spinning={this.state.loading}>
                            
                            <Card
                                title="Sözleşme Metninde Kullanılacak Bilgiler"
                                bodyStyle={{ padding: "10px",marginBottom:"10px" }}
                                extra={
                                    
                                    <ButtonGroup>
                                        <Button disabled={this.state.yetkiler.indirmeadmin.durum===true ? false:this.state.yetkiler.indirme.durum==true ? false:true}  onClick={this.sozlesmeMetniIndir.bind(this)} size="middle" type="primary">Sözleşme Metnini Hazırla</Button>
                                        <Button disabled={this.state.indirmeLinki=="" ? true:false} onClick={this.sozlesmeDownload.bind(this,this.state.indirmeLinki)} size="middle" type="primary">Sözleşme Metnini İnidr</Button>
                                    </ButtonGroup>
                                
                                }>
                                <Row gutter="16">
                                    {bilgiDegistirme}
                                    <Col md={12} sm={24} xs={24}>
                                        <Table pagination={{position:'none'}} size="small" title={() => <h3 style={{color:"#009688"}}>Kişisel Bilgiler</h3>} dataSource={bilgitablosu} columns={bilgitablecolumn} />
                                    </Col>
                                    <Col md={12} sm={24} xs={24}>
                                        <Table pagination={{position:'none'}}  size="small" title={() => <h3 style={{color:"#009688"}}>Taksitlendirme Aralığı</h3>} dataSource={taksitduzenleme} columns={taksitcolumns} />
                                    </Col>
                                </Row>
                            </Card>
                        </Spin>
                    </Col>
                </Row>
    
                <Row style={{width: '100%',display: this.state.yetkiler.admin.durum===true ? 'flex':this.state.yetkiler.yukleme.durum===true ? "flex":"none"}} gutter={gutter} justify="start">
                    <Col md={24} sm={24} xs={24} style={colStyle}>
                        <Card
                            bodyStyle={{ padding: "0",marginBottom:"0" }}>
                            <Row style={{width:"100%"}}>
                                <Col lg={24} md={24} sm={24} xs={24}>
                                    <Dragger  {...uploadProps}>
                                        <p className="ant-upload-drag-icon">
                                        <Icon type="inbox" />
                                        </p>
                                        <p className="ant-upload-text">Dosyanızı Bu Alanı Tıklayarak ya da Sürükleyip Bırakarak Yükleyiniz</p>
                                        <p className="ant-upload-hint">Sadece PDF formatındaki dosyalar kabul edilmektedir</p>
                                        <p className="ant-upload-hint">Yükleme İşlemini Yapılabilmesi İçin Lütfen Kullanıcı Seçimi Yapınız</p>
                                    </Dragger>
                                </Col>
                            </Row>
                        </Card>
                    </Col>
                </Row>
                
            </LayoutWrapper>);
        }else{
            return (
                <LayoutWrapper>
                    
                </LayoutWrapper>
                );
        }

        
    }
}

export default connect(state => ({
    ...state.App.toJS()
}))(Sozlesme);