import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import axios from 'axios';
import Input,{Textarea} from '../../components/uielements/input';
import Select, { SelectOption} from '../../components/uielements/select';
import Checkbox from '../../components/uielements/checkbox';
import Button from '../../components/uielements/button';
import authAction from '../../redux/auth/actions';
import Auth0 from '../../helpers/auth0/index';
import Firebase from '../../helpers/firebase';
import FirebaseLogin from '../../components/firebase';
import IntlMessages from '../../components/utility/intlMessages';
import SignUpStyleWrapper from './signup.style';
import config2 from '../../settings/config';
const ajaxURL = config2.http+config2.server+':'+config2.port;
/*eslint-disable*/
import '../../containers/css/toastrc.css'
import '../../containers/css/animated.css'
import Myfx from '../../components/fonksiyonlar/fonksiyonlar'

import { ToastContainer } from "react-toastr";
import mesaj from '../../components/toast/toast'
let container;
import AppLocale from '../../languageProvider';
import config, {
    getCurrentLanguage
} from '../../containers/LanguageSwitcher/config';
const langMessage = AppLocale[getCurrentLanguage(config.defaultLanguage || 'english').locale].messages;
/*eslint-enable*/

const { login } = authAction;

class SignUp extends Component {
  state = {
    redirectToReferrer: false,
    programlar:[],
    egitimler:[],
    reklamlar:[],
    adsoyad:"",
    mail:"",
    telefon:"",
    program:"",
    egitim:"",
    reklam:"",
    notlar:""
  };

  componentDidMount(){
    var that = this;
    axios.get(ajaxURL+'/program/list').then(function(response){
      if(response.data.status===true){
        var state = that.state;
        state.programlar = response.data.result;
        that.setState(state);
      }else{
        new mesaj(container,{durum:"error",baslik:"Bir Problem Var",icerik:"İlgilenilen Program Listesi Çekilemedi"});
      }
    });

    axios.get(ajaxURL+'/egitim/list').then(function(response){
      if(response.data.status===true){
        var state = that.state;
        state.egitimler = response.data.result;
        that.setState(state);
      }else{
        new mesaj(container,{durum:"error",baslik:"Bir Problem Var",icerik:"Eğitim Bilgisi Giriş Listesi Çekilemedi"});
      }
    });

    axios.get(ajaxURL+'/takip/list').then(function(response){
      if(response.data.status===true){
        var state = that.state;
        state.reklamlar = response.data.result;
        that.setState(state);
      }else{
        new mesaj(container,{durum:"error",baslik:"Bir Problem Var",icerik:"Bizi Nereden Buldunuz Seçenek Listesi Çekilemedi"});
      }
    });
    
  };

  componentWillReceiveProps(nextProps) {
    if (
      this.props.isLoggedIn !== nextProps.isLoggedIn &&
      nextProps.isLoggedIn === true
    ) {
      this.setState({ redirectToReferrer: true });
    }
  }

  adayUyeKaydet = () => {
    /*const { login } = this.props;
    login();
    this.props.history.push('/dashboard');*/
    debugger;
    var that = this;
    if(Myfx.stringBosmu(that.state.mail) && Myfx.stringBosmu(that.state.adsoyad) && Myfx.stringBosmu(that.state.telefon)){
        if(Myfx.emailKontrol(that.state.mail)){
            var gonderilecekler = {mail:that.state.mail,isim:that.state.adsoyad,telefon:that.state.telefon,program:that.state.program,egitim:that.state.egitim,reklam:that.state.reklam,notlar:that.state.notlar};
            axios.post(ajaxURL+'/adayuye/insert',gonderilecekler).then(function (res) {
                if(res.data.status===true){
                    new mesaj(container,{durum:"success",baslik:langMessage["alert.mesaj11"],icerik:res.data.message});
                    var state = that.state;
                    state["adsoyad"]="";
                    state["mail"]="";
                    state["telefon"]="";
                    state["program"]="yok";
                    state["egitim"]="yok";
                    state["reklam"]="yok";
                    state["notlar"]="";
                    that.setState(state);
                    setTimeout(function () {
                        that.props.history.push('/');
                    },5000);

                }else{
                    new mesaj(container,{durum:"error",baslik:langMessage["alert.mesaj9"],icerik:res.data.message});
                }
            });
        }else{
            new mesaj(container,{durum:"error",baslik:langMessage["alert.mesaj1"],icerik:that.state.mail+langMessage["alert.mesaj2"]});
        }
    }else{
        new mesaj(container,{durum:"error",baslik:langMessage["alert.mesaj3"],icerik:that.state.mail+langMessage["alert.mesaj4"]});
    }
  };

   selectProgramDegisti = (deger) =>{
       var state = this.state;
       state.program=deger;
       this.setState(state);
   };

    selectEgitimDegisti = (deger) =>{
       var state = this.state;
       state.egitim=deger;
       this.setState(state);
   };

    selectReklamDegisti = (deger) =>{
       var state = this.state;
       state.reklam=deger;
       this.setState(state);
   };

  inputDegisti = (e) =>{
      var element = e.target;
      var id = element.id;
      var deger = element.value;
      var state = this.state;
      switch (id){
          case "adsoyad":
              state.adsoyad=deger;
              break;
          case "mail":
              state.mail=deger;
              break;
          case "telefon":
              state.telefon=deger;
              break;
          case "notlar":
              state.notlar=deger;
              break;
      }
      this.setState(state);
  };

  render() {
    
    const programlistesi = this.state.programlar.map((item) =>
      <SelectOption value={item.id}>{item.isim}</SelectOption>
    );

    const egitimlistesi = this.state.egitimler.map((item) =>
      <SelectOption value={item.id}>{item.isim}</SelectOption>
    );

    const reklamlistesi = this.state.reklamlar.map((item) =>
      <SelectOption value={item.id}>{item.isim}</SelectOption>
    );

    return (
      <SignUpStyleWrapper className="isoSignUpPage">
          <ToastContainer
              ref={ref => container = ref}
              className="toast-top-right"
          />
        <div className="isoSignUpContentWrapper">
          <div className="isoSignUpContent">
            <div className="isoLogoWrapper">
              <Link to="/dashboard">
                <IntlMessages id="page.signUpTitle" />
              </Link>
            </div>

            <div className="isoSignUpForm">
              <div className="isoInputWrapper">
                <Input size="large" id="adsoyad" onChange={this.inputDegisti.bind(this)} placeholder="Ad ve Soyadınız" />
              </div>

              <div className="isoInputWrapper">
                <Input size="large" id="mail" onChange={this.inputDegisti.bind(this)} placeholder="Email Adresiniz" />
              </div>

              <div className="isoInputWrapper">
                <Input size="large" id="telefon" onChange={this.inputDegisti.bind(this)} placeholder="Telefon Numaranız" />
              </div>

                <div className="isoInputWrapper">
                    <Select style={{width:"100%"}} defaultValue="yok" id="program" onChange={this.selectProgramDegisti.bind(this)}>
                        <SelectOption value="yok"> İlgilendiğiniz Program </SelectOption>
                        {programlistesi}
                    </Select>
                </div>

                <div className="isoInputWrapper">
                    <Select style={{width:"100%"}} defaultValue="yok" id="egitim" onChange={this.selectEgitimDegisti.bind(this)}>
                        <SelectOption value="yok">Eğitim Durumunuz</SelectOption>
                        {egitimlistesi}
                    </Select>
                </div>

                <div className="isoInputWrapper">
                    <Select style={{width:"100%"}} defaultValue="yok" id="reklam" onChange={this.selectReklamDegisti.bind(this)}>
                        <SelectOption value="yok">Bizi Nereden Buldunuz ?</SelectOption>
                        {reklamlistesi}
                    </Select>
                </div>
                <div  className="isoInputWrapper">
                    <Textarea placeholder="Bize Sormak İstekilerinizi Bu Alandan Yazabilirsiniz" id="notlar" onChange={this.inputDegisti.bind(this)}></Textarea>
                </div>


              <div className="isoInputWrapper" style={{ marginBottom: '50px' }}>
                <Checkbox>
                  <IntlMessages id="page.signUpTermsConditions" />
                </Checkbox>
              </div>

              <div className="isoInputWrapper">
                <Button type="primary" onClick={this.adayUyeKaydet.bind(this)} >
                  <IntlMessages id="page.signUpButton" />
                </Button>
              </div>
              <div className="isoInputWrapper isoCenterComponent isoHelperWrapper">
                <Link to="/signin">
                  <IntlMessages id="page.signUpAlreadyAccount" />
                </Link>
              </div>
            </div>
          </div>
        </div>
      </SignUpStyleWrapper>
    );
  }
}

export default connect(
  state => ({
    isLoggedIn: state.Auth.get('idToken') !== null ? true : false
  }),
  { login }
)(SignUp);
