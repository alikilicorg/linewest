import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Input from '../../components/uielements/input';
import Button from '../../components/uielements/button';
import IntlMessages from '../../components/utility/intlMessages';
import ForgotPasswordStyleWrapper from './forgotPassword.style';
/*eslint-disable*/
import '../../containers/css/toastrc.css'
import '../../containers/css/animated.css'
import Myfx from '../../components/fonksiyonlar/fonksiyonlar'
import { ToastContainer } from "react-toastr";
import mesaj from "../../components/toast/toast";
let container;
import AppLocale from '../../languageProvider';
import config, {
    getCurrentLanguage
} from '../../containers/LanguageSwitcher/config';
const langMessage = AppLocale[getCurrentLanguage(config.defaultLanguage || 'english').locale].messages;
/*eslint-enable*/

export default class extends Component {
    state={
        mail:""
    };

    mailGir = (e) => {
        var deger = e.target.value;
        var state = this.state;
        state.mail = deger;
        this.setState(state);
    };

    sifreGonder = () =>{
        debugger;
        var mail = this.state.mail;
        var that = this;
        if(Myfx.stringBosmu(mail)){
            if(Myfx.emailKontrol(mail)){
                new mesaj(container,{durum:"success",baslik:"Mail Gönderilmiştir",icerik:"Şifre Elde Etme Maili "+mail+" Adlı Mail Adresinize Gönderilmiştir. Lütfen Kontrol Ediniz"});
                setTimeout(function () {
                    that.props.history.push('/');
                },4000);

            }else{
                new mesaj(container,{durum:"error",baslik:langMessage["alert.mesaj1"],icerik:mail+langMessage["alert.mesaj2"]});
            }
        }else{
            new mesaj(container,{durum:"error",baslik:langMessage["alert.mesaj3"],icerik:mail+langMessage["alert.mesaj4"]});
        }

    };

  render() {

    return (
      <ForgotPasswordStyleWrapper className="isoForgotPassPage">
          <ToastContainer
              ref={ref => container = ref}
              className="toast-top-right"
          />
        <div className="isoFormContentWrapper">
          <div className="isoFormContent">
            <div className="isoLogoWrapper">
              <Link to="/dashboard">
                <IntlMessages id="page.forgetPassTitle" />
              </Link>
            </div>

            <div className="isoFormHeadText">
              <h3>
                <IntlMessages id="page.forgetPassSubTitle" />
              </h3>
              <p>
                <IntlMessages id="page.forgetPassDescription" />
              </p>
            </div>

            <div className="isoForgotPassForm">
              <div className="isoInputWrapper">
                <Input size="large" placeholder="Email" onChange={this.mailGir.bind(this)}/>
              </div>

              <div className="isoInputWrapper">
                <Button type="primary" onClick={this.sifreGonder.bind(this)}>
                  <IntlMessages id="page.sendRequest" />
                </Button>
              </div>
            </div>
          </div>
        </div>
      </ForgotPasswordStyleWrapper>
    );
  }
}
