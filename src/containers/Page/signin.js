import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import axios from 'axios';
import {message} from 'antd'
import Input from '../../components/uielements/input';
import Checkbox from '../../components/uielements/checkbox';
import Button from '../../components/uielements/button';
import authAction from '../../redux/auth/actions';
import Auth0 from '../../helpers/auth0';
import Firebase from '../../helpers/firebase';
import FirebaseLogin from '../../components/firebase';
import IntlMessages from '../../components/utility/intlMessages';
import SignInStyleWrapper from './signin.style';

import config2 from '../../settings/config';
const ajaxURL = config2.http+config2.server+':'+config2.port;
const { login } = authAction;

/*eslint-disable*/
import Myfx from '../../components/fonksiyonlar/fonksiyonlar'
import AppLocale from '../../languageProvider';
import config, {
    getCurrentLanguage
} from '../../containers/LanguageSwitcher/config';
const langMessage = AppLocale[getCurrentLanguage(config.defaultLanguage || 'english').locale].messages;
/*eslint-enable*/

//const { login } = authAction;

class SignIn extends Component {
  state = {
    redirectToReferrer: false,
    mail:"",
    sifre:"",
    isPersonel:false
  };
  componentWillReceiveProps(nextProps) {
    if (
      this.props.isLoggedIn !== nextProps.isLoggedIn &&
      nextProps.isLoggedIn === true
    ) {
      this.setState({ redirectToReferrer: true });
    }
  }

  yetkiKontrol = (userid,yetkiid,konum,success,error) =>{
    axios.post(ajaxURL+'/yetkilendirme/yetkiKontrol',{
      userid : userid,
      yetkiid : yetkiid,
      usertype: konum
      }).then(function (res) {
        success(res);
      }).catch(function(err){
        error(err);
      });
  }

  handleLogin = () => {
    debugger;
    var that = this;
      if(Myfx.stringBosmu(that.state.mail) && Myfx.stringBosmu(that.state.sifre) ){
        if(Myfx.emailKontrol(that.state.mail)){

          if(that.state.isPersonel){
            axios.post(ajaxURL+'/personel/loginControl',{
            mail : that.state.mail,
            sifre : that.state.sifre
            }).then(function (res) {
                if(res.data.status===true){
                  var personelid = res.data.result["id"];
                  var yetkiid = 1;
                  that.yetkiKontrol(personelid,yetkiid,'personel',function(res){
                    if(res.data.status){
                        localStorage.setItem('user',JSON.stringify({id:personelid,type:"personel",yetkiler:[]}));
                        that.props.history.push('/dashboard');
                        message.success("Sisteme Başarılı Bir Şekilde Giriş Yaptınız");
                    }else{
                        message.error("Personel Girişi İçin Yetkiniz Bulunmamaktadır");
                    }
                  },function(err){
                    message.error("Personel Girişi İçin Yetkiniz Bulunmamaktadır");
                  });
                }else{
                    message.error("Girdiğiniz Bilgiler Dahilinde Sistemimizde Böyle Bir Kayıda Rastlanmamıştır. Lütfen İletişime Geçiniz");
                }
            });
          }else{
            axios.post(ajaxURL+'/user/loginControl',{
              mail : that.state.mail,
              sifre : that.state.sifre
              }).then(function (res) {
                  if(res.data.status===true){
                    var personelid = res.data.result["id"];
                    var yetkiid = 32;
                    that.yetkiKontrol(personelid,yetkiid,'user',function(res){
                      if(res.data.status){
                        localStorage.setItem('user',JSON.stringify({id:personelid,type:"user",yetkiler:[]}));
                        localStorage.setItem('editUser',JSON.stringify({id:personelid,durum:true,isim:"Profiliniz"}));
                        that.props.history.push('/dashboard');
                        message.success("Sisteme Başarılı Bir Şekilde Giriş Yaptınız");
                        
                      }else{
                        message.error("Personel Girişi İçin Yetkiniz Bulunmamaktadır");
                    }
                  },function(err){
                    message.error("Personel Girişi İçin Yetkiniz Bulunmamaktadır");
                  });
                }else{
                    message.error("Girdiğiniz Bilgiler Dahilinde Sistemimizde Böyle Bir Kayıda Rastlanmamıştır. Lütfen İletişime Geçiniz");
                }
              });
          }
            
        }else{
            message.error(that.state.mail+" : Mail Adresi Kontrol Sistemimizden Geçemedi. Lütfen Doğru Bir Mail Giriniz");
        }
      }else {
        message.error("Giriş Bilgilerinizi Eksik Girmişsiniz. Lütfen Tüm Bilgilerinizi Giriniz");
      }
  };

  setMail=(e)=>{
    var mail = e.target.value;
    var state = this.state;
      state.mail = mail;
      this.setState(state);
  };
  setSifre=(e)=>{
      var sifre = e.target.value;
      var state = this.state;
      state.sifre = sifre;
      this.setState(state);
  };

  setPersonel=(e)=>{
    debugger;
    var personelmi = e.target.checked;
    var state = this.state;
    state.isPersonel = personelmi;
    this.setState(state);
  }

  render() {
    const from = { pathname: '/dashboard' };
    const { redirectToReferrer } = this.state;

    if (redirectToReferrer) {
      return <Redirect to={from} />;
    }
    return (
      <SignInStyleWrapper className="isoSignInPage">
        <div className="isoLoginContentWrapper">
          <div className="isoLoginContent">
            <div className="isoLogoWrapper">
              <Link to="/dashboard">
                <IntlMessages id="page.signInTitle" />
              </Link>
            </div>

            <div className="isoSignInForm">
                <div className="isoInputWrapper">
                <Input size="large" onChange={this.setMail.bind(this)} placeholder="Mail Adresiniz" />
              </div>

              <div className="isoInputWrapper">
                <Input size="large" onChange={this.setSifre.bind(this)} type="password" placeholder="Parolanız" />
              </div>

              <div className="isoInputWrapper isoLeftRightComponent">
                    <Checkbox onChange={this.setPersonel.bind(this)} checked={this.state.isPersonel}>
                        <IntlMessages id="page.personelcheckbox" />
                    </Checkbox>
                </div>

              <div className="isoInputWrapper isoLeftRightComponent">
                <Checkbox>
                  <IntlMessages id="page.signInRememberMe" />
                </Checkbox>
                <Button type="primary" onClick={this.handleLogin}>
                  <IntlMessages id="page.signInButton" />
                </Button>
              </div>

              <p className="isoHelperText">
                <IntlMessages id="page.signInPreview" />
              </p>

              <div className="isoInputWrapper isoOtherLogin">

                {Auth0.isValid &&
                  <Button
                    onClick={() => {
                      Auth0.login(this.handleLogin);
                    }}
                    type="primary btnAuthZero"
                  >
                    <IntlMessages id="page.signInAuth0" />
                  </Button>}

                {Firebase.isValid && <FirebaseLogin login={this.handleLogin} />}
              </div>
              <div className="isoCenterComponent isoHelperWrapper">
                <Link to="/forgotpassword" className="isoForgotPass">
                  <IntlMessages id="page.signInForgotPass" />
                </Link>
                <Link to="/signup">
                  <IntlMessages id="page.signInCreateAccount" />
                </Link>
              </div>
            </div>
          </div>
        </div>
      </SignInStyleWrapper>
    );
  }
}

export default connect(
  state => ({
    isLoggedIn: state.Auth.get('idToken') !== null ? true : false,
  }),
  { login }
)(SignIn);
