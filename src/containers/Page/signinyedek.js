// FN : 1526235901007
import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import axios from 'axios';
import Input from '../../components/uielements/input';
import Checkbox from '../../components/uielements/checkbox';
import Button from '../../components/uielements/button';
import authAction from '../../redux/auth/actions';
import Auth0 from '../../helpers/auth0';
import IntlMessages from '../../components/utility/intlMessages';
import SignInStyleWrapper from './signin.style';
import config2 from '../../settings/config';
const ajaxURL = config2.http+config2.server+':'+config2.port;
const { login } = authAction;

/*eslint-disable*/
import '../../containers/css/toastrc.css'
import '../../containers/css/animated.css'
import Myfx from '../../components/fonksiyonlar/fonksiyonlar'

import { ToastContainer } from "react-toastr";
import mesaj from '../../components/toast/toast'
let container;
import AppLocale from '../../languageProvider';
import config, {
    getCurrentLanguage
} from '../../containers/LanguageSwitcher/config';
const langMessage = AppLocale[getCurrentLanguage(config.defaultLanguage || 'english').locale].messages;
/*eslint-enable*/
class SignIn extends Component {
  state = {
    redirectToReferrer: false,
    mail:"",
    sifre:"",
    isPersonel:false
  };
  componentWillReceiveProps(nextProps) {
    if (
      this.props.isLoggedIn !== nextProps.isLoggedIn &&
      nextProps.isLoggedIn === true
    ) {
      this.setState({ redirectToReferrer: true });
    }
  }
  setMail=(e)=>{
    var mail = e.target.value;
    var state = this.state;
      state.mail = mail;
      this.setState(state);
  };
  setSifre=(e)=>{
      var sifre = e.target.value;
      var state = this.state;
      state.sifre = sifre;
      this.setState(state);
  };
  setPersonel=(e)=>{
    debugger;
    var personelmi = e.target.checked;
    var state = this.state;
    state.isPersonel = personelmi;
    this.setState(state);
  }
  yetkiKontrol = (userid,yetkiid,konum,success,error) =>{
    axios.post(ajaxURL+'/yetkilendirme/yetkiKontrol',{
      userid : userid,
      yetkiid : yetkiid,
      usertype: konum
      }).then(function (res) {
        success(res);
      }).catch(function(err){
        error(err);
      });
  }
  handleLogin = () => {
    /*const { login } = this.props;
    login();
    this.props.history.push('/dashboard');*/
    debugger;
      var that = this;
        if(Myfx.stringBosmu(that.state.mail) && Myfx.stringBosmu(that.state.sifre) ){
          if(Myfx.emailKontrol(that.state.mail)){

            if(that.state.isPersonel){
              axios.post(ajaxURL+'/personel/loginControl',{
              mail : that.state.mail,
              sifre : that.state.sifre
              }).then(function (res) {
                  if(res.data.status===true){
                    var personelid = res.data.result["id"];
                    var yetkiid = 1;
                    that.yetkiKontrol(personelid,yetkiid,'personel',function(res){
                      if(res.data.status){
                        new mesaj(container,{durum:"success",baslik:langMessage["alert.mesaj7"],icerik:that.state.mail+langMessage["alert.mesaj8"]});
                        localStorage.setItem('user',JSON.stringify({id:personelid,type:"personel",yetkiler:[]}));
                        that.props.history.push('/dashboard');
                      }else{
                        new mesaj(container,{durum:"error",baslik:"Yetkiniz Yok",icerik:"Personel Girişi İçin Yetkiniz Bulunmamaktadır"});
                      }
                    },function(err){
                      new mesaj(container,{durum:"error",baslik:"Yetkiniz Yok",icerik:"Personel Girişi İçin Yetkiniz Bulunmamaktadır"});
                    });
                  }else{
                      new mesaj(container,{durum:"error",baslik:langMessage["alert.mesaj5"],icerik:that.state.mail+langMessage["alert.mesaj6"]});
                  }
              });
            }else{
              axios.post(ajaxURL+'/user/loginControl',{
                mail : that.state.mail,
                sifre : that.state.sifre
                }).then(function (res) {
                    if(res.data.status===true){
                      var personelid = res.data.result["id"];
                      var yetkiid = 32;
                      that.yetkiKontrol(personelid,yetkiid,'user',function(res){
                        if(res.data.status){
                          new mesaj(container,{durum:"success",baslik:langMessage["alert.mesaj7"],icerik:that.state.mail+langMessage["alert.mesaj8"]});
                          localStorage.setItem('user',JSON.stringify({id:personelid,type:"user",yetkiler:[]}));
                          localStorage.setItem('editUser',JSON.stringify({id:personelid,durum:true,isim:"Profiliniz"}));
                          that.props.history.push('/dashboard');
                        }else{
                          new mesaj(container,{durum:"error",baslik:"Yetkiniz Yok",icerik:"Kullanıcı Girişi İçin Yetkiniz Bulunmamaktadır"});
                        }
                      },function(err){
                        new mesaj(container,{durum:"error",baslik:"Yetkiniz Yok",icerik:"Kullanıcı Girişi İçin Yetkiniz Bulunmamaktadır"});
                      });
                    }else{
                        new mesaj(container,{durum:"error",baslik:langMessage["alert.mesaj5"],icerik:that.state.mail+langMessage["alert.mesaj6"]});
                    }
                });
            }
              
          }else{
              new mesaj(container,{durum:"error",baslik:langMessage["alert.mesaj1"],icerik:that.state.mail+langMessage["alert.mesaj2"]});
          }
        }else {
            new mesaj(container,{durum:"error",baslik:langMessage["alert.mesaj3"],icerik:that.state.mail+langMessage["alert.mesaj4"]});
        }
    

  };

  render() {
    const from = { pathname: '/dashboard' };
    const { redirectToReferrer } = this.state;

    if (redirectToReferrer) {
      return <Redirect to={from} />;
    }
    return (
      <SignInStyleWrapper className="isoSignInPage">
          <ToastContainer
              ref={ref => container = ref}
              className="toast-top-right"
          />
        <div className="isoLoginContentWrapper">
          <div className="isoLoginContent">
            <div className="isoLogoWrapper">
              <Link to="/dashboard">
                <IntlMessages id="page.signInTitle" />
              </Link>
            </div>

            <div className="isoSignInForm">
              <div className="isoInputWrapper">
                <Input size="large" onChange={this.setMail.bind(this)} placeholder="Mail Adresiniz" />
              </div>

              <div className="isoInputWrapper">
                <Input size="large" onChange={this.setSifre.bind(this)} type="password" placeholder="Parolanız" />
              </div>

                <div className="isoInputWrapper isoLeftRightComponent">
                    <Checkbox onChange={this.setPersonel.bind(this)} checked={this.state.isPersonel}>
                        <IntlMessages id="page.personelcheckbox" />
                    </Checkbox>
                </div>

              <div className="isoInputWrapper isoLeftRightComponent">
                <Checkbox checked>
                  <IntlMessages id="page.signInRememberMe" />
                </Checkbox>
                <Button type="primary" onClick={this.handleLogin}>
                  <IntlMessages id="page.signInButton" />
                </Button>
              </div>

              <div className="isoInputWrapper isoOtherLogin">
                {Auth0.isValid &&
                  <Button
                    onClick={() => {
                      Auth0.login(this.handleLogin);
                    }}
                    type="primary btnAuthZero"
                  >
                    <IntlMessages id="page.signInAuth0" />
                  </Button>}

                {/*{Firebase.isValid && <FirebaseLogin login={this.handleLogin} />}*/}
              </div>
              <div className="isoCenterComponent isoHelperWrapper">
                <Link to="/forgotpassword" className="isoForgotPass">
                  <IntlMessages id="page.signInForgotPass" />
                </Link>
                <Link to="/signup">
                  <IntlMessages id="page.signInCreateAccount" />
                </Link>
              </div>
            </div>
          </div>
        </div>
      </SignInStyleWrapper>
    );
  }
}

export default connect(
  state => ({
    isLoggedIn: state.Auth.get('idToken') !== null ? true : false,
  }),
  { login }
)(SignIn);
