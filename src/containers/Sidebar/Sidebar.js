import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import {message} from 'antd';
import config2 from '../../settings/config';
import clone from 'clone';
import { Link } from 'react-router-dom';
import { Layout } from 'antd';
//import options from './options';
import Scrollbars from '../../components/utility/customScrollBar.js';
import Menu from '../../components/uielements/menu';
import IntlMessages from '../../components/utility/intlMessages';
import SidebarWrapper from './sidebar.style';
import appActions from '../../redux/app/actions';
import Logo from '../../components/utility/logo';
const ajaxURL = config2.http+config2.server+':'+config2.port;
const SubMenu = Menu.SubMenu;
const { Sider } = Layout;

window.editUser = JSON.parse(localStorage.getItem('editUser'));

const {
  toggleOpenDrawer,
  changeOpenKeys,
  changeCurrent,
  toggleCollapsed,
} = appActions;
const stripTrailingSlash = str => {
  if (str.substr(-1) === '/') {
    return str.substr(0, str.length - 1);
  }
  return str;
};

class Sidebar extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.onOpenChange = this.onOpenChange.bind(this);
    this.state = {
      options : [],
      anamenuler:[],
      altmenuler:[],
      user:localStorage.getItem('user')!==null ? JSON.parse(localStorage.getItem('user')):{id:-1,durum:false,type:"user"}
    };
  }

  yetkileriGetir(){
    var that = this;
    var ekURL = "";
    var data = {};
    if(this.state.user.type==="personel"){
      ekURL = "/yetkilendirme/personelTumYetkiler"
      data["personelid"]=this.state.user.id;
    }else{
      ekURL = "/yetkilendirme/userTumYetkiler"
      data["userid"]=this.state.user.id;
    }
    axios.post(ajaxURL+ekURL,data).then(function(gelen){
      debugger;
      var state = that.state;
      state.user.yetkiler = gelen.data.result;
      that.setState(state);
    }).catch(function(hata){
      message.error(hata.message);
    });
    
  }

  anaMenuleriGetir(){
    var that = this;
    axios.get(ajaxURL+'/menu/anamenulist').then(function(gelen){
      var state = that.state;
      state.anamenuler = gelen.data.result;
      that.setState(state);
    }).catch(function(hata){
      message.error(hata.message);
    });
  }

  altMenuleriGetir(){
    var that = this;
    axios.get(ajaxURL+'/menu/altmenulist').then(function(gelen){
      var state = that.state;
      state.altmenuler = gelen.data.result;
      that.setState(state);
    }).catch(function(hata){
      message.error(hata.message);
    });
  }

  optionsRefresh(){
    var anamenuler = this.state.anamenuler;
    var altmenuler = this.state.altmenuler;
    var newoptions = [];
    for(var i in anamenuler){
      var id = anamenuler[i].id;
      var key = anamenuler[i].key;
      var label = anamenuler[i].label;
      var lefticon = anamenuler[i].lefticon;
      var children = anamenuler[i].children;
      var yetkiid = anamenuler[i].yetkiid;
      if(this.state.user.yetkiler.indexOf(yetkiid)!==-1){
        if(children===true){
          var newaltmenu = [];
          for(var j in altmenuler){
            var key2 = altmenuler[j].key;
            var label2 = altmenuler[j].label;
            var yetkiid2 = altmenuler[j].yetkiid;
            var anamenuid = altmenuler[j].anamenuid;
            if(this.state.user.yetkiler.indexOf(yetkiid2)!==-1 && anamenuid===id){
              newaltmenu.push({key:key2,label:label2});
            }
          }
          newoptions.push({key:key,label:label,leftIcon:lefticon,children:newaltmenu});
        }else{
          newoptions.push({key:key,label:label,leftIcon:lefticon});
        }
      }
    }
    var state = this.state;
    state.options = newoptions;
    this.setState(state);
  }

  componentWillMount(){
    var that = this;
    this.yetkileriGetir();
    this.anaMenuleriGetir();
    this.altMenuleriGetir();
    
    
  }
  componentDidMount(){
    var that = this;
    setTimeout(function(){
     that.optionsRefresh();
    },500);
    
  }

  handleClick(e) {
    this.props.changeCurrent([e.key]);
    if (this.props.app.view === 'MobileView') {
      setTimeout(() => {
        this.props.toggleCollapsed();
        this.props.toggleOpenDrawer();
      }, 100);
    }
  }
  onOpenChange(newOpenKeys) {
    const { app, changeOpenKeys } = this.props;
    const latestOpenKey = newOpenKeys.find(
      key => !(app.openKeys.indexOf(key) > -1)
    );
    const latestCloseKey = app.openKeys.find(
      key => !(newOpenKeys.indexOf(key) > -1)
    );
    let nextOpenKeys = [];
    if (latestOpenKey) {
      nextOpenKeys = this.getAncestorKeys(latestOpenKey).concat(latestOpenKey);
    }
    if (latestCloseKey) {
      nextOpenKeys = this.getAncestorKeys(latestCloseKey);
    }
    changeOpenKeys(nextOpenKeys);
  }
  getAncestorKeys = key => {
    const map = {
      sub3: ['sub2'],
    };
    return map[key] || [];
  };
  getMenuItem = ({ singleOption, submenuStyle, submenuColor }) => {
    const { key, label, leftIcon, children } = singleOption;
    const url = stripTrailingSlash(this.props.url);
    if (children) {
      return (
        <SubMenu
          key={key}
          title={
            <span className="isoMenuHolder" style={submenuColor}>
              <i className={leftIcon} />
              <span className="nav-text">
                <IntlMessages id={label} />
              </span>
            </span>
          }
        >
          {children.map(child => {
            const linkTo = child.withoutDashboard
              ? `/${child.key}`
              : `${url}/${child.key}`;
            return (
              <Menu.Item style={submenuStyle} key={child.key}>
                <Link style={submenuColor} to={linkTo}>
                  <IntlMessages id={child.label} />
                </Link>
              </Menu.Item>
            );
          })}
        </SubMenu>
      );
    }
    return (
      <Menu.Item key={key}>
        <Link to={`${url}/${key}`}>
          <span className="isoMenuHolder" style={submenuColor}>
            <i className={leftIcon} />
            <span className="nav-text">
              <IntlMessages id={label} />
            </span>
          </span>
        </Link>
      </Menu.Item>
    );
  };
  render() {
    const { app, toggleOpenDrawer, customizedTheme, height } = this.props;
    const collapsed = clone(app.collapsed) && !clone(app.openDrawer);
    const { openDrawer } = app;
    const mode = collapsed === true ? 'vertical' : 'inline';
    const onMouseEnter = event => {
      if (openDrawer === false) {
        toggleOpenDrawer();
      }
      return;
    };
    const onMouseLeave = () => {
      if (openDrawer === true) {
        toggleOpenDrawer();
      }
      return;
    };
    const styling = {
      backgroundColor: customizedTheme.backgroundColor,
    };
    const submenuStyle = {
      backgroundColor: 'rgba(0,0,0,0.3)',
      color: customizedTheme.textColor,
    };
    const submenuColor = {
      color: customizedTheme.textColor,
    };
    return (
      <SidebarWrapper>
        <Sider
          trigger={null}
          collapsible={true}
          collapsed={collapsed}
          width="240"
          className="isomorphicSidebar"
          onMouseEnter={onMouseEnter}
          onMouseLeave={onMouseLeave}
          style={styling}
        >
          <Logo collapsed={collapsed} />
          <Scrollbars style={{ height: height - 70 }}>
            <Menu
              onClick={this.handleClick}
              theme="dark"
              className="isoDashboardMenu"
              mode={mode}
              openKeys={collapsed ? [] : app.openKeys}
              selectedKeys={app.current}
              onOpenChange={this.onOpenChange}
            >
              {this.state.options.map(singleOption =>
                this.getMenuItem({ submenuStyle, submenuColor, singleOption })
              )}
            </Menu>
          </Scrollbars>
        </Sider>
      </SidebarWrapper>
    );
  }
}

export default connect(
  state => ({
    app: state.App.toJS(),
    customizedTheme: state.ThemeSwitcher.toJS().sidebarTheme,
    height: state.App.toJS().height,
  }),
  { toggleOpenDrawer, changeOpenKeys, changeCurrent, toggleCollapsed }
)(Sidebar);
