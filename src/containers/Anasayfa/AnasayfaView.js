import React, { Component } from "react";
import { connect } from "react-redux";
import Card from '../../components/uielements/card';
import LayoutWrapper from '../../components/utility/layoutWrapper.js';
import PageHeader from '../../components/utility/pageHeader';
import { Row } from 'antd';


class Anasayfa extends Component {

    render() {
        const rowStyle = {
            width: '100%',
            display: 'flex',
            flexFlow: 'row wrap'
        };
        const gutter = 16;
        return (
            <LayoutWrapper>
                <PageHeader>Sayfa Başlığı</PageHeader>
                <Row style={rowStyle} gutter={gutter} justify="start">
asd
                </Row>
                <div style={{ height: "100%" }}>
                <Card
                    title="Kart Başlığı"
                    extra={<a>Yardım</a>}
                    style={{ width: '100%' }}
                >
                    <p>Deneme amaçlı birşeyler yazıldı</p>
                    <p>Card content</p>
                </Card>
            </div>
            </LayoutWrapper>
        );
    }
}
export default connect(state => ({
    ...state.App.toJS()
}))(Anasayfa);
