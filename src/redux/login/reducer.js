import actions from './actions';


export default function loginReducer(state, action) {
    switch (action.type) {
        case actions.SET_USER_INFO:
            return state.set('loginUser', action.user);
        default:
            return state;
    }
}