import { all, takeEvery, fork } from 'redux-saga/effects';
import actions from './actions';

export function* setUserInfoAction() {
    yield takeEvery(actions.SET_USER_INFO, function*() {});
}
export default function* rootSaga() {
    yield all([fork(setUserInfoAction)]);
}