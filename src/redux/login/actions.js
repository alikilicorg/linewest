const actions = {
    SET_USER_INFO:'SET_USER_INFO',
    setUserInfo:user => {
        return (dispatch, getState) => {
            const userData = getState().Invoices.get('user');
            dispatch({
                type:actions.SET_USER_INFO,
                user:userData || user
            });
        };
    }

};
export default actions;