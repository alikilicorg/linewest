const durum = 1; // 0 local | 1 server
const serverIp = "188.166.122.215";

const config = {
    server:durum===0 ? "localhost":serverIp,
    localhost:"localhost",
    http:"http://",
    port:"3001",
    localport:"3000",
    serverFileRoot:""
};
config.serverFileRoot =config.http+config.server+':'+config.port

export default config;
