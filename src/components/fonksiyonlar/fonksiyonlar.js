class Myfx {
    constructor(){
        this.version="1.0.0.0";
    }
    static emailKontrol = (email) => {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    };
    static stringBosmu = (text) => {
        var status = false;
        if(text.length!==0){status=true;}
        return status;
    };
}

export default Myfx;