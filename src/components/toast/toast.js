class mesaj {
    constructor(container,obj){
        this.durumlar = ["error","info","success","warning"];
        this.durum="";
        if(this.durumlar.indexOf(obj["durum"])>-1){
            this.durum=obj["durum"];
        }else{
            this.durum="info";
        }
        this.mesaj=obj["icerik"] || "";
        this.baslik=obj["baslik"] || "";
        this.options = obj["options"] || {closeButton: true};
        this.container = container;
        this.goster();
    }

    goster = () => {
        if(this.durum==="success"){
            this.container.success(this.mesaj,this.baslik,this.options);
        }
        if(this.durum==="error"){
            this.container.error(this.mesaj,this.baslik,this.options);
        }
        if(this.durum==="info"){
            this.container.info(this.mesaj,this.baslik,this.options);
        }
        if(this.durum==="warning"){
            this.container.warning(this.mesaj,this.baslik,this.options);
        }

    };

    clear = () =>{
      this.container.clear();
    };

}

export default mesaj;
